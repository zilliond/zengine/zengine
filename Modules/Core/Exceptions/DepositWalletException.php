<?php


namespace Modules\Core\Exceptions;

class DepositWalletException extends \Exception
{
    public function __construct($depositId)
    {
        parent::__construct("User wallet in deposit - $depositId not set or invalid", 500);
    }
}
