<?php


namespace Modules\Core\Exceptions;


class CurrencyNotFoundException extends \Exception
{
    public function __construct($message = "Currency not find")
    {
        parent::__construct($message, 500);
    }
}
