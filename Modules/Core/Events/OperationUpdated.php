<?php

namespace Modules\Core\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Modules\Core\Models\Operation;

class OperationUpdated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Operation
     *
     */
    public $operation;

    /**
     * Create a new event instance.
     *
     * @param  Operation  $operation
     */
    public function __construct(Operation $operation)
    {
        $this->operation = Operation::find($operation->id);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('operation.'.$this->operation->id);
    }

    /**
     * @return string
     */
    public function broadcastAs()
    {
        return 'operation.updated';
    }
}
