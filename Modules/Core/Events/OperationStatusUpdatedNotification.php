<?php

namespace Modules\Core\Events;

use Illuminate\Broadcasting\PrivateChannel;
use Modules\Core\Models\Operation;

class OperationStatusUpdatedNotification extends OperationNotificationEvent
{
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel("user.operations.{$this->operation->user_id}");
    }

    /**
     * @return string
     */
    public function broadcastAs()
    {
        return 'operation.status-updated';
    }

    /**
     * @return array
     */
    public function broadcastWith()
    {
        \Log::info($this->getTranslateParams());
        \Log::info(trans("notifications.user_operations.status_updated.{$this->operation->status}.{$this->operation->type}", $this->getTranslateParams()));
        return [
            'operation'     => Operation::find($this->operation->id),
            'has_message'   => \Lang::has("notifications.user_operations.status_updated.{$this->operation->status}.{$this->operation->type}") &&
                trans("notifications.user_operations.status_updated.{$this->operation->status}.{$this->operation->type}", $this->getTranslateParams()),
            'message'       => trans("notifications.user_operations.status_updated.{$this->operation->status}.{$this->operation->type}", $this->getTranslateParams()),
            'style'         => $this->getNotificationStatus()
        ];
    }
}
