<?php


namespace Modules\Core\OperationTypes;


use Modules\Core\Models\Operation;
use Modules\Core\Services\Contracts\OperationServiceInterface;

class DepositOpenOperationType extends OperationType
{
    public function success(Operation $operation) : Operation
    {
        app(OperationServiceInterface::class)->referralPay($operation);
        return parent::success($operation);
    }
}
