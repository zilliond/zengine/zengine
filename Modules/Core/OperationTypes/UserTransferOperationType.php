<?php


namespace Modules\Core\OperationTypes;


use Modules\Core\Models\Operation;
use Modules\Core\Services\Contracts\UserServiceInterface;

class UserTransferOperationType extends OperationType
{
    /**
     * @param  Operation  $operation
     * @return Operation
     * @throws \Modules\Core\Exceptions\BalanceException
     * @throws \Modules\Core\Exceptions\BalanceTypeNotFindException
     * @throws \Modules\Core\Exceptions\CanPayWalletException
     * @throws \Modules\Core\Exceptions\OperationWalletInvalidException
     * @throws \Modules\Core\Exceptions\UserBalancesNotDividedException
     */
    public function create(Operation $operation) : Operation
    {
        app(UserServiceInterface::class)->canPayFrom($operation->user, $operation->amount, $operation->currency, $operation->wallet);
        app(UserServiceInterface::class)->takeMoney($operation);
        $operation = parent::create($operation);
        return $this->success($operation);
    }

    /**
     * @param  Operation  $operation
     * @return Operation
     * @throws \Modules\Core\Exceptions\BalanceTypeNotFindException
     * @throws \Modules\Core\Exceptions\OperationWalletInvalidException
     */
    public function success(Operation $operation) : Operation
    {
        $wallet = null;
        $user = $operation->user;
        if ('divided' === setting('balances_type')) {
            $wallet = $user->wallets()->firstOrCreate([
                'payment_system_id' => $operation->wallet->payment_system_id,
            ]);
        }
        app(UserServiceInterface::class)->putMoney($operation, $user, $wallet);
        return parent::success($operation);
    }
}
