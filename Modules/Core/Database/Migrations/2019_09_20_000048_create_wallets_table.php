<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('payment_system_id');
            $table->string('wallet')->default('');
            $table->decimal('balance', setting('currencies.amount.decimal_total'), setting('currencies.amount.decimal_precision'))->default(0);
            $table->timestamps();

            $table->unique(['user_id', 'payment_system_id'], 'user_id_payment_system_id_unique');
            //$table->unique(['wallet', 'payment_system_id'], 'wallet_payment_system_id_unique');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('payment_system_id')->references('id')->on('payment_systems');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallets');
    }
}
