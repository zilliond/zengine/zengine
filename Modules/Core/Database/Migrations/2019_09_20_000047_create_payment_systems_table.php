<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentSystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_systems', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('provider');
            $table->string('wallet')->nullable();
            $table->string('code')->nullable();
            $table->unsignedInteger('currency_id');
            $table->decimal('balance', setting('currencies.amount.decimal_total'), setting('currencies.amount.decimal_precision'))->default(0);

            $table->boolean('can_refill')->default(1);
            $table->decimal('refill_commission', setting('currencies.amount.decimal_total'), setting('currencies.amount.decimal_precision'))->default(0);
            $table->decimal('min_refill_amount', setting('currencies.amount.decimal_total'), setting('currencies.amount.decimal_precision'))->default(0);
            $table->decimal('max_refill_amount', setting('currencies.amount.decimal_total'), setting('currencies.amount.decimal_precision'))->default(0);

            $table->boolean('can_withdraw')->default(1);
            $table->string('withdraw_type')->default('manual');
            $table->decimal('min_withdraw_amount', setting('currencies.amount.decimal_total'), setting('currencies.amount.decimal_precision'))->default(0);
            $table->decimal('max_withdraw_amount', setting('currencies.amount.decimal_total'), setting('currencies.amount.decimal_precision'))->default(0);
            $table->decimal('withdraw_commission', setting('currencies.amount.decimal_total'), setting('currencies.amount.decimal_precision'))->default(0);

            $table->boolean('enabled')->default(1);
            $table->boolean('need_wallet')->default(0);
            $table->text('credentials')->nullable();
            $table->timestamps();

            $table->foreign('currency_id')->references('id')->on('currencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_systems');
    }
}
