<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('login')->unique();
            $table->string('name')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('pin_code')->nullable();
            $table->string('google2fa_secret')->nullable();
            $table->decimal('balance', setting('currencies.amount.decimal_total'), setting('currencies.amount.decimal_precision'))->default(0);
            $table->unsignedInteger('currency_id')->default(1);
            $table->mediumInteger('role')->default(app('zengine')->modelClass('User')::ROLE_USER);
            $table->unsignedBigInteger('inviter_id')->nullable();
            $table->unsignedSmallInteger('ref_level')->default(0);
            $table->string('referral_percent')->nullable();
            $table->decimal('referrals_earned', setting('currencies.amount.decimal_total'), setting('currencies.amount.decimal_precision'))->default(0);
            $table->decimal('invested', setting('currencies.amount.decimal_total'), setting('currencies.amount.decimal_precision'))->default(0);
            $table->decimal('structure_invested', setting('currencies.amount.decimal_total'), setting('currencies.amount.decimal_precision'))->default(0);
            $table->string('locale')->default(config('app.fallback_locale', 'en'));
            $table->string('comment')->nullable();

            $table->string('last_login_ip')->nullable();
            $table->string('country_code', 5)->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('last_login_at')->nullable();
            $table->timestamp('last_online_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('inviter_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
