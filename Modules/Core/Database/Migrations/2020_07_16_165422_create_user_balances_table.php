<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (setting('balances_type') === 'divided') {
            Schema::create('user_balances', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('user_id');
                $table->unsignedBigInteger('payment_system_id');
                $table->decimal('balance', setting('currencies.amount.decimal_total'), setting('currencies.amount.decimal_precision'))->default(0);
                $table->foreign('user_id')->references('id')->on('users');
                $table->foreign('payment_system_id')->references('id')->on('payment_systems');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (setting('balances_type') === 'divided') {
            Schema::dropIfExists('user_balances');
        }
    }
}
