<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = factory(app('zengine')->modelClass('Plan'), 5)->create();
        $this->command->info($plans->count().' Plans created.');
    }
}
