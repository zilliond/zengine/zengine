<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;

class PaymentSystemsDefaultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rub = app('zengine')->model('Currency')->where('code_iso', 'RUB')->firstOrFail();
        app('zengine')->model('PaymentSystem')->create([
            'name'          => 'Ручное USD',
            'provider'      => 'manual',
            'code'          => 'USD',
            'currency_id'   => 1,
            'enabled'       => 1,
            'can_refill'    => 1,
            'can_withdraw'  => 1,
            'need_wallet'   => 0,
        ]);
        app('zengine')->model('PaymentSystem')->create([
            'name'          => 'Payeer USD',
            'provider'      => 'payeer',
            'wallet'        => env('PAYEER_ACCOUNT'),
            'code'          => 'PY',
            'currency_id'   => 1,
            'enabled'       => 1,
            'can_refill'    => 1,
            'can_withdraw'  => 1,
            'need_wallet'   => 1,
            'credentials'   => [
                'shop_id'   => env('PAYEER_SHOP_ID'),
                'secret'    => env('PAYEER_SECRET'),
                'account'   => env('PAYEER_ACCOUNT'),
                'api_id'    => env('PAYEER_API_ID'),
                'api_key'   => env('PAYEER_API_KEY'),
            ]
        ]);
        app('zengine')->model('PaymentSystem')->create([
            'name'          => 'Perfect Money USD',
            'provider'      => 'perfect_money',
            'wallet'        => env('PMONEY_ACC'),
            'code'          => 'PM',
            'currency_id'   => 1,
            'enabled'       => 1,
            'can_refill'    => 1,
            'can_withdraw'  => 1,
            'need_wallet'   => 1,
            'credentials'   => [
                'sci_account'   => env('PMONEY_ACC'),
                'sci_name'      => env('PMONEY_NAME'),
                'sci_key'       => env('PMONEY_KEY'),
                'api_acc'       => env('PMONEY_API_ACC'),
                'api_pass'      => env('PMONEY_API_PASS'),
            ]
        ]);
        app('zengine')->model('PaymentSystem')->create([
            'name'          => 'Advanced Cash USD',
            'provider'      => 'adv_cash',
            'wallet'        => env('ADVCASH_ACC'),
            'code'          => 'AC',
            'currency_id'   => 1,
            'enabled'       => 1,
            'can_refill'    => 1,
            'can_withdraw'  => 1,
            'need_wallet'   => 1,
            'credentials'   => [
                'sci_account'   => env('ADVCASH_ACC'),
                'sci_name'      => env('ADVCASH_NAME'),
                'sci_key'       => env('ADVCASH_KEY'),
                'api_name'      => env('ADVCASH_API_NAME'),
                'api_key'       => env('ADVCASH_API_KEY'),
            ]
        ]);
        app('zengine')->model('PaymentSystem')->create([
            'name'          => 'Cryptonator USD',
            'provider'      => 'cryptonator',
            'code'          => 'CN',
            'currency_id'   => 1,
            'enabled'       => 1,
            'can_refill'    => 1,
            'can_withdraw'  => 1,
            'credentials'   => [
                'sci_id'        => env('CRYPTONATOR_MERCHANT'),
                'sci_secret'    => env('CRYPTONATOR_SECRET'),
            ]
        ]);
        app('zengine')->model('PaymentSystem')->create([
            'name'          => 'FreeKassa',
            'provider'      => 'freekassa',
            'code'          => 'FK',
            'currency_id'   => 1,
            'enabled'       => 1,
            'can_refill'    => 1,
            'can_withdraw'  => 1,
            'need_wallet'   => 1,
            'credentials'   => [
                'merchant_id'       => env('FREEKASSA_MERCHANT_ID'),
                'merchant_secret'   => env('FREEKASSA_MERCHANT_SECRET'),
                'merchant_secret_2' => env('FREEKASSA_MERCHANT_SECRET_2'),
                'wallet_id'         => env('FREEKASSA_WALLET_ID'),
                'wallet_key'        => env('FREEKASSA_WALLET_KEY'),
            ]
        ]);
        app('zengine')->model('PaymentSystem')->create([
            'name'          => 'Qiwi',
            'provider'      => 'qiwi',
            'code'          => 'QW',
            'currency_id'   => $rub->id,
            'enabled'       => 1,
            'can_refill'    => 1,
            'can_withdraw'  => 0,
            'need_wallet'   => 1,
            'credentials'   => [
                'public_key'    => env('QIWI_PUBLIC_KEY'),
                'private_key'   => env('QIWI_PRIVATE_KEY'),
            ]
        ]);
    }
}
