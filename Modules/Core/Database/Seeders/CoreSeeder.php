<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;

class CoreSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $enFaker = \Faker\Factory::create('en_US');
        $ruFaker = \Faker\Factory::create('ru_RU');
        for ($i = 0; $i < 6; $i++) {
            app('zengine')->model('FaqItem')->create([
                'question'  => [
                    'ru'    => $ruFaker->realText(50),
                    'en'    => $enFaker->realText(50),
                ],
                'answer'    => [
                    'ru'    => $ruFaker->realText(500),
                    'en'    => $enFaker->realText(500),
                ],
            ]);
        }
        $this->call(CurrenciesDefaultSeeder::class);
        $this->call(InstallAdminSeeder::class);
        $this->call(SettingsSeeder::class);
        $this->call(PaymentSystemsDefaultSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PagesTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(PlansTableSeeder::class);
        $this->call(DepositsTableSeeder::class);
        $this->call(OperationsSeeder::class);
    }
}
