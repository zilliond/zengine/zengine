<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Models\Currency;

class CurrenciesDefaultSeeder extends Seeder
{
    protected $start_rates = [
        'USD_RUB' => 77,
        'USD_EUR' => 0.88,
        'USD_UAH' => 28,
        'USD_BTC' => 0.000029,
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app('zengine')->model('Currency')->create([
            'name'              => 'Рубль',
            'symbol'            => '₽',
            'code'              => 'rub',
            'code_iso'          => 'RUB',
            'auto_update_rate'  => 1,
            'update_provider'   => 'openexchangerates_free'
        ]);
        app('zengine')->model('Currency')->create([
            'name'               => 'Евро',
            'symbol'             => '€',
            'code'               => 'eur',
            'code_iso'           => 'EUR',
            'auto_update_rate'   => 1,
            'update_provider'    => 'openexchangerates_free'
        ]);
        app('zengine')->model('Currency')->create([
            'name'              => 'Гривна',
            'symbol'            => '₴',
            'code'              => 'UAH',
            'code_iso'          => 'UAH',
            'auto_update_rate'  => 1,
            'update_provider'   => 'openexchangerates_free'
        ]);
        app('zengine')->model('Currency')->create([
            'name'              => 'Bitcoin',
            'symbol'            => 'BTC',
            'code'              => 'BTC',
            'code_iso'          => 'BTC',
            'auto_update_rate'  => 1,
            'format_precision'  => 6,
            'update_provider'   => 'openexchangerates_free'
        ]);
        $this->generateReverseRates();
    }

    protected function generateReverseRates()
    {
        $currencies = app('zengine')->model('Currency')->get();
        foreach ($currencies as $from) {
            foreach ($currencies as $to) {
                /** @var Currency $from */
                /** @var Currency $to */
                $rate = 1;
                if ($from->code_iso === $to->code_iso) {
                    $rate = 1;
                } elseif (isset($this->start_rates["{$from->code_iso}_{$to->code_iso}"])) {
                    $rate = (float) $this->start_rates["{$from->code_iso}_{$to->code_iso}"];
                } elseif (isset($this->start_rates["{$to->code_iso}_{$from->code_iso}"])) {
                    $rate = 1 / (float) $this->start_rates["{$to->code_iso}_{$from->code_iso}"];
                } else {
                    $to_usd = app('zengine')->model('CurrencyRate')->where('currency_id', $from->id)->where('target_currency_id', 1)->first();
                    $from_usd = app('zengine')->model('CurrencyRate')->where('currency_id', 1)->where('target_currency_id', $to->id)->first();
                    if ($to_usd && $to_usd->rate && $from_usd && $from_usd->rate) {
                        $rate = $to_usd->rate * $from_usd->rate;
                    }
                }
                app('zengine')->model('CurrencyRate')->create([
                    'currency_id'        => $from->id,
                    'target_currency_id' => $to->id,
                    'rate'               => $rate
                ]);
                $this->command->info("Rate {$from->code_iso}-{$to->code_iso} generated - {$rate}");
            }
        }
    }
}
