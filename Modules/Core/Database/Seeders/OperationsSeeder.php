<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Core\Services\OperationService;

class OperationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Modules\Core\Exceptions\BalanceException
     * @throws \Modules\Core\Exceptions\BalanceTypeNotFindException
     * @throws \Modules\Core\Exceptions\OperationWalletInvalidException
     */
    public function run()
    {
        $operationService = app('operations');
        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 300; $i++) {
            $user = app('zengine')->model('User')->inRandomOrder()->first();
            /** @var \Modules\Core\Models\User $user */
            $currency = app('zengine')->model('Currency')->inRandomOrder()->first();
            $payment_system = app('zengine')->model('PaymentSystem')->inRandomOrder()->first();
            $wallet = $user->wallets()->inRandomOrder()->first();

            $usd = app('currencies')->getDefaultCurrency();
            $usd_amount = $faker->randomFloat(null, 20, 200);
            $amount = app('currencies')->convert($usd_amount, $usd, $currency);

            $operation = app('zengine')->model('Operation')->create([
                'user_id'           => $user->id,
                'amount'            => $amount,
                'currency_id'       => $currency->id,
                'wallet_id'         => optional($wallet)->id,
                'type'              => app('zengine')->modelClass('Operation')::TYPE_USER_REFILL,
                'status'            => app('zengine')->modelClass('Operation')::STATUS_SUCCESS,
                'payment_system_id' => optional($wallet)->id ? $wallet->payment_system_id : $payment_system->id,
                'created_at'        => $faker->dateTimeBetween('-2 years')
            ]);
            $user = app('zengine')->model('User')->inRandomOrder()->first();
            $currency = app('zengine')->model('Currency')->inRandomOrder()->first();
            $payment_system = app('zengine')->model('PaymentSystem')->inRandomOrder()->first();
            $operationService->typeByOperation($operation)->success($operation);

            $usd = app('currencies')->getDefaultCurrency();
            $usd_amount = $faker->randomFloat(null, 10, 100);
            $amount = app('currencies')->convert($usd_amount, $usd, $currency);

            $operation = app('zengine')->model('Operation')->create([
                'user_id'           => $user->id,
                'amount'            => $amount,
                'currency_id'       => $currency->id,
                'wallet_id'         => optional($wallet)->id,
                'type'              => app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW,
                'status'            => app('zengine')->modelClass('Operation')::STATUS_SUCCESS,
                'payment_system_id' => optional($wallet)->id ? $wallet->payment_system_id : $payment_system->id,
                'created_at'        => $faker->dateTimeBetween('-2 years')
            ]);
            $operationService->typeByOperation($operation)->success($operation);
        }
        $user = app('zengine')->model('User')->inRandomOrder()->first();
        /** @var \Modules\Core\Models\User $user */
        $currency = app('zengine')->model('Currency')->inRandomOrder()->first();
        $payment_system = app('zengine')->model('PaymentSystem')->inRandomOrder()->first();
        $wallet = $user->wallets()->inRandomOrder()->first();

        $usd = app('currencies')->getDefaultCurrency();
        $usd_amount = $faker->randomFloat(null, 20, 200);
        $amount = app('currencies')->convert($usd_amount, $usd, $currency);

        $operation = app('zengine')->model('Operation')->create([
            'user_id'           => $user->id,
            'amount'            => $amount,
            'currency_id'       => $currency->id,
            'wallet_id'         => optional($wallet)->id,
            'type'              => app('zengine')->modelClass('Operation')::TYPE_USER_REFILL,
            'status'            => app('zengine')->modelClass('Operation')::STATUS_SUCCESS,
            'payment_system_id' => optional($wallet)->id ? $wallet->payment_system_id : $payment_system->id,
            'created_at'        => now()
        ]);
        $user = app('zengine')->model('User')->inRandomOrder()->first();
        $currency = app('zengine')->model('Currency')->inRandomOrder()->first();
        $payment_system = app('zengine')->model('PaymentSystem')->inRandomOrder()->first();
        $operationService->typeByOperation($operation)->success($operation);
        \Log::info('Operation id', $operation->toArray());

        $usd = app('currencies')->getDefaultCurrency();
        $usd_amount = $faker->randomFloat(null, 10, 100);
        $amount = app('currencies')->convert($usd_amount, $usd, $currency);

        $operation = app('zengine')->model('Operation')->create([
            'user_id'           => $user->id,
            'amount'            => $amount,
            'currency_id'       => $currency->id,
            'wallet_id'         => optional($wallet)->id,
            'type'              => app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW,
            'status'            => app('zengine')->modelClass('Operation')::STATUS_SUCCESS,
            'payment_system_id' => optional($wallet)->id ? $wallet->payment_system_id : $payment_system->id,
            'created_at'        => now()
        ]);
        $operationService->typeByOperation($operation)->success($operation);
        \Log::info('Operation id', $operation->toArray());
    }
}
