<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = factory(app('zengine')->modelClass('Post'), 20)->create();
        $this->command->info($posts->count().' Posts created.');
    }
}
