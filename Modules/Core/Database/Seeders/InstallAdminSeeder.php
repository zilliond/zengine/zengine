<?php

namespace Modules\Core\Database\Seeders;

use Illuminate\Database\Seeder;

class InstallAdminSeeder extends Seeder
{
    public function run()
    {
        app('zengine')->model('User')->create([
            'name'      => env('ADMIN_NAME', 'Admin'),
            'email'     => env('ADMIN_EMAIL', 'admin@zillion.design'),
            'login'     => env('ADMIN_LOGIN', 'admin'),
            'password'  => bcrypt(env('ADMIN_PASSWORD', 'password')),
            'role'      => app('zengine')->modelClass('User')::ROLE_ADMIN
        ]);
    }
}
