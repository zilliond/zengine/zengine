<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(app('zengine')->modelClass('Deposit'), function (Faker $faker) {
    $plan = app('zengine')->model('Plan')->inRandomOrder()->first();
    $user = app('zengine')->model('User')->inRandomOrder()->first();
    $currency = app('zengine')->model('Currency')->inRandomOrder()->first();
    $currencyService = app('currencies');

    $wallet = null;
    if (setting('balances_type') === 'divided') {
        $wallet = $user->wallets()->inRandomOrder()->first();
    }
    if (!$plan || !$user || !$currency) {
        throw new DomainException('User or Plan or Currency not exist');
    }
    return [
        'amount' => $faker->randomFloat(
            2,
            $currency->id === $currencyService->getDefaultCurrency()->id ? $plan->min_amount : $currencyService->convert($plan->min_amount, $currencyService->getDefaultCurrency(), $currency),
            $currency->id === $currencyService->getDefaultCurrency()->id ? $plan->max_amount : $currencyService->convert($plan->max_amount, $currencyService->getDefaultCurrency(), $currency)
        ),
        'created_at'  => $faker->dateTimeBetween('-2 years'),
        'updated_at'  => now(),
        'currency_id' => $currency->id,
        'plan_id'     => $plan->id,
        'user_id'     => $user->id,
        'wallet_id'   => optional($wallet)->id,
    ];
});
