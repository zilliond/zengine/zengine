<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use DavidBadura\FakerMarkdownGenerator\FakerProvider as MarkdownProvider;
use Faker\Generator as Faker;

$factory->define(app('zengine')->modelClass('Post'), function (Faker $faker) {
    $faker->addProvider(new MarkdownProvider($faker));
    return [
        'title'        => ['ru' => $faker->word, 'en' => $faker->word],
        'description'  => ['ru' => \Illuminate\Support\Str::limit($faker->paragraph, 250), 'en' => \Illuminate\Support\Str::limit($faker->paragraph, 250)],
        'keywords'     => $faker->word,
        'body'         => ['ru' => $faker->markdown() . "\n\n". MarkdownProvider::markdownInlineImg(), 'en' => $faker->markdown() . "\n\n". MarkdownProvider::markdownInlineImg()],
        'is_important' => $faker->randomElement([0,1]),
        'file_url'     => $faker->imageUrl(640, 480, null, false),
        'created_at'   => $faker->date('Y-m-d H:i:s'),
        'updated_at'   => $faker->date('Y-m-d H:i:s')
    ];
});
