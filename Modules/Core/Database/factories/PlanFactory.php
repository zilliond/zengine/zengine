<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(app('zengine')->modelClass('Plan'), function (Faker $faker) {
    $limit = $faker->randomElement([0, 3]) ? 0 : $faker->numberBetween(10, 100);

    return [
        'name'                        => $faker->word,
        'min_amount'                  => $faker->numberBetween(1, 10),
        'max_amount'                  => $faker->numberBetween(10, 100),
        'percent'                     => $faker->randomFloat(4, 1, 20),
        'type'                        => $faker->randomElement(array_keys(app('zengine')->model('Plan')->types())),
        'duration'                    => $faker->numberBetween(2, 30),
        'amount_bonus'                => 0,
        'balance_bonus'               => 0,
        'not_charge_weekends'         => $faker->randomElement([0,1]),
        'additional_referral_percent' => null,
        'is_limited'                  => $limit ? 1 : 0,
        'max_deposits'                => $limit,
        'currency_id'                 => 1,
        'can_close'                   => 1,
        'return_percent'              => $faker->numberBetween(0, 100),
        'created_at'                  => $faker->date('Y-m-d H:i:s'),
        'updated_at'                  => $faker->date('Y-m-d H:i:s')
    ];
});
