<?php


namespace Modules\Core\CurrencyRateUpdaters;

use GuzzleHttp\Client;
use Illuminate\Support\Str;

class BloombergRapidCurrencyController extends BaseCurrencyProvider
{
    /**
     * @inheritDoc
     */
    public static function getAvailableCurrencies() : array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getRates(array $codes, string $base_code = null) : array
    {
        $pairs = $this->fetchRates($codes);
        return $this->prepareResponse($pairs);
    }

    public function fetchRates(array $codes) : array
    {
        $lower_codes = collect($codes)->map(function ($code) {
            return Str::lower($code);
        });
        if (!$lower_codes->has('usd')) {
            $lower_codes->push('usd');
        }
        $query_id = $lower_codes->join(',');
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://bloomberg-market-and-financial-news.p.rapidapi.com',
            // You can set any number of default request options.
            'timeout'  => 5,
        ]);

        $request = $client->get('market/get-cross-currencies', [
            'query'   => [ 'id' => $query_id ],
            'headers' => $this->getRequestHeaders()
        ]);
        $json = $request->getBody()->getContents();
        $data = json_decode($json, true);
        $result = collect($data['result']);
        \Log::info('BloombergRapidCurrencyController: '.$result->count().' currency exchange pairs received');
        return $result->toArray();
    }

    public function prepareResponse(array $result) : array
    {
        $pairs = [];
        foreach ($result as $item) {
            $pair_key = Str::upper($item['sourceCurrency']) . '_' . Str::upper($item['currency']);
            $pairs[$pair_key] = (float) $item['last'];
        }
        return $pairs;
    }

    public function getRequestHeaders()
    {
        return [
            'x-rapidapi-host' => 'bloomberg-market-and-financial-news.p.rapidapi.com',
            'x-rapidapi-key'  => config('zengine.currency_updater.bloomberg_rapid.api_key')
        ];
    }

    /**
     * @inheritDoc
     */
    public static function getName()
    {
        return 'Bloomberg Market (RapidApi)';
    }

    /**
     * @inheritDoc
     */
    public static function getAlias()
    {
        return 'bloomberg_rapid';
    }

    /**
     * @inheritDoc
     */
    public static function getSupportedCurrencyTypes() : array
    {
        return [app('zengine')->model('Currency')->TYPE_FIAT];
    }
}
