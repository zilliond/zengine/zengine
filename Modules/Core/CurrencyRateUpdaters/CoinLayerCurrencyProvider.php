<?php


namespace Modules\Core\CurrencyRateUpdaters;

use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class CoinLayerCurrencyProvider extends BaseCurrencyProvider
{

    /**
     * @inheritDoc
     */
    public static function getAvailableCurrencies() : array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public static function getSupportedCurrencyTypes() : array
    {
        return [app('zengine')->model('Currency')->TYPE_FIAT, app('zengine')->model('Currency')->TYPE_CRYPTO];
    }

    /**
     * @inheritDoc
     */
    public function getRates(array $codes, string $base_code = null) : array
    {
        if ($base_code) {
            $codes[] = $base_code;
        } else {
            $codes[] = 'USD';
        }
        \Log::info('codes');
        \Log::info($codes);
        $result = [];
        foreach ($codes as $code) {
            $rates = $this->fetchRates($code);
            $result += $this->prepareResult($rates, $codes, $code);
        }
        return $result;
    }

    public function fetchRates(string $target = 'USD') : Collection
    {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'http://api.coinlayer.com/',
            // You can set any number of default request options.
            'timeout'  => 2.0,
        ]);
        $request = $client->get('live', [
            'query' => [
                'access_key' => config('zengine.currency_updater.coin_layer.api_key'),
                'target'     => $target
            ]
        ]);
        $json = $request->getBody()->getContents();
        $data = json_decode($json, true);

        $rates = collect($data['rates']);
        \Log::info('CoinLayerCurrencyProvider: '.$rates->count().' currency exchange pairs received');
        return $rates;
    }

    public function prepareResult(Collection $rates, array $codes, string $target_code) : array
    {
        $result = [];
        $iso_codes = collect($codes)->map(function ($code) {
            return Str::upper($code);
        })->toArray();
        $rates = $rates->only($iso_codes);
        foreach ($rates as $code => $rate) {
            $key = Str::upper($code) . '_' . Str::upper($target_code);
            $result[$key] = (float) $rate;
        }
        return $result;
    }

    /**
     * @inheritDoc
     */
    public static function getName()
    {
        return 'CoinLayer';
    }

    /**
     * @inheritDoc
     */
    public static function getAlias()
    {
        return 'coin_layer';
    }
}
