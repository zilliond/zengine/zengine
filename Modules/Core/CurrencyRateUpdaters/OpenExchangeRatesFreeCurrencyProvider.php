<?php


namespace Modules\Core\CurrencyRateUpdaters;

use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class OpenExchangeRatesFreeCurrencyProvider extends BaseCurrencyProvider
{
    /**
     * @inheritDoc
     */
    public function getRates(array $codes, string $base_code = null) : array
    {
        if ($base_code && Str::upper($base_code) !== 'USD') {
            return [];
        }
        $base_code = 'USD';
        $rates = $this->fetchRates($base_code);
        return $this->prepareResult($rates, $codes, $base_code, true);
    }

    public function prepareResult(Collection $rates, array $codes, string $base_code, bool $reverse_pair = false) : array
    {
        $result = [];
        $iso_codes = collect($codes)->map(function ($code) {
            return Str::upper($code);
        })->toArray();
        $rates = $rates->only($iso_codes);
        foreach ($rates as $code => $rate) {
            $key = Str::upper($base_code) . '_' . Str::upper($code);
            $result[$key] = (float) $rate;
            if ($reverse_pair) {
                $reverse_key = Str::upper($code) . '_' . Str::upper($base_code);
                $reverse_rate = (float) (1 / $rate);
                $result[$reverse_key] = $reverse_rate;
            }
        }
        return $result;
    }

    public function fetchRates(string $base_code) : Collection
    {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://openexchangerates.org/api/',
            // You can set any number of default request options.
            'timeout'  => 2.0,
        ]);
        $request = $client->get('latest.json', [
            'query' => [
                'app_id'            => config('zengine.currency_updater.openexchangerates.client_id'),
                'show_alternative'  => 1,
                'base'              => $base_code
            ],
            'timeout' => 60
        ]);
        $json = $request->getBody()->getContents();
        $data = json_decode($json, true);

        $rates = collect($data['rates']);
        \Log::info('OpenExchangeCurrencyProvider: '.$rates->count().' currency exchange pairs received');
        return $rates;
    }

    /**
     * Getting name of Currency Provider
     * @return string
     */
    public static function getName()
    {
        return 'OpenExchangeRates Free';
    }

    /**
     * Getting alias of Currency Provider
     * @return string
     */
    public static function getAlias()
    {
        return 'openexchangerates_free';
    }

    /**
     * @inheritDoc
     */

    public static function getAvailableCurrencies() : array
    {
        return ["AED", "AFN", "ALL", "AMD", "ANG", "AOA", "ARS", "AUD", "AWG", "AZN", "BAM", "BBD", "BDT", "BGN", "BHD", "BIF", "BMD", "BND", "BOB", "BRL", "BSD", "BTC", "BTN", "BTS", "BWP", "BYN", "BZD", "CAD", "CDF", "CHF", "CLF", "CLP", "CNH", "CNY", "COP", "CRC", "CUC", "CUP", "CVE", "CZK", "DASH", "DJF", "DKK", "DOGE", "DOP", "DZD", "EAC", "EGP", "EMC", "ERN", "ETB", "ETH", "EUR", "FCT", "FJD", "FKP", "FTC", "GBP", "GEL", "GGP", "GHS", "GIP", "GMD", "GNF", "GTQ", "GYD", "HKD", "HNL", "HRK", "HTG", "HUF", "IDR", "ILS", "IMP", "INR", "IQD", "IRR", "ISK", "JEP", "JMD", "JOD", "JPY", "KES", "KGS", "KHR", "KMF", "KPW", "KRW", "KWD", "KYD", "KZT", "LAK", "LBP", "LD", "LKR", "LRD", "LSL", "LTC", "LYD", "MAD", "MDL", "MGA", "MKD", "MMK", "MNT", "MOP", "MRO", "MRU", "MUR", "MVR", "MWK", "MXN", "MYR", "MZN", "NAD", "NGN", "NIO", "NMC", "NOK", "NPR", "NVC", "NXT", "NZD", "OMR", "PAB", "PEN", "PGK", "PHP", "PKR", "PLN", "PPC", "PYG", "QAR", "RON", "RSD", "RUB", "RWF", "SAR", "SBD", "SCR", "SDG", "SEK", "SGD", "SHP", "SLL", "SOS", "SRD", "SSP", "STD", "STN", "STR", "SVC", "SYP", "SZL", "THB", "TJS", "TMT", "TND", "TOP", "TRY", "TTD", "TWD", "TZS", "UAH", "UGX", "USD", "UYU", "UZS", "VEF", "VEF_BLKMKT", "VEF_DICOM", "VEF_DIPRO", "VES", "VND", "VTC", "VUV", "WST", "XAF", "XAG", "XAU", "XCD", "XDR", "XMR", "XOF", "XPD", "XPF", "XPM", "XPT", "XRP", "YER", "ZAR", "ZMW", "ZWL"];
    }

    /**
     * @inheritDoc
     */
    public static function getSupportedCurrencyTypes() : array
    {
        return [app('zengine')->model('Currency')->TYPE_FIAT, app('zengine')->model('Currency')->TYPE_CRYPTO];
    }
}
