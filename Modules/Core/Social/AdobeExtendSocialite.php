<?php


namespace Modules\Core\Social;

use SocialiteProviders\Manager\SocialiteWasCalled;

class AdobeExtendSocialite
{
    /**
     * Execute the provider.
     * @param  SocialiteWasCalled  $socialiteWasCalled
     */
    public function handle(SocialiteWasCalled $socialiteWasCalled)
    {
        $socialiteWasCalled->extendSocialite('adobe', AdobeProvider::class);
    }
}
