<?php


namespace Modules\Core\Social;

use SocialiteProviders\InstagramBasic\Provider;
use SocialiteProviders\Manager\SocialiteWasCalled;

class InstagramExtendSocialite
{
    /**
     * Execute the provider.
     * @param  SocialiteWasCalled  $socialiteWasCalled
     */
    public function handle(SocialiteWasCalled $socialiteWasCalled)
    {
        $socialiteWasCalled->extendSocialite('instagram', Provider::class);
    }
}
