<?php

namespace Modules\Core\Http\Middleware;

use Illuminate\Foundation\Http\Exceptions\MaintenanceModeException;
use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;
use Illuminate\Support\Carbon;

class CheckForMaintenanceMode extends Middleware
{
    /**
     * The URIs that should be reachable while maintenance mode is enabled.
     *
     * @var array
     */
    protected $except = [
        '/login',
        '/admin',
        '/admin/*',
        '/api/*',
    ];

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function handle($request, \Closure $next)
    {
        $enabled = (bool) setting('general.enabled', true);
        if (!$enabled) {
            if ($this->inExceptArray($request)) {
                return $next($request);
            }

            throw new MaintenanceModeException(Carbon::now()->getTimestamp(), null, '');
        }

        return $next($request);
    }
}
