<?php

namespace Modules\Core\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Modules\Core\Http\Controllers\Controller;
use Modules\Core\Models\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * @param  Request  $request
     * @param  User  $user
     * @throws ValidationException
     * @throws \PragmaRX\Google2FA\Exceptions\IncompatibleWithGoogleAuthenticatorException
     * @throws \PragmaRX\Google2FA\Exceptions\InvalidCharactersException
     * @throws \PragmaRX\Google2FA\Exceptions\SecretKeyTooShortException
     */
    protected function authenticated(Request $request, User $user) : void
    {
        if (setting('users.2fa') !== 'disabled') {
            if ($user->google2fa_secret) {
                if (!$code = $request->get(config('google2fa.otp_input'))) {
                    \Auth::logout();
                    throw ValidationException::withMessages([
                        config('google2fa.otp_input') => [config('google2fa.error_messages.cannot_be_empty')],
                    ]);
                };
                if (!\Google2FA::verifyKey($user->google2fa_secret, $code)) {
                    \Auth::logout();
                    throw ValidationException::withMessages([
                        config('google2fa.otp_input') => [config('google2fa.error_messages.wrong_otp')],
                    ]);
                }
            }
        }
        if ((bool) setting('general.log_ip', true)) {
            $user->fill([
                'last_login_at' => now()->toDateTimeString(),
                'last_login_ip' => $request->getClientIp()
            ]);
            $user->country_code = $user->last_login_country_code;
            $user->auth_logs()->create([
                'ip'           => $request->getClientIp(),
                'country_code' => $user->last_login_country_code
            ]);
        }
        $user->save();
    }

    public function username()
    {
        return config('zengine.auth.username');
    }
}
