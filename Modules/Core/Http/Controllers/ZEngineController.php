<?php


namespace Modules\Core\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\Core\Exceptions\PaymentSystemException;
use Modules\Core\Models\Currency;
use Modules\Core\Services\Contracts\CurrencyServiceInterface;
use Modules\Core\Services\Contracts\CurrencyUpdaterServiceInterface;
use Modules\Core\Services\OperationService;

class ZEngineController extends Controller
{
    /**
     * Handler for all payment systems notifications
     *
     * @param  Request  $request
     * @return mixed
     * @throws PaymentSystemException
     */
    public function ipn_handle(Request $request)
    {
        \Log::channel('payment_systems')->info('Zengine|handler|start', print_r(request()->all(), 1));
        /** @var OperationService $operationService */
        $operationService = app('operations');
        try {
            return $operationService->handle($request);
        } catch (PaymentSystemException $e) {
            \Log::channel('payment_systems')->info("Zengine|handler|error: {$e->getMessage()}", print_r(request()->all(), 1));
            throw $e;
        }
    }

    /**
     * @return array
     */
    public function engine() : array
    {
        return [
            'engine'    => 'zengine',
            'version'   => config('app.version'),
        ];
    }

    /**
     * @return string
     */
    public function version() : string
    {
        return config('app.version');
    }

    /**
     * @return array
     */
    public function licence() : array
    {
        // TODO: secret token for confirm
        return [
            'active'    => true,
            'expires'   => Carbon::now()->addYear()
        ];
    }

    public function update(Currency $currency, CurrencyUpdaterServiceInterface $currencyService)
    {
        $currency = $currencyService->updateRate($currency);
        return [
            'status'    => 'success',
            'currency'  => $currency
        ];
    }

    public function update_all(CurrencyUpdaterServiceInterface $currencyService)
    {
        $currencyService->updateAllRates();
        return [
            'status'        => 'success',
            'currencies'    => app('zengine')->model('Currency')->all()
        ];
    }

    public function currencies(CurrencyServiceInterface $currencyService)
    {
        return [
            'default'    => $currencyService->getDefaultCurrency(),
            'currencies' => $currencyService->getCurrencies(),
            'rates'      => app('zengine')->model('CurrencyRate')->all(),
            'settings'   => [
                'amount' => setting('currencies.amount')
            ]
        ];
    }
}
