<?php

namespace Modules\Core\Observers;

use Modules\Core\Models\Deposit;

class DepositObserver
{
    public function creating(Deposit $deposit)
    {
        \Log::info('Deposit created observer');
        $service = app('deposits');
        $service->initAccruals($deposit);
    }

    public function created(Deposit $deposit)
    {
        $currencyService = app('currencies');
        $amount = $currencyService->convert($deposit->amount, $deposit->currency, $deposit->user->currency);
        $deposit->user->invested += $amount;
        $deposit->user->save();
        $inviters = app('zengine')->model('Referral')->with('user')->where('referral_id', $deposit->user_id)->get();
        foreach ($inviters as $inviter) {
            $inviter->user->structure_invested += $amount;
            $inviter->user->save();
        }
    }
}
