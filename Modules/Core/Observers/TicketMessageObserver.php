<?php

namespace Modules\Core\Observers;

use Modules\Core\Models\TicketMessage;

class TicketMessageObserver
{
    public function created(TicketMessage $ticketMessage)
    {
        $ticketMessage->ticket->last_message_at = $ticketMessage->created_at;
        $ticketMessage->ticket->save();
    }
}
