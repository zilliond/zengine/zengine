<?php


namespace Modules\Core\Observers;

use Modules\Core\Models\User;
use Modules\Core\Services\Contracts\CurrencyServiceInterface;

class UserObserver
{
    /**
     * @var CurrencyServiceInterface
     */
    private $currencyService;

    public function __construct()
    {
        $this->currencyService = app('currencies');
    }

    public function creating(User $user)
    {
        if ($user->inviter) {
            $user->ref_level = $user->inviter->ref_level + 1;
        }
    }

    public function created(User $user)
    {
        if ($user->inviter) {
            $this->recursiveUpdateRefsStats($user->inviter);
        }
    }

    public function updating(User $user)
    {
        if ($user->isDirty('currency_id') &&
            $user->getOriginal('currency_id') !== (int) $user->currency_id &&
            $from_currency = app('zengine')->model('Currency')->find($user->getOriginal('currency_id'))
        ) {
            $user->balance = $this->currencyService->convert($user->balance, $from_currency, $user->currency);
        }
    }

    protected function recursiveUpdateRefsStats(User $user)
    {
        $stat = app('zengine')->model('Stat')->firstOrNew(
            ['key' => 'USER_REFERRALS_COUNT_'.$user->id],
            ['value' => 0]
        );
        $stat->value += 1;
        $stat->save();
        if ($user->inviter) {
            $this->recursiveUpdateRefsStats($user->inviter);
        }
    }
}
