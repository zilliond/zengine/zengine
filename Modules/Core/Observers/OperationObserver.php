<?php


namespace Modules\Core\Observers;

use App\Stat;
use Modules\Core\Events\OperationCreatedNotification;
use Modules\Core\Events\OperationStatusUpdatedNotification;
use Modules\Core\Events\OperationUpdated;
use Modules\Core\Models\Operation;
use Modules\Core\Services\Contracts\CurrencyServiceInterface;
use Modules\Core\Services\OperationService;

class OperationObserver
{
    /**
     * @var OperationService
     */
    private $operationService;

    /**
     * @var CurrencyServiceInterface
     */
    private $currencyService;

    public function __construct()
    {
        $this->operationService = app('operations');
        $this->currencyService = app('currencies');
    }

    public function created(Operation $operation)
    {
        event(new OperationCreatedNotification($operation));
    }

    public function updating(Operation $operation)
    {
        if ($operation->isDirty('status')) {
            $operation->status_changed_at = now();
        }
    }

    public function updated(Operation $operation)
    {
        if ($operation->isDirty()) {
            event(new OperationUpdated($operation));
        }
        if ($operation->isDirty('status')) {
            event(new OperationStatusUpdatedNotification($operation));
        }
    }
}
