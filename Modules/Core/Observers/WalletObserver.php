<?php

namespace Modules\Core\Observers;

use Modules\Core\Models\User;
use Modules\Core\Models\Wallet;

class WalletObserver
{
    public function created(Wallet $wallet)
    {
        if (setting('balances_type') === 'divided') {
            $this->recalculateBalance($wallet->user);
        }
    }

    public function updated(Wallet $wallet)
    {
        if (setting('balances_type') === 'divided') {
            $this->recalculateBalance($wallet->user);
        }
    }

    protected function recalculateBalance(User $user)
    {
        $currencyService = app('currencies');
        $user->loadMissing(['wallets.payment_system.currency']);
        $total_balance = $user->wallets->reduce(static function ($total, Wallet $wallet) use ($currencyService) {
            return $total + $currencyService->convertToDefault($wallet->balance, $wallet->payment_system->currency);
        }, 0);
        $user->balance = $total_balance;
        $user->save();
    }
}
