<?php

namespace Modules\Core\Providers;

use Modules\Core\Models\Policies\DepositPolicy;
use Modules\Core\Models\Policies\UserPolicy;
use Modules\Core\Models\Policies\WalletPolicy;
use Modules\Core\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        \Gate::policy(app('zengine')->modelClass('User'), UserPolicy::class);

        \Gate::define('admin_panel', 'Modules\Core\Models\Policies\UserPolicy@admin_panel');
        \Gate::define('is-admin', 'Modules\Core\Models\Policies\UserPolicy@admin');
        \Gate::define('unlocked', 'Modules\Core\Models\Policies\UserPolicy@unlocked');
        \Gate::define('locked', 'Modules\Core\Models\Policies\UserPolicy@locked');

        \Gate::define('can-modify', 'Modules\Core\Models\Policies\WalletPolicy@can_modify');
        \Gate::define('close', 'Modules\Core\Models\Policies\DepositPolicy@close');
        //
    }
}
