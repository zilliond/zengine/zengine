<?php

namespace Modules\Core\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Modules\Core\Social\AdobeExtendSocialite;
use Modules\Core\Social\GoogleExtendSocialite;
use Modules\Core\Social\InstagramExtendSocialite;
use Modules\Core\Social\MailruExtendSocialite;
use Modules\Core\Social\MicrosoftExtendSocialite;
use SocialiteProviders\Facebook\FacebookExtendSocialite;
use SocialiteProviders\LinkedIn\LinkedInExtendSocialite;
use SocialiteProviders\Manager\SocialiteWasCalled;
use SocialiteProviders\VKontakte\VKontakteExtendSocialite;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        SocialiteWasCalled::class => [
            FacebookExtendSocialite::class,
            VKontakteExtendSocialite::class,
            MailruExtendSocialite::class,
            GoogleExtendSocialite::class,
            MicrosoftExtendSocialite::class,
            InstagramExtendSocialite::class,
            LinkedInExtendSocialite::class,
            AdobeExtendSocialite::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
