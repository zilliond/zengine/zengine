<?php

namespace Modules\Core\Providers;

use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;
use Modules\Core\Services\Contracts\CurrencyServiceInterface;
use Modules\Core\Services\Contracts\CurrencyUpdaterServiceInterface;
use Modules\Core\Services\Contracts\DepositServiceInterface;
use Modules\Core\Services\Contracts\OperationServiceInterface;
use Modules\Core\Services\Contracts\SocialAuthServiceInterface;
use Modules\Core\Services\Contracts\UserServiceInterface;

class BindingsServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register services.
     */
    public function register()
    {
        $this->app->singleton(
            CurrencyServiceInterface::class,
            config('zengine.services.currencies')
        );
        $this->app->singleton(
            OperationServiceInterface::class,
            config('zengine.services.operations')
        );
        $this->app->singleton(
            DepositServiceInterface::class,
            config('zengine.services.deposits')
        );
        $this->app->singleton(
            UserServiceInterface::class,
            config('zengine.services.users')
        );
        $this->app->singleton(
            SocialAuthServiceInterface::class,
            config('zengine.services.social_auth')
        );
        $this->app->singleton(
            CurrencyUpdaterServiceInterface::class,
            config('zengine.services.currency-updater')
        );
        $this->app->alias(CurrencyServiceInterface::class, 'currencies');
        $this->app->alias(DepositServiceInterface::class, 'deposits');
        $this->app->alias(OperationServiceInterface::class, 'operations');
        $this->app->alias(UserServiceInterface::class, 'users');
        $this->app->alias(SocialAuthServiceInterface::class, 'social_auth');
        $this->app->alias(CurrencyUpdaterServiceInterface::class, 'currency-updater');
    }

    /**
     * Bootstrap services.
     */
    public function boot()
    {
        //
    }

    public function provides()
    {
        return [
            CurrencyServiceInterface::class,
            DepositServiceInterface::class,
            OperationServiceInterface::class,
            UserServiceInterface::class,
            SocialAuthServiceInterface::class,
            CurrencyUpdaterServiceInterface::class,
        ];
    }
}
