<?php

namespace Modules\Core\Console\Commands;

use Illuminate\Console\Command;
use Modules\Core\Models\Currency;

class UpdateCurrenciesRates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'currencies:rates-update
                            {code? : Currency code for update rate}
                            {--provider= : Provider for getting rates}
                            {--base-code= : Base currency code(for custom pair)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update currencies rates';

    /**
     * @var \Modules\Core\Services\\Modules\Currencies\Services\Contracts\CurrencyUpdaterService
     */
    private $currencyService;

    public function __construct(\Modules\Core\Services\Contracts\CurrencyUpdaterServiceInterface $currencyService)
    {
        parent::__construct();
        $this->currencyService = $currencyService;
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        if ($provider = $this->option('provider')) {
            $this->currencyService->setProvider($provider);
        }
        /*
        if($base_code = $this->option('base-code')){
            $this->currencyService->setBaseCurrencyCode($base_code);
            \Log::info(json_encode($this->currencyService->getProvider()->getRates()));
        }
        */
        if ($code = $this->argument('code')) {
            $currency = $this->currencyService->updateRateByCode($code);
            if ($currency->isDirty('rate')) {
                $this->info("Currencies: {$currency->code_iso} rate updated, new rate - {$currency->rate}");
            } else {
                $this->info("Currencies: {$currency->code_iso} rate not beed updated");
            }
        } else {
            $currencies = $this->currencyService->updateAllRates();
            foreach ($currencies as $currency) {
                /* @var Currency $currency */
                if ($currency->isDirty('rate')) {
                    $this->info("Currencies: {$currency->code_iso} rate updated, new rate - {$currency->rate}");
                } else {
                    $this->info("Currencies: {$currency->code_iso} rate not beed updated");
                }
            }
        }
        $this->info('Currencies updated');
    }

    public function info($string, $verbosity = null)
    {
        parent::info($string, $verbosity);
        \Log::info($string);
    }
}
