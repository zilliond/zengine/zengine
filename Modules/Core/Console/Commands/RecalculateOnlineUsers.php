<?php

namespace Modules\Core\Console\Commands;

use Illuminate\Console\Command;

class RecalculateOnlineUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'zengine:users:online';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recalculate online users from cache and save to cache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users_ids = app('zengine')->model('User')->pluck('id');
        $count = 0;
        foreach ($users_ids as $id) {
            if (\Cache::has('user-is-online-'.$id)) {
                ++$count;
            }
        }
        $this->info('Current online users: '.$count);
        \Cache::put('online-users', $count, now()->addMinutes(5));
    }
}
