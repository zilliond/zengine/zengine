<?php

namespace Modules\Core\Services\Contracts;

use Illuminate\Http\Request;
use Modules\Core\Models\Operation;
use Modules\Core\OperationTypes\OperationType;
use Modules\Core\Payments\PaymentSystemProvider;

interface OperationServiceInterface
{
    public function typeByOperation(Operation $operation) : OperationType;

    public function handle(Request $request);

    public function getOperationType(string $type) : OperationType;

    public function setOperationType(string $type, string $class) : OperationServiceInterface;

    public function getProvider(string $alias) : ?PaymentSystemProvider;

    public function setProvider(string $key, string $providerClass) : OperationServiceInterface;

    public function getProviders() : array;

    public function getProvidersFields() : array;

    public function parsePercentsString($string) : array;

    public function preparePercents(array $percents) : string;

    public function referralPay(Operation $operation) : void;
}
