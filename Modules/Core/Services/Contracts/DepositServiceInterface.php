<?php


namespace Modules\Core\Services\Contracts;


use Modules\Core\Models\Deposit;
use Modules\Core\Models\Plan;

interface DepositServiceInterface
{
    public function create(Deposit $deposit, $force = false);
    public function initAccruals(Deposit $deposit);
    public function processAccrual(Deposit $deposit);
    public function adminPlanAccrual(Plan $plan, float $percent);
    public function close(Deposit $deposit);
}
