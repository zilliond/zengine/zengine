<?php


namespace Modules\Core\Models\Traits;

use Illuminate\Contracts\Encryption\DecryptException;

/**
 * Trait Encryptable
 * @package App
 * @mixin \Eloquent
 *
 */
trait Encryptable
{
    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);

        if (in_array($key, $this->encryptable)) {
            try {
                $value = \Crypt::decrypt($value);
            } catch (DecryptException $e) {
                $value = null;
            }
        }

        return $value;
    }

    public function setAttribute($key, $value)
    {
        if (in_array($key, $this->encryptable)) {
            $value = \Crypt::encrypt($value);
        }

        return parent::setAttribute($key, $value);
    }
}
