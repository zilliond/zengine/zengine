<?php

namespace Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Modules\Core\Models\Ticket
 *
 * @property int $id
 * @property int $user_id
 * @property string $subject
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $new_admin_messages
 * @property-read \Illuminate\Database\Eloquent\Collection $new_messages
 * @property-read mixed $new_user_messages
 * @property-read bool $read
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Models\TicketMessage[] $messages
 * @property-read int|null $messages_count
 * @property-read \Modules\Core\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Ticket newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Ticket newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Ticket query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Ticket whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Ticket whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Ticket whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Ticket whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Ticket whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Ticket whereUserId($value)
 * @mixin \Eloquent
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\Ticket onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Ticket whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\Ticket withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\Ticket withoutTrashed()
 * @property \Illuminate\Support\Carbon $last_message_at
 * @method static \Illuminate\Database\Eloquent\Builder|Ticket whereLastMessageAt($value)
 */
class Ticket extends Model
{
    use SoftDeletes;

    public const STATUS_OPEN = 'open';
    public const STATUS_CLOSE = 'close';

    protected $fillable = [
        'user_id',
        'subject',
        'status',
        'last_message_at',
    ];

    protected $casts = [
        'last_message_at' => 'datetime',
    ];

    protected $appends = [
        'read'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(app('zengine')->modelClass('User'));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(app('zengine')->modelClass('TicketMessage'));
    }

    /**
     * @return bool
     */
    public function getReadAttribute()
    {
        if ($this->relationLoaded('messages')) {
            return !$this->messages->where('user_id', \Auth::user()->is_admin ? '=' : '!=', $this->user_id)
                ->where('read', '=', 0)->count();
        }
        return !$this->messages()->where('user_id', \Auth::user()->is_admin ? '=' : '!=', $this->user_id)
            ->where('read', '=', 0)->count();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getNewMessagesAttribute()
    {
        $this->loadMissing('messages');
        return $this->messages->where('read', '=', 0);
    }

    /**
     * @return mixed
     */
    public function getNewAdminMessagesAttribute()
    {
        return $this->new_messages->where('user_id', '!=', $this->user_id);
    }

    /**
     * @return mixed
     */
    public function getNewUserMessagesAttribute()
    {
        return $this->new_messages->where('user_id', '==', $this->user_id);
    }
}
