<?php

namespace Modules\Core\Models\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Core\Models\Deposit;
use Modules\Core\Models\User;

class DepositPolicy
{
    use HandlesAuthorization;

    /**
     * @param  User  $user
     * @param  Deposit  $deposit
     * @return bool
     */
    public function close(User $user, Deposit $deposit) : bool
    {
        return $deposit->plan->can_close && $deposit->status === app('zengine')->modelClass('Deposit')::STATUS_OPEN && $deposit->left_accruals > 0;
    }
}
