<?php

namespace Modules\Core\Models\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Core\Models\User;

class UserPolicy
{
    use HandlesAuthorization;

    public function admin(User $user)
    {
        return $user->is_admin;
    }

    public function admin_panel(User $user)
    {
        return $user->is_admin;
    }

    public function unlocked(User $user)
    {
        return $user->unlocked();
    }

    public function locked(User $user)
    {
        return !$user->unlocked();
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \Modules\Core\Models\User  $user
     * @param  User  $model
     * @return mixed
     */
    public function view(?User $user, User $model)
    {
        return true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  User  $user
     * @return mixed
     */
    public function create(?User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  User  $user
     * @param  \Modules\Core\Models\User  $model
     * @return mixed
     */
    public function update(User $user, User $model)
    {
        return $this->admin($user) || $user->id === $model->id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  User  $user
     * @param  User  $model
     * @return mixed
     */
    public function delete(User $user, User $model)
    {
        return $this->admin($user);
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \Modules\Core\Models\User  $user
     * @param  User  $model
     * @return mixed
     */
    public function restore(User $user, User $model)
    {
        return $this->admin($user);
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  User  $user
     * @param  User  $model
     * @return mixed
     */
    public function forceDelete(User $user, User $model)
    {
        return $this->admin($user);
    }
}
