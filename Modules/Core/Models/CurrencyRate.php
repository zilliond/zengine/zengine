<?php

namespace Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\CurrencyRate
 *
 * @property int $id
 * @property int $currency_id
 * @property int $target_currency_id
 * @property float $rate
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Modules\Core\Models\Currency $currency
 * @property-read \Modules\Core\Models\Currency $target_currency
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CurrencyRate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CurrencyRate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CurrencyRate query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CurrencyRate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CurrencyRate whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CurrencyRate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CurrencyRate whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CurrencyRate whereTargetCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CurrencyRate whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CurrencyRate whereType($value)
 */
class CurrencyRate extends Model
{
    const TYPE_API = 'api';
    const TYPE_FIXED = 'fixed';

    protected $fillable = [
        'currency_id',
        'target_currency_id',
        'rate',
        'type',
    ];

    protected $casts = [
        'currency_id'           => 'integer',
        'target_currency_id'    => 'integer',
        'rate'                  => 'float',
        'type'                  => 'string',
    ];

    public function currency()
    {
        return $this->belongsTo(app('zengine')->modelClass('Currency'));
    }

    public function target_currency()
    {
        return $this->belongsTo(app('zengine')->modelClass('Currency'));
    }
}
