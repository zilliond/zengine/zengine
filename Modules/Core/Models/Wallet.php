<?php

namespace Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Core\Models\Wallet
 *
 * @property int $id
 * @property int $user_id
 * @property int $payment_system_id
 * @property string $wallet
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Modules\Core\Models\PaymentSystem $payment_system
 * @property-read \Modules\Core\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Wallet newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Wallet newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Wallet query()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Wallet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Wallet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Wallet wherePaymentSystemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Wallet whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Wallet whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Wallet whereWallet($value)
 * @mixin \Eloquent
 * @property float $balance
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Wallet whereBalance($value)
 */
class Wallet extends Model
{
    protected $fillable = ['user_id', 'payment_system_id', 'wallet', 'balance'];

    public function user()
    {
        return $this->belongsTo(app('zengine')->modelClass('User'));
    }

    public function payment_system()
    {
        return $this->belongsTo(app('zengine')->modelClass('PaymentSystem'));
    }
}
