<?php

namespace Modules\Core\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Stat
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Stat newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Stat newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Stat query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $key
 * @property string $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Stat whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Stat whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Stat whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Stat whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Stat whereValue($value)
 */
class Stat extends Model
{
    protected $fillable = [
        'key', 'value'
    ];
}
