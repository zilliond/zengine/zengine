<?php

namespace Modules\Core\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Translatable\HasTranslations;

/**
 * Modules\Core\Models\Post.
 *
 * @property int                             $id
 * @property string                          $title
 * @property string|null                     $description
 * @property string|null                     $keywords
 * @property string                          $body
 * @property string|null                     $images
 * @property bool                            $is_important
 * @property int                             $views
 * @property string|null                     $file_url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Post newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Post newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\Post onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Post query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Post whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Post whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Post whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Post whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Post whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Post whereImageUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Post whereImages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Post whereIsImportant($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Post whereKeywords($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Post whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Post whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Post whereViews($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\Post withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\Post withoutTrashed()
 * @mixin \Eloquent
 * @property mixed $translations
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Post whereFileUrl($value)
 * @property int $is_hidden
 * @method static \Illuminate\Database\Eloquent\Builder|Post whereIsHidden($value)
 * @method static Builder|Post hidden()
 * @method static Builder|Post visible()
 */
class Post extends Model
{
    use SoftDeletes;
    use HasTranslations;

    public $table = 'posts';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'title',
        'description',
        'keywords',
        'body',
        'is_important',
        'is_hidden',
        'file_url',
        'views',
    ];

    public $translatable = [
        'title',
        'description',
        'body',
    ];

    public function scopeVisible(Builder $builder)
    {
        return $builder->where('is_hidden', 0);
    }

    public function scopeHidden(Builder $builder)
    {
        return $builder->where('is_hidden', 1);
    }
}
