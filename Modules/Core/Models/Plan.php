<?php

namespace Modules\Core\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Validation\Rule;

/**
 * Modules\Core\Models\Plan
 *
 * @property int $id
 * @property string $name
 * @property float $min_amount
 * @property float $max_amount
 * @property int $currency_id
 * @property float $percent
 * @property string $type
 * @property int $duration
 * @property float $amount_bonus
 * @property float $balance_bonus
 * @property bool $not_charge_weekends
 * @property float|null $additional_referral_percent
 * @property bool $is_limited
 * @property int|null $max_deposits
 * @property int $can_close
 * @property float|null $return_percent
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Modules\Core\Models\Currency $currency
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Core\Models\Deposit[] $deposits
 * @property-read int|null $deposits_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan newQuery()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\Plan onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan whereAdditionalReferralPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan whereAmountBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan whereBalanceBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan whereCanClose($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan whereIsLimited($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan whereMaxAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan whereMaxDeposits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan whereMinAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan whereNotChargeWeekends($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan wherePercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan whereReturnPercent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Core\Models\Plan whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\Plan withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Core\Models\Plan withoutTrashed()
 * @mixin \Eloquent
 * @property int $is_hidden
 * @method static Builder|Plan hidden()
 * @method static Builder|Plan visible()
 * @method static Builder|Plan whereIsHidden($value)
 */
class Plan extends Model
{
    use SoftDeletes;

    public const TYPE_DAILY = 'daily';
    public const TYPE_HOURLY = 'hourly';

    public $table = 'plans';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'name',
        'currency_id',
        'min_amount',
        'max_amount',
        'percent',
        'type',
        'duration',
        'amount_bonus',
        'balance_bonus',
        'not_charge_weekends',
        'additional_referral_percent',
        'is_limited',
        'max_deposits',
        'can_close',
        'return_percent',
        'is_hidden',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                          => 'integer',
        'name'                        => 'string',
        'min_amount'                  => 'float',
        'max_amount'                  => 'float',
        'percent'                     => 'float',
        'type'                        => 'string',
        'duration'                    => 'integer',
        'amount_bonus'                => 'float',
        'balance_bonus'               => 'float',
        'not_charge_weekends'         => 'boolean',
        'additional_referral_percent' => 'float',
        'is_limited'                  => 'boolean',
        'max_deposits'                => 'integer'
    ];

    /**
     * Validation rules
     *
     * @return  array
     */
    public static function rules()
    {
        return [
            'name'                  => 'required',
            'min_amount'            => 'required',
            'max_amount'            => 'required',
            'currency_id'           => 'required|exists:currencies,id',
            'percent'               => 'required|min:0.001',
            'type'                  => [
                'required',
                Rule::in([
                    self::TYPE_HOURLY,
                    self::TYPE_DAILY
                ])
            ],
            'duration'                    => 'required|integer|min:1',
            'amount_bonus'                => 'nullable|min:0',
            'balance_bonus'               => 'nullable|min:0',
            'not_charge_weekends'         => 'boolean',
            'additional_referral_percent' => 'nullable',
            'is_limited'                  => 'boolean',
            'max_deposits'                => 'nullable|integer|min:0',
            'can_close'                   => 'boolean',
            'return_percent'              => 'nullable|integer|min:0|max:100',
        ];
    }

    public static function types()
    {
        return [
            self::TYPE_DAILY  => 'Daily',
            self::TYPE_HOURLY => 'Hourly',
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function deposits()
    {
        return $this->hasMany(app('zengine')->modelClass('Deposit'), 'plan_id', 'id');
    }

    public function currency()
    {
        return $this->belongsTo(app('zengine')->modelClass('Currency'));
    }

    public function scopeVisible(Builder $builder)
    {
        return $builder->where('is_hidden', 0);
    }

    public function scopeHidden(Builder $builder)
    {
        return $builder->where('is_hidden', 1);
    }
}
