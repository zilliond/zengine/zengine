<?php


namespace Modules\Core\Payments;

use Illuminate\Http\Request;
use Modules\Core\Exceptions;
use Modules\Core\Exceptions\OperationException;
use Modules\Core\Exceptions\PaymentSystemException;
use Modules\Core\Models\Operation;

class QiwiProvider extends PaymentSystemProvider
{

    /**
     * @inheritDoc
     */
    public function getName() : string
    {
        return 'Qiwi';
    }

    public function makeRefill(Operation $operation) : Operation
    {
        $this->checkCredentials(['private_key']);
        $private_key = $this->getCredential('private_key');
        $amount = number_format($operation->amount, 2, '.', '');
        $date = now()->addMonth()->toIso8601String();
        $response = $this->guzzle->request('PUT', "https://api.qiwi.com/partner/bill/v1/bills/{$operation->id}", [
            'json' => [
                'amount'                => [
                    'value'             => $amount,
                    'currency'          => $operation->currency->code_iso,
                ],
                'expirationDateTime'    => $date,
                'customer'              => [
                    'email'             => $operation->user->email,
                ],
                'comment'   => 'Balance refill',
            ],
            'headers' => $this->getHeaders($private_key)
        ]);
        $result = json_decode((string) $response->getBody(), true);

        $redirect_url = $result['payUrl'] . "&successUrl=" . url('cabinet/refill?status=success'); // TODO: Refactor
        \Log::channel('payment_systems')->info("QiwiProvider|makeRefill|Operation {$operation->id}: payment_url: {$redirect_url}");
        $operation->payment_system_data = [
            'refill_result' => $result,
            'type'          => 'redirect',
            'redirect_url'  => $redirect_url
        ];
        $operation->save();

        return $operation;
    }

    /**
    public function makeRefill(Operation $operation) : Operation
    {
        $this->checkCredentials(['public_key']);
        $public_key = $this->getCredential('public_key');
        $amount = number_format($operation->amount, 2, '.', '');
        $form_data = [
            'publicKey' => $public_key,
            'billId'    => $operation->id,
            'amount'    => $amount,
            'email'     => $operation->user->email,
            'comment'   => 'Balance refill',
        ];
        $redirect_url = 'https://oplata.qiwi.com/create?'.http_build_query($form_data);
        \Log::channel('payment_systems')->info("QiwiProvider|makeRefill|Operation {$operation->id}: payment_url: $redirect_url");
        $operation->payment_system_data = [
            'amount'        => $amount,
            'type'          => 'redirect',
            'redirect_url'  => $redirect_url
        ];
        $operation->save();

        return $operation;
    }
     */

    public function canHandle(Request $request) : bool
    {
        return $request->header('X-Api-Signature-SHA256') && $request->filled('bill.billId');
    }

    public function handle(Request $request)
    {
        $this->checkCredentials(['private_key']);
        $private_key = $this->getCredential('private_key');
        \Log::channel('payment_systems')->info("QiwiProvider|Handler|start", ['request' => $request->all()]);
        \Log::channel('payment_systems')->info($request->all());
        $bill = $request->get('bill');
        $operation = app('zengine')->model('Operation')->find($bill['billId']);
        if (!$operation) {
            \Log::channel('payment_systems')->info("QiwiProvider|Handler|Operation {$operation->id} not find - request", ['request' => $request->all()]);
            throw new PaymentSystemException('QiwiProvider| Operation not find');
        }
        $hash_data = implode("|", [
            $bill['amount']['currency'],
            $bill['amount']['value'],
            $bill['billId'],
            $bill['siteId'],
            $bill['status']['value'],
        ]);
        $hash = hash_hmac('sha256', $hash_data, $private_key);
        if ($hash === $request->header('X-Api-Signature-SHA256')) {
            \Log::channel('payment_systems')->info("QiwiProvider|Handler|Operation {$operation->id} success");
            $this->operationService->typeByOperation($operation)->success($operation);
            return;
        }
        \Log::channel('payment_systems')->info("QiwiProvider| sign mismatch |Operation {$operation->id}", ['request' => $request->all()]);
        throw new PaymentSystemException('QiwiProvider| sign mismatch');
    }

    public function checkStatus(Operation $operation)
    {
        $operation->loadMissing('payment_system');
        if ($operation->payment_system->provider !== 'qiwi') {
            throw new PaymentSystemException('Not qiwi');
        }
        if (!in_array($operation->type, [app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW, app('zengine')->modelClass('Operation')::TYPE_USER_REFILL], true)) {
            throw new PaymentSystemException('Not refill or withdraw');
        }
        if (!in_array($operation->status, [app('zengine')->modelClass('Operation')::STATUS_CREATED, app('zengine')->modelClass('Operation')::STATUS_IN_PROGRESS], true)) {
            throw new PaymentSystemException('Status not progressed');
        }
        \Log::channel('payment_systems')->info("QiwiProvider|Handler|Operation {$operation->id} check status start");
        $this->checkCredentials(['private_key']);
        $private_key = $this->getCredential('private_key');
        $response = $this->guzzle->request('GET', "https://api.qiwi.com/partner/bill/v1/bills/{$operation->id}", [
            'headers' => $this->getHeaders($private_key)
        ]);
        $result = json_decode((string) $response->getBody(), true);
        \Log::channel('payment_systems')->info("QiwiProvider|Handler|Operation {$operation->id} check status resp", ['response' => $result]);
        if (is_array($result) && is_array($result['status']) && $result['status']['value']) {
            if ($result['status']['value'] === 'PAID') {
                $this->operationService->typeByOperation($operation)->success($operation);
            } elseif ($result['status']['value'] === 'REJECTED' || $result['status']['value'] === 'EXPIRED') {
                $this->operationService->typeByOperation($operation)->reject($operation);
            }
        }
    }

    /**
     * @return float
     * @throws Exceptions\OperationException
     * @throws PaymentSystemException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getBalance() : float
    {
        $this->checkCredentials(['person_id', 'api_key']);
        $person_id = $this->getCredential('person_id');
        $api_key = $this->getCredential('api_key');
        $response = $this->guzzle->request('GET', "https://edge.qiwi.com/funding-sources/v2/persons/{$person_id}/accounts", [
            'headers' => $this->getHeaders($api_key)
        ]);
        $result = json_decode((string) $response->getBody(), true);
        if (is_array($result) && count($result['accounts'])) {
            foreach ($result['accounts'] as $account) {
                if ($account['alias'] === 'qw_wallet_rub' && $account['hasBalance'] && is_array($account['balance'])) {
                    return (float) $account['balance']['amount'];
                }
            }
        }
        return 0;
    }

    public function makeWithdraw(Operation $operation) : Operation
    {
        $this->checkCredentials(['api_key']);
        $api_key = $this->getCredential('api_key');
        if (!isset($operation->payment_system_data['target'])) {
            throw new OperationException('QiwiProvider|makeWithdraw|Not set target account');
        }

        $currencyService = app('currencies');
        if ($operation->currency->code_iso === 'RUB') {
            $amount_rub = number_format($operation->amount, 2, '.', '');
        } else {
            $rub = app('zengine')->model('Currency')->where('code_iso', 'RUB')->orWhere('code', 'RUB')->firstOrFail();
            $amount_rub = number_format($currencyService->convert($operation->amount, $operation->currency, $rub), 2, '.', '');
        }

        $target = $operation->payment_system_data['target'];
        $payment = [
            'id'    => $operation->id,
            'sum'   => [
                'amount'    => $amount_rub,
                'currency'  => 643,
            ],
            'paymentMethod' => [
                'type'      => 'Account',
                'accountId' => 643,
            ],
            'fields'    => [
                'account'   => $target[0] === '+' ? $operation->payment_system_data['target'] : '+'.$operation->payment_system_data['target']
            ]
        ];

        $response = $this->guzzle->request('POST', 'https://edge.qiwi.com/sinap/api/v2/terms/99/payments', [
            'form_params'   => $payment,
            'headers'       => $this->getHeaders($api_key)
        ]);
        $result = json_decode((string) $response->getBody(), true);
        \Log::info($result);
    }

    protected function getHeaders($private_key) : array
    {
        return [
            'Authorization' => "Bearer $private_key",
            'Accept'        => "application/json",
            'Content-type'  => "application/json",
        ];
    }

    public function getCredentialsKeys() : array
    {
        return [
            'public_key',
            'private_key',
            'person_id',
            'api_key',
        ];
    }

    public function getCredentialsFields() : array
    {
        return [
            'public_key'    => 'Публичный ключ',
            'private_key'   => 'Приватный ключ',
            'person_id'     => 'Номер кошелька',
            'api_key'       => 'API Ключ',
        ];
    }
}
