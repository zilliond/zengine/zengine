<?php


namespace Modules\Core\Payments;

use Exception;
use Illuminate\Http\Request;
use Log;
use Modules\Core\Models\Operation;

class ManualPaymentProvider extends PaymentSystemProvider
{
    /** @inheritDoc */
    public function getName() : string
    {
        return 'Manual';
    }

    public function makeRefill(Operation $operation) : Operation
    {
        $operation->payment_system_data = [
            'type'      => 'none'
        ];
        $operation->save();

        return $operation;
    }

    public function makeWithdraw(Operation $operation) : Operation
    {
        return $operation;
    }

    /**
     * @param  Request  $request
     * @throws Exception
     */
    public function handle(Request $request)
    {
        Log::channel('payment_systems')->info("ManualPaymentProvider|Handler|handle not support for this payment provider");
        throw new Exception('Handle don\'t support.');
    }

    public function getCredentialsKeys() : array
    {
        return [];
    }

    public function getCredentialsFields() : array
    {
        return [];
    }
}
