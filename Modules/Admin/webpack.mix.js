const mix = require('laravel-mix');
require('laravel-mix-merge-manifest');
const webpack_config = require('./webpack.config')

mix.setPublicPath('../../public').mergeManifest();

mix.js(__dirname + '/Resources/assets/js/app.js', 'js/admin.js')
    .sass( __dirname + '/Resources/assets/sass/app.scss', 'css/admin.css');

// Copy images to public
mix.copyDirectory(__dirname + '/Resources/assets/img', '../../public/img');
mix.webpackConfig(webpack_config);
mix.sourceMaps(true);
mix.version();
