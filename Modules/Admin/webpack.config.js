const path = require('path')
const SVGSpritemapPlugin = require('svg-spritemap-webpack-plugin');
module.exports = {
    resolve: {
        extensions: ['.js', '.json', '.vue'],
        alias: {
            'Admin': path.resolve(__dirname, 'Resources', 'assets', 'js'),
        }
    },
    plugins: [
        new SVGSpritemapPlugin(__dirname + '/Resources/assets/sprites/**/*.svg', {
            output: {
                filename: 'img/sprite.svg',
                svgo:
                    {
                        plugins: [
                            {
                                removeStyleElement: true
                            },
                            {
                                removeUselessStrokeAndFill: true
                            },
                            {
                                removeAttrs: {
                                    attrs: '(stroke|fill)'
                                }
                            },
                        ]
                    }
            },
            sprite: {
                prefix: false
            }
        }),
    ]
}
