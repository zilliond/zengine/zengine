<?php

return [
    'top_plans_first'       => 'Тарифы',
    'top_plans_second'      => ' -  топ 4 тарифа',
    'top_payments_first'    => 'Платежки',
    'top_payments_second'   => ' -  топ 4 платежки',
    'top_balances_first'    => 'Балансы',
    'top_balances_second'   => ' -  топ 4 платежки',
    'operations'            => [
        'refills'           => 'Пополнения',
        'users'             => 'Пользователи',
        'withdraws'         => 'Выплачено',
    ],
    'daily' => [
        'title'             => 'Пополнений и выводов за день',
        'refill'            => 'Пополнено:',
        'withdrawn'         => 'Выведено:',
        'profit_daily'      => 'Чистая прибыль за день: ',
        'profit_percent'    => 'Процент прибыли: ',
        'daily'             => 'По дням',
        'last_12_days'      => 'За последние  12 дней',
    ],
    'events' => [
        'title'             => 'Последние события проекта',
        'sub_title'         => 'в данном поле вы сможете отследить все что происходит в проекте вы сможете',
        'operations'        => [
            'refills'       => 'Пополнения',
            'refill'        => 'Пополнение',
            'withdraws'     => 'Выводы',
            'withdraw'      => 'Вывод',
            'deposits'      => 'Депозиты',
            'deposit'       => 'Депозит',
            'registrations' => 'Регистрации',
            'registration'  => 'Регистрация',
            'ref_pays'      => 'Реферальнные',
            'ref_pay'       => 'Реферальнные',
        ]
    ],
    'investors' => [
        'title'             => 'Инвесторов в проекте:',
        'active'            => 'Активные',
        'total'             => 'Всего партнеров',
    ],
    'waiting_refills'       => 'Пополнений в ожидании:',
    'statistics'            => 'Статистика - топ 3 партнеров:',
    'referrals'             => [
        'title'             => 'Рефферальные отличиления:',
        'active'            => 'Активные',
        'total'             => 'Всего партнеров',
    ],
];
