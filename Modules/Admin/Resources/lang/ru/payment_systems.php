<?php

return [
    'title'                     => 'Список платежных систем',
    'add_button_first'          => 'Добавить',
    'add_button_second'         => 'платежную систему',
    'update_button_first'       => 'Обновить',
    'update_button_second'      => 'балансы',
    'form'                      => [
        'create_title'          => 'Добавление платежной системы',
        'update_title'          => 'Редактирование платежной системы',
        'required_help'         => '- поля обязательные для заполнения',
        'provider'              => 'Основной модуль:',
        'name'                  => 'Название:',
        'wallet'                => 'Номер счета:',
        'code'                  => 'Аббревиатура:',
        'refill_commission'     => 'Комиссия на ввод:',
        'withdraw_commission'   => 'Комиссия на вывод:',
        'currency'              => 'Валюта:',
        'refill_amount'         => 'Суммы пополнения',
        'withdraw_amount'       => 'Суммы выводов',
        'usage_settings'        => 'Настройки пользования:',
        'usage_types'           => [
            'refill'            => 'Пополнения',
            'withdraw'          => 'Вывод',
            'wallet'            => 'Кошелек',
        ],
        'withdraw_type'         => 'Автовыплаты API:',
        'withdraw_types'        => [
            'manual'            => 'Ручные',
            'semi_auto'         => 'Полуавтоматические',
            'auto'              => 'Автоматические'
        ],
        'status'                => 'Статус:',
        'access_settings'       => '- Настройки доступа',
        'submit'                => 'Сохранить',
    ]
];
