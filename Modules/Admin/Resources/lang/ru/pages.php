<?php

return [
    'table' => [
        'title'         => 'Список страниц',
        'page'          => 'Страница',
        'route'         => 'Ссылка',
    ],
    'form' => [
        'title'         => 'Заголовок:',
        'description'   => 'Описание страницы:',
        'keywords'      => 'Ключевые слова:',
        'route'         => 'Путь:',
        'submit'        => 'Сохранить',
    ]
];
