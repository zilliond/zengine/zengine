'use strict';

const dayjs = require('dayjs');
const utc = require('dayjs/plugin/utc');
const timezone = require('dayjs/plugin/timezone');
dayjs.extend(utc);
dayjs.extend(timezone);

export default {
    /**
     * Vue
     * @param {Vue} Vue
     */
    install(Vue) {
        Vue.prototype.$dayjs = {
            get(){
                return dayjs;
            }
        };
        Vue.prototype.$date = dayjs
        Vue.dayjs = dayjs;
    }
};
