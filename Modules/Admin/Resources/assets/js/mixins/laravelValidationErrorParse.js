export default {
    methods: {
        parseErrorToHtml(error){
            console.log('parse');
            let html = '<ul>';
            let data = error.response.data;
            console.log(data);
            _.map(data.errors, (item) => {
                console.log(item);
                if(Array.isArray(item)){
                    html += `<li>${item.join('|')}</li>`;
                }
            });
            return html;
        },
        displayErrorsSwal(error){
            const html = this.parseErrorToHtml(error);
            this.$swal({
                title: 'Ошибка при валидации',
                html: html,
                type: 'error'
            });
        }
    }
}
