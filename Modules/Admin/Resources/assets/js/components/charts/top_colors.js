export default [
    {
        type: 'linear',
        x: 0,
        y: 0,
        x2: 1,
        y2: 1,
        colorStops: [{
            offset: 1, color: 'rgb(177, 34, 229)' // color at 0% position
        }, {
            offset: 0, color: 'rgb(255, 99, 221)' // color at 100% position
        }],
        global: false // false by default
    },
    {
        type: 'linear',
        x: 0,
        y: 0,
        x2: 1,
        y2: 1,
        colorStops: [{
            offset: 1, color: 'rgb(244, 48, 127)' // color at 100% position
        }, {
            offset: 0, color: 'rgb(255, 124, 109)' // color at 0% position
        }],
        global: false // false by default
    },
    {
        type: 'linear',
        x: 0,
        y: 0,
        x2: 1,
        y2: 1,
        colorStops: [{
            offset: 1, color: 'rgb(255, 130, 89)' // color at 100% position
        }, {
            offset: 0, color: 'rgb(255, 223, 63)' // color at 0% position
        }],
        global: false // false by default
    },
    {
        type: 'linear',
        x: 0,
        y: 0,
        x2: 1,
        y2: 1,
        colorStops: [{
            offset: 1, color: 'rgb(255, 189, 89)' // color at 100% position
        }, {
            offset: 0, color: 'rgb(241, 255, 63)' // color at 0% position
        }],
        global: false // false by default
    },
];
