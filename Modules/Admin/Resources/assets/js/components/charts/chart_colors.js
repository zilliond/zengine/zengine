import top_colors from "./top_colors";

export default {
    default_gradients: top_colors,
    blue_gradient: {
        type: 'linear',
        x: 0.5,
        y: 0,
        x2: 0.5,
        y2: 1,
        colorStops: [{
            offset: 0, color: 'rgb(43, 157, 217)'
        }, {
            offset: 1, color: 'rgb(82, 91, 211)'
        }],
        global: false
    },
    green_gradient: {
        type: 'linear',
        x: 0,
        y: 1,
        x2: 1,
        y2: 0,
        colorStops: [{
            offset: 0, color: 'rgb(71, 179, 105)'
        }, {
            offset: 1, color: 'rgb(58, 196, 102)'
        }],
        global: false
    },
    red_gradient: {
        type: 'linear',
        x: 0,
        y: 1,
        x2: 1,
        y2: 0,
        colorStops: [{
            offset: 0, color: 'rgb(232, 60, 99)'
        }, {
            offset: 1, color: 'rgb(255, 84, 85)'
        }],
        global: false
    },
    gray: '#e3e3e3'
}
