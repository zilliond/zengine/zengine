import lodash from 'lodash';
window._ = lodash;

import Vue from 'vue'
import VueRouter from 'vue-router'
import store from 'Admin/store';
import { mapActions } from 'vuex';
import ECharts from 'vue-echarts'
import 'echarts/lib/chart/pie';
import 'echarts/lib/chart/bar';
import 'echarts/lib/chart/line';
import 'echarts/lib/component/legend';
import 'echarts/lib/component/legendScroll';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/graphic';

import VueSweetalert2 from 'vue-sweetalert2';
import VueFlatPickr from 'vue-flatpickr-component';
import 'flatpickr/dist/flatpickr.css';

import VueI18n from 'vue-i18n'
Vue.use(VueI18n);
import messages from 'Admin/lang.generated';
const i18n = new VueI18n({
    locale: 'ru', // set locale
    messages, // set locale messages
});
import datePlugin from 'Admin/plugins/date';
Vue.use(VueRouter);
Vue.use(VueSweetalert2);
Vue.use(VueFlatPickr);
Vue.use(datePlugin)

Vue.component('toggle', require('Admin/components/Toggle').default);
Vue.component('vuex-toggle', require('Admin/components/VuexToggle').default);
Vue.component('ui-select', require('Admin/components/Select').default);
Vue.component('select-wrapper', require('Admin/components/SelectWrapper').default);
Vue.component('form-select', require('Admin/components/FormSelect').default);
Vue.component('ui-radio', require('Admin/components/Radio').default);
Vue.component('ui-checkbox', require('Admin/components/Checkbox').default);
Vue.component('checkbox-value', require('Admin/components/CheckboxValue').default);
Vue.component('toast-editor', require('Admin/components/ToastEditor').default);

Vue.component('ui-filter', require('Admin/components/UiFilter').default);
Vue.component('tooltip', require('Admin/components/Tooltip').default);
Vue.component('paginator', require('Admin/components/Paginator').default);
Vue.component('local-paginator', require('Admin/components/LocalPaginator.vue').default);
Vue.component('v-chart', ECharts);

import App from 'Admin/App';
import routes from 'Admin/routes';

const router = new VueRouter({
    mode: 'history',
    routes: routes,
    base: '/admin'
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,
    store,
    i18n,
    methods: {
        ...mapActions([
            'createAxios',
            'getSupportedLocales',
            'getPaymentSystems',
        ]),
        ...mapActions('auth', [
            'getUser',
        ])
    },
    created(){
        this.createAxios();
        this.getSupportedLocales();
        this.getUser();
        this.getPaymentSystems();
    }
});
