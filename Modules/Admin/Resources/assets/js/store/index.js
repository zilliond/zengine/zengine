import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
import axios from 'axios';
import currencies from "../currencies";

const state = {
    app_url: window.system.app_url,
    locale: window.system.locale,
    supportedLocales: {},
    admin: {
        prefix: window.system.admin_prefix
    },
    system: {
        csrf: window.system.csrf,
        timezone: window.system.timezone,
        balances_type: window.system.balances_type,
        volume: true,
    },
    http: {
        baseURL : window.system.base_url,
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'X-CSRF-TOKEN': window.system.csrf,
            'Authorization': `Bearer ${window.system.user.token}`,
        },
    },
    payment_systems: [],
    axios: undefined,
};

const getters = {
    app_url: state => state.app_url,
    admin_prefix: state => state.admin.prefix,
    timezone: state => state.system.timezone,
    balances_type: state => state.system.balances_type,
    locale: state => state.locale,
    supportedLocales: state => state.supportedLocales,
    csrf: state => state.system.csrf,
    notifications_volume: state => state.system.volume,
    axios: state => state.axios,
    currencies: state => currencies.currencies,
    payment_systems: state => state.payment_systems,
};

const actions = {
    enableVolume({commit}){
        commit('ENABLE_VOLUME');
    },
    disableVolume({commit}){
        commit('DISABLE_VOLUME');
    },
    createAxios({ commit }){
        commit('SET_AXIOS', axios.create(state.http));
    },
    setLocale({ commit, dispatch }, token){
        commit('SET_LOCALE', token);
        dispatch('auth/setUserLocale', token);
    },
    getSupportedLocales({commit, getters}){
        getters.axios
            .get('/locale/supported')
            .then(resp => resp.data)
            .then(locales => commit('SET_SUPPORTED_LOCALES', locales));
    },
    setAuthToken({ commit, dispatch }, token){
        commit('SET_AUTH_TOKEN', token);
        dispatch('createAxios');
    },
    getPaymentSystems({commit, getters}){
        getters.axios
            .get('/payment_systems')
            .then(resp => resp.data)
            .then(payment_systems => commit('SET_PAYMENT_SYSTEMS', payment_systems));
    }
};

const mutations = {
    TOGGLE_VOLUME(state){
        state.system.volume = !state.system.volume;
    },
    ENABLE_VOLUME(state){
        state.system.volume = true;
    },
    DISABLE_VOLUME(state){
        state.system.volume = false;
    },
    SET_AXIOS(state, axios){
        state.axios = axios;
    },
    SET_LOCALE(state, locale){
        state.locale = locale;
    },
    SET_SUPPORTED_LOCALES(state, locales){
        state.supportedLocales = locales;
    },
    SET_AUTH_TOKEN(state, token){
        state.http.headers['Authorization'] = 'Bearer ' + token;
    },
    SET_PAYMENT_SYSTEMS(state, payment_systems){
        state.payment_systems = payment_systems;
    },
};

import auth from './modules/auth';

const store = new Vuex.Store({
    modules: {
        auth: auth
    },
    state,
    getters,
    actions,
    mutations,
});

export default store;
