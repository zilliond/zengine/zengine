import axios from 'axios';
import _ from 'lodash';
import Vue from 'vue';
const accounting = require('accounting');

class Currencies {
    constructor(){
        this.state = Vue.observable({
            default_currency: {id: 1, precision: 2, format_precision: 2},
            currencies: [],
            rates: [],
            settings: {
                amount: {
                    decimal_total:16,
                    decimal_precision:6
                }
            },
        });
        this.loadData();
    }

    get decimal_precision(){
        return this.state.settings.amount.decimal_precision || 6;
    }

    get default_currency_id(){
        return this.state.default_currency.id;
    }

    get default_currency(){
        return this.state.default_currency;
    }

    set default_currency(default_currency){
        this.state.default_currency = default_currency;
    }

    get currencies(){
        return this.state.currencies;
    }

    set currencies(currencies){
        this.state.currencies = currencies;
    }

    get rates(){
        return this.state.rates;
    }

    set rates(rates){
        this.state.rates = rates;
    }

    rateById(id){
        const pair = _.find(this.state.rates, {currency_id: this.default_currency_id, target_currency_id: id});
        if(!pair || !pair.rate){
            return 0;
        }
        return pair.rate;
    }

    get settings(){
        return this.state.settings;
    }

    set settings(settings){
        this.state.settings = settings;
    }

    format(amount = 0, currency = undefined){
        if(currency === undefined){
            currency = this.default_currency;
        }
        return accounting.formatMoney(Number.parseFloat(amount).toFixed(currency.format_precision ? currency.format_precision : 2), currency.symbol + ' ', currency.format_precision, ' ');
    }


    formatAmount(amount = 0, precision = 2, symbol = ''){
        return accounting.formatMoney(Number.parseFloat(amount).toFixed(precision), symbol + ' ', precision, ' ');
    }

    formatWithDefault(amount = 0, currency = undefined) {
        if(currency === undefined){
            currency = this.default_currency;
        }
        let formatted = this.format(amount, currency);
        if(currency.id !== this.default_currency.id){
            const default_amount = this.convertToDefault(amount, currency);
            formatted += ` (${this.format(default_amount, this.default_currency)})`;
        }
        return formatted;
    }

    formatInDefault(amount = 0) {
        return this.format(amount, this.default_currency);
    }

    getById(id){
        const currency = _.find(this.state.currencies, {id: id});
        if(!currency){
            return {
                id: id,
                name: 'Undefined',
                rate: 0,
                symbol: '',
                code: '',
                code_iso: '',
            };
        }
        return currency;
    }


    getByCode(code){
        const currency = _.find(this.state.currencies, {code_iso: code});
        if(!currency){
            console.log(currency);
            return {
                id: 0,
                name: 'Undefined',
                rate: 0,
                symbol: '',
                code: code,
                code_iso: code,
            };
        }
        return currency;
    }

    convert(amount, from, to){
        amount = Number.parseFloat(amount);
        if(amount === 0.0){
            return 0.0.toFixed(this.state.settings.amount.decimal_precision);
        }
        if(from.id === to.id){
            return amount.toFixed(this.state.settings.amount.decimal_precision);
        }
        const pair = _.find(this.state.rates, {
            currency_id: from.id,
            target_currency_id: to.id,
        });
        if(pair.rate)
            return  (amount * Number.parseFloat(pair.rate)).toFixed(this.state.settings.amount.decimal_precision);
        return NaN;
    }

    convertByCode(amount, from_code, to_code){
        let from = this.getByCode(from_code);
        let to = this.getByCode(to_code);
        return this.convert(amount, from, to);
    }

    convertToDefault(amount, currency){
        return this.convert(amount, currency, this.state.default_currency);
    }

    loadData(){
        this.loading = true;
        axios.get('/api/currencies')
            .then(resp => resp.data)
            .then(data => {
                this.state.currencies = data.currencies;
                this.state.rates = data.rates;
                this.state.default_currency = data.default;
                this.state.settings = data.settings;
            }).finally(() => {
                this.loading = false;
            });
    }
}

export default new Currencies();
