<?php /** @noinspection SuspiciousBinaryOperationInspection */

use Modules\Admin\Http\Controllers\DashboardController;

Route::group(['prefix' => setting('admin.url'), 'as' => 'admin.', 'middleware' => ['can:admin_panel']], function () {
    Route::get('/{any?}', [DashboardController::class, 'spa'])->where('any', '.*')->name('spa');
});
