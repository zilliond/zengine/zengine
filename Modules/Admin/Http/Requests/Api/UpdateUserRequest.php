<?php

namespace Modules\Admin\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = (int) $this->get('id');
        return [
            'id'                => 'required|numeric|min:1',
            'password'          => 'nullable|min:1|confirmed',
            'email'             => 'required|email:rfc|unique:users,email,'.$id,
            'balance'           => 'required|numeric|min:0',
            'currency_id'       => 'required|integer|exists:currencies,id',
            'referral_percent'  => 'nullable',
            'comment'           => 'nullable',
            'wallets.*'         => 'nullable|string'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
