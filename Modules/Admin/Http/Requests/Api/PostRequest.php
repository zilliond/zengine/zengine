<?php

namespace Modules\Admin\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title.*'       => 'required|string|min:5',
            'description.*' => 'nullable|string',
            'keywords'      => 'nullable|string',
            'body.*'        => 'required|string|min:10',
            'file_url'      => 'nullable|string',
            'file'          => 'nullable|mimes:jpeg,png,jpg,gif,svg',
            'is_important'  => 'required|in:0,1',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
