<?php

namespace Modules\Admin\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class CreateDepositRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'plan_id'           => 'required|integer|exists:plans,id',
            'currency_id'       => 'required|integer|exists:currencies,id',
            'user_id'           => 'required|integer|exists:users,id',
            'amount'            => 'required|min:0.00001',
        ];
    }
}
