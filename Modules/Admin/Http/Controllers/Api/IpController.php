<?php

namespace Modules\Admin\Http\Controllers\Api;

use Modules\Core\Models\AuthLog;
use Modules\Core\Models\BlacklistEntry;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class IpController extends Controller
{
    public function auth_log()
    {
        return app('zengine')->model('AuthLog')->with('user')
            ->when(\request('ip'), function (Builder $query) {
                return $query->where('ip', \request('ip'));
            })
            ->when(\request('user_id'), function (Builder $query) {
                return $query->orWhere('user_id', \request('user_id'));
            })
            ->latest()
            ->paginate();
    }

    public function blacklist()
    {
        return app('zengine')->model('BlacklistEntry')->latest()->paginate(20);
    }

    public function deleteBlacklistEntry($id)
    {
        $blacklistEntry = app('zengine')->model('Blacklist')->findOrFail($id);
        return [
            'status' => $blacklistEntry->delete() ? 'success' : 'error'
        ];
    }

    public function storeBlacklistEntry(Request $request)
    {
        return app('zengine')->model('BlacklistEntry')->create($request->only(['ip', 'comment']));
    }

    public function updateBlacklistEntry($id, Request $request)
    {
        $blacklistEntry = app('zengine')->model('Blacklist')->findOrFail($id);
        $blacklistEntry->fill($request->only(['ip', 'comment']));
        $blacklistEntry->save();
        return $blacklistEntry;
    }
}
