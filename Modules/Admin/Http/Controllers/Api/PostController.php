<?php

namespace Modules\Admin\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Modules\Admin\Http\Requests\Api\PostRequest;

class PostController extends Controller
{
    public function index()
    {
        return app('zengine')->model('Post')->latest()->paginate();
    }

    public function show($id)
    {
        return app('zengine')->model('Post')->findOrFail($id);
    }

    public function store(PostRequest $request)
    {
        $data = $request->all();
        \Debugbar::info($data);
        /* @var $file \Illuminate\Http\UploadedFile */
        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $file = $request->file('file');
            \Debugbar::info($file->hashName());
            \Debugbar::info($file->extension());
            $file->storePublicly('/public/files');
            $url = \Storage::url('files/'.$file->hashName());
            \Debugbar::info($url);
            $data['file_url'] = $url;
        }
        $post = app('zengine')->model('Post')->create($data);
        return [
            'status' => 'success',
            'post'   => $post
        ];
    }

    public function update($id, PostRequest $request)
    {
        $post = app('zengine')->model('Post')->findOrFail($id);
        $post->fill($request->all());
        $post->save();
        return [
            'status' => 'success',
            'post'   => $post
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function toggle_hidden($id)
    {
        $post = app('zengine')->model('Post')->findOrFail($id);
        $post->is_hidden = !$post->is_hidden;

        return [
            'status'    => $post->save() ? 'success' : 'error',
            'message'   => $post->is_hidden ? 'Новость скрыта' : 'Новость теперь отображается',
            'post'      => $post
        ];
    }

    public function delete($id)
    {
        $post = app('zengine')->model('Post')->findOrFail($id);
        return [
            'status' => $post->delete() ? 'success' : 'error'
        ];
    }

    public function stats()
    {
        $most_popular_post = app('zengine')->model('Post')->orderByDesc('views')->first();
        return [
            'posts'             => app('zengine')->model('Post')->count(),
            'total_views'       => app('zengine')->model('Post')->sum('views'),
            'most_popular_post' => $most_popular_post ? route('news.show') : ''
        ];
    }
}
