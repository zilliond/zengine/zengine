<?php

namespace Modules\Admin\Http\Controllers\Api;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PromoController extends Controller
{
    public function index(Request $request)
    {
        return app('zengine')->model('Promo')->type($request->get('type'))->when(
            request('search'),
            function (Builder $builder) {
                    return $builder->where('name', 'LIKE', '%' . request('search') . '%');
                }
        )->paginate(10);
    }

    public function stats(Request $request)
    {
        $promos = app('zengine')->model('Promo')->type($request->get('type'))->get();
        $top = $promos->sortByDesc('profit_percent')->first();

        return [
            'top'    => $top,
            'total'  => app('zengine')->model('Promo')->type($request->get('type'))->count(),
            'budget' => [
                'invested'       => $promos->sum('invested'),
                'income'         => $promos->sum('income'),
                'profit_percent' => round($promos->avg('profit_percent'), 2),
            ],
        ];
    }

    public function show($id)
    {
        return app('zengine')->model('Promo')->findOrFail($id);
        ;
    }

    public function store(Request $request)
    {
        app('zengine')->model('Promo')->create($request->all());

        return [
            'status' => 'success',
        ];
    }

    public function update($id, Request $request)
    {
        $promo = app('zengine')->model('Promo')->findOrFail($id);
        $promo->fill($request->all());

        return [
            'status' => $promo->save() ? 'success' : 'error',
        ];
    }

    public function delete($id)
    {
        $promo = app('zengine')->model('Promo')->findOrFail($id);
        return [
            'status' => $promo->delete() ? 'success' : 'error',
        ];
    }
}
