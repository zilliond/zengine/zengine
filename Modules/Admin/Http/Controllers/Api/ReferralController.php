<?php

namespace Modules\Admin\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\Services\OperationService;

class ReferralController extends Controller
{
    /**
     * @var \Modules\Core\Services\OperationService|\Illuminate\Contracts\Foundation\Application
     */
    private $operationService;

    public function __construct()
    {
        $this->operationService = app('operations');
    }

    public function index()
    {
        return app('zengine')->model('User')->with('currency')
            ->orderByDesc('referrals_earned')
            ->paginate();
    }

    public function settings()
    {
        return [
            'levels' => $this->operationService->parsePercentsString(setting('refsys.depo_create_percents'))
        ];
    }

    public function updateSettings(Request $request)
    {
        $levels = $this->operationService->preparePercents($request->get('levels'));
        setting([
            'refsys.depo_create_percents'   => $levels,
            'refsys.depo_accruals_percents' => $levels,
        ]);
        return [
            'levels' => $this->operationService->parsePercentsString(setting('refsys.depo_create_percents')),
            'status' => 'success'
        ];
    }
}
