<?php

namespace Modules\Admin\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\Models\Deposit;
use Modules\Core\Models\Operation;

class SettingController extends Controller
{
    public function general()
    {
        return [
            'fields'       => config('settings.fields.general'),
            'settings'     => setting('general'),
            'telegram_key' => \Cache::remember('telegram_bot_key', 300, function () {
                return \Str::random(32);
            }),
        ];
    }

    public function updateGeneral(Request $request)
    {
        $data = [];
        $fields = $request->get('fields');
        foreach ($fields as $key => $value) {
            $data['general.' . $key] = $value;
        }
        setting($data)->save();
        return [
            'settings' => setting('general'),
        ];
    }

    public function social()
    {
        return [
            'providers' => config('settings.social_providers'),
            'settings'  => setting('social_auth'),
        ];
    }

    public function updateSocial(Request $request)
    {
        $data = [];
        $fields = $request->get('fields');
        foreach ($fields as $key => $value) {
            $data['social_auth.' . $key] = $value;
        }
        setting($data)->save();
        return [
            'settings' => setting('social_auth'),
        ];
    }

    public function fake()
    {
        return [
            'fields'   => config('settings.fields.fake'),
            'settings' => setting('fake'),
            'stats'    => $this->stats_for_fake(),
        ];
    }

    public function updateFake(Request $request)
    {
        $data = [];
        $fields = $request->get('fields');
        foreach ($fields as $key => $value) {
            $data['fake.' . $key] = $value;
        }
        setting($data)->save();
        return [
            'settings' => setting('fake'),
            'stats'    => $this->stats_for_fake(),
        ];
    }

    public function stats_for_fake()
    {
        $currencyService = app(config('zengine.services.currencies'));
        $users = app('zengine')->model('User')->count();
        $active_users = app('zengine')->model('User')->whereHas('operations')->count();
        $online_users = (int) \Cache::get('online-users', 0);
        $today_deposits_sums = app('zengine')->model('Deposit')->with(['currency'])
            ->where('created_at', '>=', now()->startOfDay())->where('created_at', '<=', now()->endOfDay())
            ->get()->reduce(static function ($carry, $deposit) use ($currencyService) {
                /** @var Deposit $deposit */
                return $carry + $currencyService->convertToDefault($deposit->amount, $deposit->currency);
            }, 0);
        $refills_sum = app('zengine')->model('Operation')->with('currency')->where('status', app('zengine')
            ->modelClass('Operation')::STATUS_SUCCESS)->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_REFILL)
            ->get()->reduce(static function ($carry, $operation) use ($currencyService) {
                /** @var Operation $operation */
                return $carry + $currencyService->convertToDefault($operation->amount, $operation->currency);
            }, 0);
        $withdraws_sum = app('zengine')->model('Operation')->with('currency')
                ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)->where('type', app('zengine')
                ->modelClass('Operation')::TYPE_USER_WITHDRAW)->get()
            ->reduce(static function ($carry, $operation) use ($currencyService) {
                /** @var Operation $operation */
                return $carry + $currencyService->convertToDefault($operation->amount, $operation->currency);
            }, 0);

        return compact('users', 'active_users', 'online_users', 'today_deposits_sums', 'refills_sum', 'withdraws_sum');
    }

    public function system_info()
    {
        return \Cache::remember('system_info', 60 * 60, function () {
            return \Larinfo::getInfo();
        });
    }
}
