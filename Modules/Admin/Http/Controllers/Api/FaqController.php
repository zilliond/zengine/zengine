<?php

namespace Modules\Admin\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index()
    {
        return app('zengine')->model('FaqItem')->paginate();
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return array
     */
    public function store(Request $request)
    {
        $faqItem = app('zengine')->model('FaqItem')->create($request->only([
            'question',
            'answer',
            'hidden'
        ]));

        return [
            'status'    => 'success',
            'item'      => $faqItem
        ];
    }

    /**
     * Show the specified resource.
     * @param $id
     * @return \Modules\Core\Models\FaqItem
     */
    public function show($id)
    {
        return app('zengine')->model('FaqItem')->findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     * @param  Request  $request
     * @param $id
     * @return string[]
     */
    public function update(Request $request, $id)
    {
        $faqItem = app('zengine')->model('FaqItem')->findOrFail($id);
        $faqItem->fill($request->only([
            'question',
            'answer',
            'hidden'
        ]));

        return [
            'status'    => $faqItem->save() ? 'success' : 'error',
            'item'      => $faqItem
        ];
    }

    /**
     * @param $id
     * @return array
     */
    public function toggle_hidden($id)
    {
        $faqItem = app('zengine')->model('FaqItem')->findOrFail($id);
        $faqItem->hidden = !$faqItem->hidden;

        return [
            'status'    => $faqItem->save() ? 'success' : 'error',
            'item'      => $faqItem
        ];
    }

    /**
     * Remove the specified resource from storage.
     * @param $id
     * @return string[]
     * @throws \Exception
     */
    public function destroy($id)
    {
        $faqItem = app('zengine')->model('FaqItem')->findOrFail($id);
        return [
            'status'    => $faqItem->delete() ? 'success' : 'error'
        ];
    }
}
