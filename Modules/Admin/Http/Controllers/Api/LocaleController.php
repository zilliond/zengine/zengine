<?php

namespace Modules\Admin\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class LocaleController extends Controller
{
    public function locale()
    {
        return \App::getLocale();
    }

    public function setLocale(Request $request)
    {
        if (in_array($request->get('locale'), config('app.supportedLocales'))) {
            \Session::put('app.locale', $request->get('locale'));
            if ($user = \Auth::user()) {
                $user->locale = $request->get('locale');
                $user->save();
            }
            \App::setLocale($request->get('locale'));

            return [
                'status' => 'success'
            ];
        }

        return [
            'status'    => 'error',
            'message'   => 'Locale not found'
        ];
    }

    public function supported()
    {
        return config('app.supportedLocales');
    }
}
