<?php

namespace Modules\Admin\Http\Controllers\Api;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use Modules\Core\Models\Deposit;
use Modules\Core\Models\Operation;
use function foo\func;

class AnalyticsController extends Controller
{
    public function index(Request $request)
    {
        $from = Carbon::parse($request->get('from'));
        $to = Carbon::parse($request->get('to'));
        $diff = $to->diff($from, true);
        $refills = $this->operations(app('zengine')->modelClass('Operation')::TYPE_USER_REFILL, $from, $to);
        $withdraws = $this->operations(app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW, $from, $to);
        $deposits = $this->operations(app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_OPEN, $from, $to);
        return [
            'refills'               => $refills,
            'refills_sum'           => $refills->sum(),
            'withdraws'             => $withdraws,
            'withdraws_sum'         => $withdraws->sum(),
            'deposits'              => $deposits,
            'deposits_sum'          => $deposits->sum(),
            'average_deposit'       => $this->average_deposit($from, $to),
            'users_registered'      => $this->users_registered($from, $to),
            'users_count'           => app('zengine')->model('User')
                ->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->count(),
            'active_users_count'    => app('zengine')->model('User')
                ->whereHas('deposits')->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->count(),
            'previous_users_count'  => app('zengine')->model('User')
                ->whereDate('created_at', '>=', $from->sub($diff))->whereDate('created_at', '<=', $to->sub($diff))->count(),
        ];
    }

    public function stats()
    {
        $refills = (float) app('zengine')->getStat('REFILLS_SUCCESS_SUM');
        $withdraws = (float) app('zengine')->getStat('WITHDRAWS_SUCCESS_SUM');
        return [
            'profit'                => round($refills - $withdraws, 2),
            'estimated_tomorrow'    => $this->estimated_accruals(now()->addDay()),
            'estimated_week'        => $this->estimated_accruals(now()->addWeek()),
            'estimated_month'       => $this->estimated_accruals(now()->addMonth()),
            'estimated_half_year'   => $this->estimated_accruals(now()->addMonths(6)),
        ];
    }

    protected function operations($type, $from, $to, $group = true)
    {
        $from = Carbon::parse($from);
        $to = Carbon::parse($to);
        $raw = app('zengine')->model('Operation')->with('currency')
            ->where('type', $type)
            ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)
            ->whereDate('created_at', '>=', $from)
            ->whereDate('created_at', '<=', $to)
            ->orderBy('created_at')
            ->get()
            ->keyBy('created_at')
            ->map(function ($operation) {
                /** @var Operation $operation */
                return $operation->default_amount;
            });
        if ($group) {
            if ($from->diffInYears($to) > 3) {
                return $this->groupOperationsByYears($raw, $from);
            }
            if ($from->diffInMonths($to) > 3) {
                return $this->groupOperationsByMonths($raw, $from);
            }
            if ($from->diffInWeeks($to) > 3) {
                return $this->groupOperationsByWeeks($raw, $from);
            }
            if ($from->diffInDays($to) > 3) {
                return $this->groupOperationsByDays($raw, $from);
            }
            if ($from->diffInHours($to) > 25) {
                return $this->groupOperationsByFewHours($raw, $from, 6);
            }
            if ($from->diffInHours($to) > 13) {
                return $this->groupOperationsByFewHours($raw, $from, 3, $to);
            }
            return $this->groupOperationsByHours($raw, $from);
        }
        return $raw;
    }

    protected function groupOperationsByYears($operations, $from)
    {
        $result = collect();
        $iterations = now()->diffInYears($from);
        for ($i = 0; $i < $iterations; $i++) {
            $result[now()->subYears($i)->format('Y')] = 0.00;
        }
        foreach ($operations as $date => $amount) {
            $day = Carbon::parse($date)->format('Y');
            if ($result->has($day)) {
                $result[$day] += (float) $amount;
            } else {
                $result[$day] = (float) $amount;
            }
        }
        return $result;
    }

    protected function groupOperationsByMonths($operations, $from)
    {
        $result = collect();
        $iterations = now()->diffInMonths($from);
        for ($i = 0; $i < $iterations; $i++) {
            $result[' '.now()->subMonths($i)->format('m')] = 0.00;
        }
        foreach ($operations as $date => $amount) {
            $day = ' '.Carbon::parse($date)->format('m');
            if ($result->has($day)) {
                $result[$day] += (float) $amount;
            } else {
                $result[$day] = (float) $amount;
            }
        }
        return $result->reverse();
    }

    protected function groupOperationsByWeeks($operations, $from)
    {
        $result = collect();
        $iterations = now()->diffInWeeks($from);
        for ($i = 0; $i < $iterations; $i++) {
            $date = now()->subWeeks($i);
            $key = $date->startOfWeek()->format('d.m').'-'.$date->endOfWeek()->format('d.m');
            $result[$key] = 0.00;
        }
        foreach ($operations as $date => $amount) {
            $date = Carbon::parse($date);
            $key = $date->startOfWeek()->format('d.m').'-'.$date->endOfWeek()->format('d.m');
            if ($result->has($key)) {
                $result[$key] += (float) $amount;
            } else {
                $result[$key] = (float) $amount;
            }
        }
        return $result->reverse();
    }

    protected function groupOperationsByDays($operations, $from)
    {
        $result = collect();
        $iterations = now()->diffInDays($from);
        for ($i = 0; $i < $iterations; $i++) {
            $result[' '.now()->subDays($i)->format('d.m')] = 0.00;
        }
        foreach ($operations as $date => $amount) {
            $day = ' '.Carbon::parse($date)->format('d.m');
            if ($result->has($day)) {
                $result[$day] += (float) $amount;
            } else {
                $result[$day] = (float) $amount;
            }
        }
        return $result->reverse();
    }

    protected function groupOperationsByFewHours($operations, $from, $interval = 2, $to = null)
    {
        $result = collect();
        if (!$to) {
            $to = now();
        }
        /** @var Carbon $from */
        $bottom_dates = collect();
        $intervals = $from->diffInHours($to) / $interval;
        $create_pair = static function ($date) use ($bottom_dates, $interval) {
            /** @var Carbon $date */
            \Debugbar::info($date->format('d.m.Y H:i'));
            $start_date = now();
            foreach ($bottom_dates as $temp) {
                if ($date->isAfter($temp)) {
                    $start_date = $temp;
                    \Debugbar::info('isAfter');
                    \Debugbar::info($temp->format('d.m.Y H:i'));
                    break;
                }
            }
            $start = $start_date->format('d.m H');
            $end = $start_date->addHours($interval)->format('H d');
            return $start.'-'.$end;
            return $start_date->addHours((int) ($interval / 2))->format('d H:00');
        };
        for ($i = 0; $i < $intervals; $i++) {
            $date = $to->toImmutable()->startOfHour()->subHours(($interval - 1) + ($i * $interval));
            $bottom_dates[] = $date->toImmutable();
            $key = $create_pair($date->addMinute());
            $result[$key] = 0.00;
        }

        foreach ($operations as $date => $amount) {
            \Debugbar::info($date);
            $date = Carbon::parse($date);
            $key = $create_pair($date);
            if ($result->has($key)) {
                $result[$key] += (float) $amount;
            } else {
                $result[$key] = (float) $amount;
            }
        }

        return $result->reverse();
    }

    protected function groupOperationsByHours($operations, $from)
    {
        $result = collect();
        $iterations = now()->diffInHours($from);
        for ($i = 0; $i < $iterations; $i++) {
            $result[now()->subHours($i)->format('H:00')] = 0.00;
        }
        foreach ($operations as $date => $amount) {
            $day = Carbon::parse($date)->format('H:00');
            if ($result->has($day)) {
                $result[$day] += (float) $amount;
            } else {
                $result[$day] = (float) $amount;
            }
        }
        return $result->reverse();
    }

    protected function estimated_accruals(Carbon $to)
    {
        $currencies = app('currencies');
        $deposits = app('zengine')->model('Deposit')->with('plan')->where('left_accruals', '>', 0)->get();
        $diff_days = $to->diffInDays(now(), 1);
        $diff_hours = $to->diffInHours(now(), 1);
        $estimated = 0;
        foreach ($deposits as $deposit) {
            /** @var Deposit $deposit */
            if ($deposit->plan->type === app('zengine')->modelClass('Plan')::TYPE_DAILY) {
                $accrual = ($deposit->amount / 100) * $deposit->plan->percent;
                $estimated += $currencies->convertToDefault($accrual * min($deposit->left_accruals, $diff_days), $deposit->currency);
            } elseif ($deposit->plan->type === app('zengine')->modelClass('Plan')::TYPE_HOURLY) {
                $accrual = ($deposit->amount / 100) * $deposit->plan->percent;
                $estimated += $currencies->convertToDefault($accrual * min($deposit->left_accruals, $diff_hours), $deposit->currency);
            }
        }
        return $estimated;
    }

    protected function average_deposit(Carbon $from, Carbon $to)
    {
        $deposits = $this->operations(app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_OPEN, $from, $to, false);
        $deposits_amounts = collect();
        foreach ($deposits as $date => $amount) {
            if ($from->diffInMonths($to) > 2) {
                $day = Carbon::parse($date)->format('Y-m-02');
            } else {
                $day = Carbon::parse($date)->format('Y-m-d');
            }
            if ($deposits_amounts->has($day)) {
                $deposits_amounts[$day]->push((float) $amount);
            } else {
                $deposits_amounts[$day] = collect((float) $amount);
            }
        }
        $averages = collect();
        foreach ($deposits_amounts as $date => $amounts) {
            /** @var Collection $amounts */
            $averages[$date] = $amounts->avg();
        }
        return $averages;
    }

    protected function users_registered(Carbon $from, Carbon $to)
    {
        $users = app('zengine')->model('User')
            ->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to)->orderBy('created_at')->get();
        $users_registered = collect();
        foreach ($users as $user) {
            $day = Carbon::parse($user->created_at)->format('Y-m-d');
            if ($users_registered->has($day)) {
                $users_registered[$day] = $users_registered[$day] + 1;
            } else {
                $users_registered[$day] = 1;
            }
        }
        if ($from->diffInMonths($to) > 2) {
            return $this->groupOperationsByMonths($users_registered, $from);
        }
        if ($from->diffInWeeks($to) > 2) {
            return $this->groupOperationsByWeeks($users_registered, $from);
        }
        if ($from->diffInDays($to) > 5) {
            return $this->groupOperationsByDays($users_registered, $from);
        }
        if ($from->diffInHours($to) > 25) {
            return $this->groupOperationsByFewHours($users_registered, $from, 6);
        }
        if ($from->diffInHours($to) > 13) {
            return $this->groupOperationsByFewHours($users_registered, $from, 3, $to);
        }
        return $this->groupOperationsByHours($users_registered, $from);
    }
}
