<?php

namespace Modules\Telegram\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Controller;
use Modules\Core\Models\Operation;
use Modules\Core\Services\OperationService;

class OperationController extends Controller
{
    public function index(Request $request)
    {
        return app('zengine')->model('Operation')->with('currency', 'user', 'user.inviter', 'payment_system')
            ->orderByDesc(request('order_by', 'id'))
            ->when(request('type'), function (Builder $when) {
                return $when->where('type', request('type'));
            })
            ->when(request('status'), function (Builder $when) {
                if (request('status') === 'unconfirmed') {
                    return $when->whereIn('status', [app('zengine')->modelClass('Operation')::STATUS_CREATED, app('zengine')->modelClass('Operation')::STATUS_IN_PROGRESS]);
                }
                return $when->where('status', request('status'));
            })
            ->paginate(5);
    }

    public function reject(Operation $operation)
    {
        /** @var OperationService $operationService */
        $operationService = app('operations');
        $operation = $operationService->typeByOperation($operation)->reject($operation);
        return $operation->loadMissing('currency', 'user', 'user.inviter', 'payment_system');
    }

    public function confirm(Operation $operation)
    {
        /** @var OperationService $operationService */
        $operationService = app('operations');
        $operation = $operationService->typeByOperation($operation)->success($operation);
        return $operation->loadMissing('currency', 'user', 'user.inviter', 'payment_system');
    }
}
