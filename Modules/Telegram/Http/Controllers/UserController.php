<?php

namespace Modules\Telegram\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\Api\CreateUserRequest;
use Modules\Core\Http\Controllers\Controller;
use Modules\Core\Models\User;

class UserController extends Controller
{
    public function index(Request $request)
    {
        return app('zengine')->model('User')->with('currency', 'inviter')
            ->latest()
            ->when(request('id'), function (Builder $when) {
                return $when->where('id', request('id'));
            })
            ->when(request('login'), function (Builder $when) {
                return $when->where('login', request('login'));
            })
            ->when(request('email'), function (Builder $when) {
                return $when->where('email', request('email'));
            })
            ->paginate(5);
    }

    public function search(Request $request)
    {
        return app('zengine')->model('User')->with('currency', 'inviter')
            ->where('id', request('id'))
            ->orWhere('login', request('login'))
            ->orWhere('email', request('email'))
            ->first();
    }

    public function check(Request $request)
    {
        $exists = app('zengine')->model('User')->
            when(request('login', ''), function (Builder $when) {
                return $when->where('login', request('login'));
            })
            ->when(request('email', ''), function (Builder $when) {
                return $when->orWhere('email', request('email'));
            })
            ->count();
        return [
            'status' => $exists ? 'exist' : 'success'
        ];
    }

    public function delete(User $user)
    {
        return [
            'status' => $user->delete() ? 'success' : 'error'
        ];
    }

    /**
     * @param  CreateUserRequest  $request
     * @return array
     */
    public function store(CreateUserRequest $request)
    {
        $data = $request->only(['login', 'email']) + [
                'name'              => $request->get('login'),
                'password'          => \Hash::make($request->get('password')),
                'email_verified_at' => now()
            ];
        $user = app('zengine')->model('User')->create($data);

        return [
            'status' => 'success',
            'user'   => $user
        ];
    }
}
