<?php

namespace Modules\Telegram\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\Models\Operation;
use Modules\Core\Models\PaymentSystem;

class TelegramController extends Controller
{
    public function login(Request $request)
    {
        if (\Cache::get('telegram_bot_key')) {
            if ($request->get('token') === \Cache::get('telegram_bot_key')) {
                $token = app('zengine')->model('User')->find(1)->createToken('tg_bot');
                return [
                    'token'     => $token->accessToken,
                    'message'   => 'authorized'
                ];
            } else {
                return [
                    'message' => 'token not match'
                ];
            }
        }
        return [
            'message' => 'token not created, enter to settings, for generate'
        ];
    }

    public function stats(Request $request)
    {
        $currencyService = app('currencies');
        $from = Carbon::parse($request->get('from', null));
        $to = Carbon::parse($request->get('to', null));
        $withdraws_sum = app('zengine')->model('Operation')->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW)
            ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)
            ->whereDate('created_at', '>=', $from)
            ->whereDate('created_at', '<=', $to)
            ->select(['amount', 'currency_id'])
            ->get()
            ->sum(function (Operation $operation) {
                return $operation->default_amount;
            });
        $refills_sum = app('zengine')->model('Operation')->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_REFILL)
            ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)
            ->whereDate('created_at', '>=', $from)
            ->whereDate('created_at', '<=', $to)
            ->select(['amount', 'currency_id'])
            ->get()
            ->sum(function (Operation $operation) {
                return $operation->default_amount;
            });
        $registrations_count = app('zengine')->model('User')->whereDate('created_at', '>=', $from)
            ->whereDate('created_at', '<=', $to)->count();
        $refs_sum = app('zengine')->model('Operation')->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_REFILL)
            ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)
            ->whereDate('created_at', '>=', $from)
            ->whereDate('created_at', '<=', $to)
            ->select(['amount', 'currency_id'])
            ->get()
            ->sum(function (Operation $operation) {
                return $operation->default_amount;
            });

        return [
            'default_currency'      => $currencyService->getDefaultCurrency(),
            'withdraws_sum'         => $withdraws_sum,
            'refills_sum'           => $refills_sum,
            'registrations_count'   => $registrations_count,
            'referrals_sum'         => $refs_sum,

        ];
    }

    /**
     * @param  PaymentSystem  $paymentSystem
     * @return PaymentSystem
     */
    public function payment_balance(PaymentSystem $paymentSystem)
    {
        $provider = $paymentSystem->getProvider();
        if (is_array($paymentSystem->credentials) && count($paymentSystem->credentials)) {
            $provider->setCredentials($paymentSystem->credentials);
        }
        if ($provider) {
            $balance = $provider->getBalance();
            if ($balance) {
                $paymentSystem->balance = $balance;
            } else {
                $paymentSystem->balance = 0.00;
            }
        }
        $paymentSystem->save();
        return $paymentSystem;
    }

    /**
     * @return array
     */
    public function payments_balances()
    {
        $payment_systems = app('zengine')->model('PaymentSystem')->with('currency')->get();
        $payments = $payment_systems->map(function (PaymentSystem $paymentSystem) {
            try {
                return $this->payment_balance($paymentSystem);
            } catch (\Exception $exception) {
                $paymentSystem->balance = 'error';
                return $paymentSystem;
            }
        });
        return [
            'payments' => $payments
        ];
    }
}
