#!/usr/bin/env bash

set -e

role=${CONTAINER_ROLE:-app}
env=${APP_ENV:-production}

if [ "$role" = "queue" ]; then

    echo "Queue role"
    php /app/artisan queue:work --verbose --tries=3 --timeout=90
    exit 1

elif [ "$role" = "scheduler" ]; then

    echo "Scheduler role"
    while [ true ]
    do
      php /app/artisan schedule:run --verbose --no-interaction &
      sleep 60
    done
    exit 1

else
    echo "Could not match the container role \"$role\""
    exit 1
fi
