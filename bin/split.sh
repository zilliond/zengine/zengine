#!/usr/bin/env bash

set -e
set -x

CURRENT_BRANCH="1.x"

function split()
{
    local SHA1
    SHA1=$(./bin/splitsh-lite --prefix=$1)
    git push "$2" "$SHA1:refs/heads/$CURRENT_BRANCH" -f
}

function remote()
{
    git remote add "$1" "$2" || true
}

git pull origin $CURRENT_BRANCH

remote core git@gitlab.com:zilliond/zengine/core.git
remote admin git@gitlab.com:zilliond/zengine/admin.git
remote telegram git@gitlab.com:zilliond/zengine/telegram.git

split 'Modules/Core' core
split 'Modules/Admin' admin
split 'Modules/Telegram' telegram
