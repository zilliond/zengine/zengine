<?php

use Modules\Core\CurrencyRateUpdaters\BloombergRapidCurrencyController;
use Modules\Core\CurrencyRateUpdaters\CoinLayerCurrencyProvider;
use Modules\Core\CurrencyRateUpdaters\OpenExchangeRatesFreeCurrencyProvider;
use Modules\Core\CurrencyRateUpdaters\OpenExchangeRatesPaidCurrencyProvider;
use Modules\Core\Payments\AdvancedCashProvider;
use Modules\Core\Payments\CryptonatorProvider;
use Modules\Core\Payments\FreeKassaProvider;
use Modules\Core\Payments\ManualPaymentProvider;
use Modules\Core\Payments\PayeerProvider;
use Modules\Core\Payments\PerfectMoneyProvider;
use Modules\Core\Payments\QiwiProvider;

return [
    'models'            => [
        'AuthLog'        => Modules\Core\Models\AuthLog::class,
        'BlacklistEntry' => Modules\Core\Models\BlacklistEntry::class,
        'Currency'       => Modules\Core\Models\Currency::class,
        'CurrencyRate'   => Modules\Core\Models\CurrencyRate::class,
        'Deposit'        => Modules\Core\Models\Deposit::class,
        'FaqItem'        => Modules\Core\Models\FaqItem::class,
        'Operation'      => Modules\Core\Models\Operation::class,
        'Page'           => Modules\Core\Models\Page::class,
        'PaymentSystem'  => Modules\Core\Models\PaymentSystem::class,
        'Plan'           => Modules\Core\Models\Plan::class,
        'Post'           => Modules\Core\Models\Post::class,
        'Promo'          => Modules\Core\Models\Promo::class,
        'Referral'       => Modules\Core\Models\Referral::class,
        'SocialAccount'  => Modules\Core\Models\SocialAccount::class,
        'Stat'           => Modules\Core\Models\Stat::class,
        'Ticket'         => Modules\Core\Models\Ticket::class,
        'TicketMessage'  => Modules\Core\Models\TicketMessage::class,
        'User'           => Modules\Core\Models\User::class,
        'UserBalance'    => Modules\Core\Models\UserBalance::class,
        'Wallet'         => Modules\Core\Models\Wallet::class,
    ],
    'services'          => [
        'currencies'       => \Modules\Core\Services\CurrencyService::class,
        'deposits'         => \Modules\Core\Services\DepositService::class,
        'operations'       => \Modules\Core\Services\OperationService::class,
        'users'            => \Modules\Core\Services\UserService::class,
        'social_auth'      => \Modules\Core\Services\SocialAuthService::class,
        'currency-updater' => \Modules\Core\Services\CurrencyUpdaterService::class,
    ],
    'auth'              => [
        'username'                          => 'login',  // User model attribute for username
        'custom_register_fields'            => [],       // Custom attributes for filling on registration
        'custom_register_fields_validation' => [],       // Validation for custom attributes
    ],
    'payment_providers' => [
        'manual'        => ManualPaymentProvider::class,
        'payeer'        => PayeerProvider::class,
        'perfect_money' => PerfectMoneyProvider::class,
        'adv_cash'      => AdvancedCashProvider::class,
        'cryptonator'   => CryptonatorProvider::class,
        'freekassa'     => FreeKassaProvider::class,
        'qiwi'          => QiwiProvider::class,
    ],
    'operations'        => [
        'types' => [
            'user_refill'     => \Modules\Core\OperationTypes\UserRefillOperationType::class,
            'user_withdraw'   => \Modules\Core\OperationTypes\UserWithdrawOperationType::class,
            'user_transfer'   => \Modules\Core\OperationTypes\UserTransferOperationType::class,
            'user_bonus'      => \Modules\Core\OperationTypes\UserBonusOperationType::class,
            'deposit_open'    => \Modules\Core\OperationTypes\DepositOpenOperationType::class,
            'deposit_accrual' => \Modules\Core\OperationTypes\DepositAccrualOperationType::class,
            'deposit_close'   => \Modules\Core\OperationTypes\DepositCloseOperationType::class,
            'referral_pay'    => \Modules\Core\OperationTypes\ReferralPayOperationType::class,
        ],
    ],
    'currency_updater'  => [
        'default_provider'  => 'openexchangerates_free',
        'providers'         => [
            'openexchangerates_free' => OpenExchangeRatesFreeCurrencyProvider::class,
            'openexchangerates_paid' => OpenExchangeRatesPaidCurrencyProvider::class,
            'bloomberg_rapid'        => BloombergRapidCurrencyController::class,
            'coin_layer'             => CoinLayerCurrencyProvider::class,
        ],
        'openexchangerates' => [
            'client_id' => env('OPENEXCHANGE_ID'),
        ],
        'bloomberg_rapid'   => [
            'api_key' => env('RAPIDAPI_KEY'),
        ],
        'coin_layer'        => [
            'api_key' => env('COINLAYER_KEY'),
        ],
    ],
];
