<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Default Settings Store
    |--------------------------------------------------------------------------
    |
    | This option controls the default settings store that gets used while
    | using this settings library.
    |
    | Supported: "json", "database"
    |
    */
    'store'              => 'json',

    /*
    |--------------------------------------------------------------------------
    | JSON Store
    |--------------------------------------------------------------------------
    |
    | If the store is set to "json", settings are stored in the defined
    | file path in JSON format. Use full path to file.
    |
    */
    'path'               => storage_path() . '/settings.json',

    /*
    |--------------------------------------------------------------------------
    | Database Store
    |--------------------------------------------------------------------------
    |
    | The settings are stored in the defined file path in JSON format.
    | Use full path to JSON file.
    |
    */
    // If set to null, the default connection will be used.
    'connection'         => null,
    // Name of the table used.
    'table'              => 'settings',
    // Cache usage configurations.
    'enableCache'        => true,
    'forgetCacheByWrite' => true,
    'cacheTtl'           => 15,
    // If you want to use custom column names in database store you could
    // set them in this configuration
    'keyColumn'          => 'key',
    'valueColumn'        => 'value',

    /*
    |--------------------------------------------------------------------------
    | Default Settings
    |--------------------------------------------------------------------------
    |
    | Define all default settings that will be used before any settings are set,
    | this avoids all settings being set to false to begin with and avoids
    | hardcoding the same defaults in all 'Settings::get()' calls
    |
    */
    'social_providers'   => [
        'facebook',
        'vkontakte',
        'mailru',
        'google',
        'microsoft',
        'instagram',
        'linkedin',
        'adobe',
    ],
    'defaults'           => [
        'system'        => [
            'balances_type' => env('BALANCES_TYPE'),
            'currency'      => env('SYSTEM_CURRENCY'),
        ],
        'balances_type' => env('BALANCES_TYPE'),
        'users'         => [
            '2fa' => 'google_2fa_optional',
        ],
        'admin'         => [
            'url' => 'admin',
        ],
        'currencies'    => [
            'default' => 1,
            'rate'    => [
                'decimal_total'     => 17,
                'decimal_precision' => 8, // TODO: Need to remigrate for change database
            ],
            'amount'  => [
                'decimal_total'     => 17,
                'decimal_precision' => 8, // TODO: Need to remigrate for change database
            ],
        ],
        'fake'          => [
            'active_users'        => 0,
            'users'               => 0,
            'online_users'        => 0,
            'today_deposits_sums' => 0,
            'refills_sum'         => 0,
            'withdraws_sum'       => 0,
        ],
        'refsys'        => [
            'depo_create_percents'   => '10-5-3-2-1',
            'depo_accruals_percents' => '10-5-3-2-1',
        ],
        'general'       => [
            'enabled'                   => true,
            'log_ip'                    => true,
            'demo_registration'         => true,
            'admin_mail'                => '',
            'max_withdraw'              => 0,
            'min_withdraw'              => 0,
            'withdraws_daily_limit'     => 0,
            'withdraw_api_limit'        => 0,
            'start_date'                => "",
            'reinvestment'              => true,
            'registration_confirmation' => true,
            'accrual_enabled'           => true,
            'ssl_enabled'               => true,
            'tg_bot_enabled'            => true,
        ],
        'sockets'       => [
            'type'    => env('MIX_ECHO_TYPE'),
            'host'    => env('MIX_ECHO_HOST'),
            'key'     => env('PUSHER_APP_KEY'),
            'cluster' => env('PUSHER_APP_CLUSTER'),
        ],
        'social_auth'   => [
            'enabled'                 => true,
            'facebook_enabled'        => env('FACEBOOK_ENABLED'),
            'facebook_client_id'      => env('FACEBOOK_CLIENT_ID'),
            'facebook_client_secret'  => env('FACEBOOK_CLIENT_SECRET'),
            'vkontakte_enabled'       => env('VKONTAKTE_ENABLED'),
            'vkontakte_client_id'     => env('VKONTAKTE_CLIENT_ID'),
            'vkontakte_client_secret' => env('VKONTAKTE_CLIENT_SECRET'),
            'mailru_enabled'          => env('MAILRU_ENABLED'),
            'mailru_client_id'        => env('MAILRU_CLIENT_ID'),
            'mailru_client_secret'    => env('MAILRU_CLIENT_SECRET'),
            'google_enabled'          => env('GOOGLE_ENABLED'),
            'google_client_id'        => env('GOOGLE_CLIENT_ID'),
            'google_client_secret'    => env('GOOGLE_CLIENT_SECRET'),
            'microsoft_enabled'       => env('MICROSOFT_ENABLED'),
            'microsoft_client_id'     => env('MICROSOFT_CLIENT_ID'),
            'microsoft_client_secret' => env('MICROSOFT_CLIENT_SECRET'),
            'instagram_enabled'       => env('INSTAGRAM_ENABLED'),
            'instagram_client_id'     => env('INSTAGRAM_CLIENT_ID'),
            'instagram_client_secret' => env('INSTAGRAM_CLIENT_SECRET'),
            'linkedin_enabled'        => env('LINKEDIN_ENABLED'),
            'linkedin_client_id'      => env('LINKEDIN_CLIENT_ID'),
            'linkedin_client_secret'  => env('LINKEDIN_CLIENT_SECRET'),
            'adobe_enabled'           => env('ADOBE_ENABLED'),
            'adobe_client_id'         => env('ADOBE_CLIENT_ID'),
            'adobe_client_secret'     => env('ADOBE_CLIENT_SECRET'),
        ],
    ],

    'fields' => [
        'general'    => [
            'enabled'                   => [
                'label'   => 'Работа сайта',
                'help'    => 'Работа сайта',
                'type'    => 'toggle',
                'default' => true,
            ],
            'log_ip'                    => [
                'label'   => 'Отслеживатель IP',
                'help'    => 'Записывать IP в лог',
                'type'    => 'toggle',
                'default' => true,
            ],
            'demo_registration'         => [
                'label'   => 'Возможности регистрации DEMO',
                'help'    => 'Возможности регистрации DEMO',
                'type'    => 'toggle',
                'default' => true,
            ],
            'admin_mail'                => [
                'label'   => 'E-mail администратора',
                'help'    => 'E-mail администратора',
                'type'    => 'text',
                'default' => '',
            ],
            'max_withdraw'              => [
                'label'   => 'Максимальная сумма на вывод',
                'help'    => 'Максимальная сумма на вывод',
                'type'    => 'float',
                'default' => 0,
            ],
            'min_withdraw'              => [
                'label'   => 'Минимальная сумма на вывод',
                'help'    => 'Минимальная сумма на вывод',
                'type'    => 'float',
                'default' => 0,
            ],
            'withdraws_daily_limit'     => [
                'label'   => 'Дневной лимит на вывод',
                'help'    => 'Дневной лимит на вывод',
                'type'    => 'integer',
                'default' => 0,
            ],
            'withdraw_api_limit'        => [
                'label'   => 'Максимальный вывод через API',
                'help'    => 'Максимальный вывод через API',
                'type'    => 'float',
                'default' => 0,
            ],
            'start_date'                => [
                'label'   => 'Дата старта проекта',
                'help'    => 'Дата старта проекта',
                'type'    => 'date',
                'default' => '',
            ],
            'reinvestment'              => [
                'label'   => 'Реинвестирование',
                'help'    => 'Реинвестирование',
                'type'    => 'toggle',
                'default' => true,
            ],
            'registration_confirmation' => [
                'label'   => 'Подтверждение регистрации по e-mail',
                'help'    => 'Подтверждение регистрации по e-mail',
                'type'    => 'toggle',
                'default' => true,
            ],
            'accrual_enabled'           => [
                'label'   => 'Начисление процентов',
                'help'    => 'Начисление процентов',
                'type'    => 'toggle',
                'default' => true,
            ],
            'ssl_enabled'               => [
                'label'   => 'SSL',
                'help'    => 'SSL',
                'type'    => 'toggle',
                'default' => true,
            ],
            'tg_bot_enabled'            => [
                'label'   => 'Telegram бот',
                'help'    => 'Включить Telegram бот',
                'type'    => 'toggle',
                'default' => true,
            ],
        ],
        'fake'       => [
            'active_users'        => [
                'label'   => 'Добавить активных пользователей',
                'help'    => 'Добавить активных пользователей',
                'icon'    => 'network',
                'type'    => 'number',
                'default' => 0,
            ],
            'users'               => [
                'label'   => 'Добавить фейковых пользователей',
                'help'    => 'Добавить фейковых пользователей',
                'icon'    => 'fake3',
                'type'    => 'number',
                'default' => 0,
            ],
            'online_users'        => [
                'label'   => 'Добавить пользователей онлайн',
                'help'    => 'Добавить пользователей онлайн',
                'icon'    => 'fake2',
                'type'    => 'number',
                'default' => 0,
            ],
            'today_deposits_sums' => [
                'label'   => 'Добавить к сумме депозитов сегодня',
                'help'    => 'Добавить к сумме депозитов сегодня',
                'icon'    => 'portfolio-of-business',
                'type'    => 'number',
                'default' => 0,
            ],
            'refills_sum'         => [
                'label'   => 'Добавить/отнять сумму вывода **',
                'help'    => 'Добавить/отнять сумму вывода **',
                'icon'    => 'level-up',
                'type'    => 'number',
                'default' => 0,
            ],
            'withdraws_sum'       => [
                'label'   => 'Добавить/отнять сумму пополнений **',
                'help'    => 'Добавить/отнять сумму пополнений **',
                'icon'    => 'add',
                'type'    => 'number',
                'default' => 0,
            ],
        ],
        'users'      => [
            '2fa' => [
                'label'   => 'Двухфакторная аутентификация',
                'help'    => '',
                'type'    => 'select',
                'values'  => [
                    'disabled'            => 'Отключена',
                    'google_2fa_optional' => 'Google Authenticator по желанию',
                ],
                'default' => 'google_2fa_optional',
            ],
        ],
        'admin'      => [
            'url' => [
                'label'   => 'Префикс для админ-панели',
                'help'    => 'Префикс для путей в админке(' . config('app.url') . '/ваш_префикс/users)',
                'type'    => 'text',
                'default' => 'admin',
            ],
        ],
        'currencies' => [
            'default' => [
                'label'   => 'Валюта по умолчанию',
                'help'    => 'Базовая валюта на сайте и у пользователей',
                'type'    => 'select',
                'values'  => function () {
                    return app('zengine')->model('Currency')->pluck('code_iso', 'id')->toArray();
                },
                'default' => 1,
            ],
        ],
        'refsys'     => [
            'depo_create_percents'   => [
                'label'   => 'Реферальные проценты за создание депозита',
                'help'    => 'Проценты по уровням в формате level1-level2-level3....',
                'type'    => 'text',
                'default' => '10-5-3-2-1',
            ],
            'depo_accruals_percents' => [
                'label'   => 'Реферальные проценты за начисления по депозиту',
                'help'    => 'Проценты по уровням в формате level1-level2-level3....',
                'type'    => 'text',
                'default' => '10-5-3-2-1',
            ],
        ],
    ],
];
