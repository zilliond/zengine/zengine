<?php

return [
    'root' => [
        /**
         * @see /vendor/martinlindhe/laravel-vue-i18n-generator/src/config/vue-i18n-generator.php
         * @see https://github.com/martinlindhe/laravel-vue-i18n-generator/blob/master/src/config/vue-i18n-generator.php
         */
        'langPath'  => 'resources/lang',
        'langFiles' => [
            'ui',
            'invest',
        ],
        'excludes' => [
            /*
            'validation',
            'example.file',
            'example-folder',
            */
        ],
        'jsPath'             => '/resources/js/langs/',
        'jsFile'             => '/resources/js/lang.generated.js',
        'output_format'      => 'es6', // es6(ex. babel),umd(ex. browser),json
        'i18nLib'            => 'vue-i18n',
        'showOutputMessages' => false,
        'escape_char'        => '!',
    ],
    'admin' => [
        /**
         * @see /vendor/martinlindhe/laravel-vue-i18n-generator/src/config/vue-i18n-generator.php
         * @see https://github.com/martinlindhe/laravel-vue-i18n-generator/blob/master/src/config/vue-i18n-generator.php
         */
        'langPath'  => 'Modules/Admin/Resources/lang',
        'langFiles' => [
            // all, admin only vue
        ],
        'excludes' => [
        ],
        'jsPath'             => '/Modules/Admin/Resources/assets/js/langs/',
        'jsFile'             => '/Modules/Admin/Resources/assets/js/lang.generated.js',
        'output_format'      => 'es6', // es6(ex. babel),umd(ex. browser),json
        'i18nLib'            => 'vue-i18n',
        'showOutputMessages' => false,
        'escape_char'        => '!',
    ],
];
