<form action="{{ $operation->payment_system_data['action'] }}" method="{{ $operation->payment_system_data['method'] }}" id="form">
    @foreach($operation->payment_system_data['form_data'] as $key => $value)
        <input type="hidden" name="{{ $key }}" value="{{ $value }}">
    @endforeach
    <input type="submit">
</form>
<script>
    function ready() {
        var form = document.getElementById('form');
        form.submit();
    }
    document.addEventListener("DOMContentLoaded", ready);
</script>
