@extends('layouts.app')

@section('title', 'News')

@section('content')
    @php/* @var Modules\Core\Models\Post $post */@endphp
    <div class="col-md-10 blogShort">
        <h1>{{ $post->title }}</h1>
        <img src="{{ $post->file_url }}" alt="post img" class="pull-left img-responsive postImg img-thumbnail margin10">
        <article>
            @markdown($post->body)
        </article>
    </div>
@endsection
