@extends('layouts.app')

@section('title', 'News')

@section('css')
    <style>
        .blogShort{
            border-bottom: 1px solid #ddd;
            display: inline-block;
        }
    </style>
@endsection

@section('content')
    <div class="col-md-10">
        <h1>News</h1>
        @foreach($posts as $post)
            @php/* @var Modules\Core\Models\Post $post */@endphp
            <div class="col-md-12 blogShort">
                <h1>{{ $post->title }}</h1>
                <img src="{{ $post->file_url }}" alt="post img" class="float-left img-responsive thumb margin10 img-thumbnail">
                <article><p>
                        {!! $post->description  !!}
                    </p></article>
                <a class="btn btn-primary float-right mb-1" href="{{ route('news.show', $post) }}">READ MORE</a>
            </div>
        @endforeach
        {{ $posts->links() }}
    </div>
@endsection
