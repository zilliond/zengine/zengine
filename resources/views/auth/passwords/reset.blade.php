<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Larahyip</title>

    <link rel="stylesheet" href="/css/auth.css">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="auth">
<div class="auth__wrapper">
    <a href="/">
        <img src="/auth/zengine_logo.png" class="auth__logo">
    </a>
    <div class="auth__box">
        <h1 class="auth__title">Сбросить пароль</h1>
        <form method="post" action="{{ url('/password/reset') }}" class="auth__form">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="auth__form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="auth__form-input" name="email" value="{{ old('email', request('email')) }}" placeholder="Email">
                @if ($errors->has('email'))
                    <span class="auth__form-help">
                        {{ $errors->first('email') }}
                    </span>
                @endif
            </div>
            <div class="auth__form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="auth__form-input" name="password" value="{{ old('password') }}" placeholder="Пароль">
                @if ($errors->has('password'))
                    <span class="auth__form-help">
                        {{ $errors->first('password') }}
                    </span>
                @endif
            </div>
            <div class="auth__form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input type="password" class="auth__form-input" name="password_confirmation" value="{{ old('password_confirmation') }}" placeholder="Повторите пароль">
                @if ($errors->has('password_confirmation'))
                    <span class="auth__form-help">
                        {{ $errors->first('password_confirmation') }}
                    </span>
                @endif
            </div>
            <button class="auth__form-submit" type="submit">Сбросить пароль</button>
            <div class="auth__links">
                <a class="auth__link" href="{{ url('/login') }}">Вход</a>
                <a class="auth__link" href="{{ url('/register') }}">Регистрация</a>
            </div>
        </form>
    </div>
    @php
        $socialService = app('social_auth');
        /** @var \Modules\Core\Services\Contracts\SocialAuthServiceInterface $socialService  */
    @endphp
    <svg style="position: absolute; width: 0px; height: 0px;">
        <defs>
            <linearGradient id="default">
                <stop offset="0%" stop-color="#0bbafb"></stop>
                <stop offset="100%" stop-color="#4285ec"></stop>
            </linearGradient>
        </defs>
    </svg>
    <div class="auth__social">
        @foreach($socialService->getActiveProviders() as $provider)
            <a href="{{ route('social_login', ['provider' => $provider]) }}" class="auth__icon">
                <svg>
                    <use xlink:href="/auth/socials.svg#{{ $provider }}" fill="url(#default)"></use>
                </svg>
            </a>
        @endforeach
    </div>
</div>
</body>
</html>
