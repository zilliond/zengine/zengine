<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Larahyip</title>

    <link rel="stylesheet" href="/css/auth.css">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="auth">
<div class="auth__wrapper">
    <a href="/">
        <img src="/auth/zengine_logo.png" class="auth__logo">
    </a>
    <div class="auth__box">
        <h1 class="auth__title">Регистрация</h1>
        @if($inviter)
            <p class="auth__sub-title">Вас пригласил: {{ $inviter->login }}({{ $inviter->name }})</p>
        @endif
        <form method="post" action="{{ url('/register') }}" class="auth__form">
            @csrf
            <div class="auth__form-group {{ $errors->has('login') ? ' has-error' : '' }}">
                <input type="text" class="auth__form-input" name="login" value="{{ old('login') }}" placeholder="Логин">
                @if ($errors->has('login'))
                    <span class="auth__form-help">
                        {{ $errors->first('login') }}
                    </span>
                @endif
            </div>
            <div class="auth__form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                <input type="text" class="auth__form-input" name="name" value="{{ old('name') }}" placeholder="Имя">
                @if ($errors->has('name'))
                    <span class="auth__form-help">
                        {{ $errors->first('name') }}
                    </span>
                @endif
            </div>
            <div class="auth__form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="auth__form-input" name="email" value="{{ old('email') }}" placeholder="Email">
                @if ($errors->has('email'))
                    <span class="auth__form-help">
                        {{ $errors->first('email') }}
                    </span>
                @endif
            </div>
            <div class="auth__form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="auth__form-input" name="password" value="{{ old('password') }}" placeholder="Пароль">
                @if ($errors->has('password'))
                    <span class="auth__form-help">
                        {{ $errors->first('password') }}
                    </span>
                @endif
            </div>
            <div class="auth__form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input type="password" class="auth__form-input" name="password_confirmation" value="{{ old('password_confirmation') }}" placeholder="Повторите пароль">
                @if ($errors->has('password_confirmation'))
                    <span class="auth__form-help">
                        {{ $errors->first('password_confirmation') }}
                    </span>
                @endif
            </div>
            <div class="auth__form-group">
                <div class="auth__form-checkbox">
                    <label>
                        <input type="checkbox" name="agree"> Согласен с правилами
                    </label>
                </div>
            </div>
            <button class="auth__form-submit" type="submit">Зарегистрироваться</button>
            <div class="auth__links">
                <a class="auth__link" href="{{ url('/login') }}">Вход</a>
                <a class="auth__link" href="{{ url('/password/reset') }}">Вспомнить пароль</a>
            </div>
        </form>
    </div>
    @php
        $socialService = app('social_auth');
        /** @var \Modules\Core\Services\Contracts\SocialAuthServiceInterface $socialService  */
    @endphp
    <svg style="position: absolute; width: 0px; height: 0px;">
        <defs>
            <linearGradient id="default">
                <stop offset="0%" stop-color="#0bbafb"></stop>
                <stop offset="100%" stop-color="#4285ec"></stop>
            </linearGradient>
        </defs>
    </svg>
    <div class="auth__social">
        @foreach($socialService->getActiveProviders() as $provider)
            <a href="{{ route('social_login', ['provider' => $provider]) }}" class="auth__icon">
                <svg>
                    <use xlink:href="/auth/socials.svg#{{ $provider }}" fill="url(#default)"></use>
                </svg>
            </a>
        @endforeach
    </div>
</div>
</body>
</html>
