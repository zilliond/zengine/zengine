<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Larahyip</title>

    <link rel="stylesheet" href="/css/auth.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="auth">
<div class="auth__wrapper">
    <a href="/">
        <img src="/auth/zengine_logo.png" class="auth__logo">
    </a>
    <div class="auth__box">
        <h1 class="auth__title">Вход</h1>
        <form method="post" action="{{ url('/login') }}" class="auth__form">
            @php
                $username = config('zengine.auth.username');
            @endphp
            @csrf
            <div class="auth__form-group {{ $errors->has($username) ? ' has-error' : '' }}">
                <input type="{{ $username === 'email' ? 'email' : 'text' }}" class="auth__form-input" name="{{ $username }}" value="{{ old($username) }}" placeholder="{{ $username }}">
                @if ($errors->has($username))
                    <span class="auth__form-help">
                        {{ $errors->first($username) }}
                    </span>
                @endif
            </div>
            <div class="auth__form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="auth__form-input" name="password" value="{{ old('password') }}" placeholder="Пароль">
                @if ($errors->has('password'))
                    <span class="auth__form-help">
                        {{ $errors->first('password') }}
                    </span>
                @endif
            </div>
            @if(setting('users.2fa') !== 'disabled')
                <div class="auth__form-group {{ $errors->has(config('google2fa.otp_input')) ? ' has-error' : '' }}">
                    <input type="text" class="auth__form-input" name="{{ config('google2fa.otp_input') }}"
                           value="{{ old(config('google2fa.otp_input')) }}" placeholder="2FA код">
                    @if ($errors->has(config('google2fa.otp_input')))
                        <span class="auth__form-help">
                        {{ $errors->first(config('google2fa.otp_input')) }}
                    </span>
                    @endif
                </div>
            @endif
            <div class="auth__form-group">
                <div class="auth__form-checkbox">
                    <label>
                        <input type="checkbox" name="remember"> Запомнить
                    </label>
                </div>
            </div>
            <button class="auth__form-submit" type="submit">Войти</button>
            <div class="auth__links">
                <a class="auth__link" href="{{ url('/password/reset') }}">Вспомнить пароль</a>
                <a class="auth__link" href="{{ url('/register') }}">Регистрация</a>
            </div>
        </form>
    </div>
    @php
        $socialService = app('social_auth');
        /** @var \Modules\Core\Services\Contracts\SocialAuthServiceInterface $socialService  */
    @endphp
    <svg style="position: absolute; width: 0px; height: 0px;">
        <defs>
            <linearGradient id="default">
                <stop offset="0%" stop-color="#0bbafb"></stop>
                <stop offset="100%" stop-color="#4285ec"></stop>
            </linearGradient>
        </defs>
    </svg>
    <div class="auth__social">
        @foreach($socialService->getActiveProviders() as $provider)
            <a href="{{ route('social_login', ['provider' => $provider]) }}" class="auth__icon">
                <svg>
                    <use xlink:href="/auth/socials.svg#{{ $provider }}" fill="url(#default)"></use>
                </svg>
            </a>
        @endforeach
    </div>
</div>
</body>
</html>
