@extends('layouts.app')
@section('containerClass', 'container-fluid')
@section('content')
    <div class="row justify-content-around col-11 offset-1">
        <div class="col-md-2">
            @include('layouts.cabinet_menu')
        </div>
        <div class="col-md-10">
            @yield('body')
        </div>
        @if(setting('users.2fa') !== 'disabled')
            <unlock-modal action_url="{{ route('cabinet.security.pin.unlock') }}" v-if="unlock_modal" @close="unlock_modal = false"></unlock-modal>
        @endif
    </div>
@endsection
