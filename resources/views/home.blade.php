@extends('layouts.app')

@section('title', 'Larahyip')

@section('content')
    <div>
        <img src="https://picsum.photos/seed/zil/1200/300" class="home-top__image" alt="">
        <h2>@lang('home.plans.title')</h2>
        <div class="card-deck">
        @php
            /* @var Modules\Core\Models\Plan $plan */
            $currencyService = app('currencies');
        @endphp
        @foreach($plans as $plan)
            <div class="card mb-3">
                <div class="card-body">
                    <h5 class="card-title">{{ $plan->name }}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">{{ $plan->duration }} {{ $plan->type === app('zengine')->modelClass('Plan')::TYPE_HOURLY ? trans('ui.hours') : trans('ui.days') }}</h6>
                    <p class="card-text">
                        @lang('home.plans.amount'): {{ $plan->min_amount }}{{ $plan->currency->symbol }} - {{ $plan->max_amount }}{{ $plan->currency->symbol }} <br>
                        @lang('home.plans.percent'): {{ $plan->percent }}% <br>
                        @lang('home.plans.charge_weekends'): {{ $plan->not_charge_weekends ? trans('ui.no') : trans('ui.yes') }} <br>
                        @lang('home.plans.limit'): {{ $plan->is_limited ? $plan->max_deposits : trans('ui.no') }} <br>
                        @lang('home.plans.additional_referral_percent'): {{ $plan->additional_referral_percent ?? trans('ui.no') }} <br>
                    </p>
                    @guest
                        <a href="{{ route('register') }}" class="card-link btn btn-primary">@lang('ui.registration')</a>
                    @else
                        <a href="{{ route('cabinet.deposits.create', ['plan_id' => $plan->id]) }}" class="card-link btn btn-primary">@lang('home.plans.invest')</a>
                    @endguest
                </div>
            </div>
        @endforeach
        </div>
        <h2>@lang('home.statistics.title')</h2>
        <table class="table">
            <tbody>
            <tr>
                <th scope="row">@lang('home.statistics.users')</th>
                <td>{{ $stats['users'] }}</td>
            </tr>
            <tr>
                <th scope="row">@lang('home.statistics.active_users')</th>
                <td>{{ $stats['active_users'] }}</td>
            </tr>
            <tr>
                <th scope="row">@lang('home.statistics.online_users')</th>
                <td>{{ $stats['online_users'] }}</td>
            </tr>
            <tr>
                <th scope="row">@lang('home.statistics.today_deposits')</th>
                <td>{{ $stats['today_deposits'] }} {{ $currencyService->getDefaultCurrency()->symbol }}</td>
            </tr>
            <tr>
                <th scope="row">@lang('home.statistics.total_withdraws')</th>
                <td>{{ $stats['total_withdraws'] }} {{ $currencyService->getDefaultCurrency()->symbol }}</td>
            </tr>
            <tr>
                <th scope="row">@lang('home.statistics.total_refills')</th>
                <td>{{ $stats['total_refills'] }} {{ $currencyService->getDefaultCurrency()->symbol }}</td>
            </tr>
            <tr>
                <th scope="row">@lang('home.statistics.start_date')</th>
                <td>{{ $stats['start_date'] }}</td>
            </tr>
            </tbody>
        </table>
        <div class="row stats-row">
            <div class="col">
                <h3>Last Deps</h3>
                <table class="table table-dark">
                    <tr>
                        <th id="user">User</th>
                        <th id="amount">Amount</th>
                    </tr>
                    @foreach($last_deposits as $deposit)
                        <tr>
                            <td>{{ $deposit->user->login }}</td>
                            <td>{{ $deposit->amount }} {{ $deposit->currency->symbol }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="col">
                <h3>Top referrals</h3>
                <table class="table table-dark">
                    <tr>
                        <th id="user">User</th>
                        <th id="amount">Earned</th>
                    </tr>
                    @foreach($top_refs as $user)
                        <tr>
                            <td>{{ $user->login }}</td>
                            <td>{{ $user->referrals_earned }} {{ $user->currency->symbol }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
            <div class="col">
                <h3>Last Withdraws</h3>
                <table class="table table-dark">
                    <tr>
                        <th id="user">User</th>
                        <th id="payment">Payment</th>
                        <th id="amount">Amount</th>
                    </tr>
                    @foreach($last_withdraws as $withdraw)
                        <tr>
                            <td>{{ $withdraw->user->login }}</td>
                            <td>{{ optional($withdraw->payment_system)->name }}</td>
                            <td>{{ $withdraw->amount }} {{ $withdraw->currency->symbol }}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
