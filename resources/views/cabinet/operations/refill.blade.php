@extends('layouts.cabinet')

@section('body')
    @include('flash::message')

    <div class="clearfix"></div>
    <h1>@lang('cabinet/refill.title')</h1>
    @if(request('status', $status))
        @if(request('status', $status) === 'success')
            <div class="alert alert-success" role="alert">
                @lang('cabinet/refill.alerts.success')
            </div>
        @elseif(request('status', $status) === 'error')
            <div class="alert alert-danger" role="alert">
                @lang('cabinet/refill.alerts.error')
            </div>
        @endif
    @endif
    <form action="{{ route('cabinet.refill.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="amount_field">@lang('cabinet/refill.amount')</label>
            <input type="number" class="form-control" name="amount" id="amount_field" step="0.00001">
        </div>
        <div class="form-group">
            <label for="currency_id_field">@lang('cabinet/refill.currency')</label>
            <select name="currency_id" id="currency_id_field" class="form-control">
                @foreach($currencies as $currency)
                    <option value="{{ $currency->id }}" @if($currency->id === (int) old('currency_id'))selected @endif>{{ $currency->code_iso }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="payment_system_field">@lang('cabinet/refill.payment_system')</label>
            <select name="payment_system" id="payment_system_field" class="form-control">
                @foreach($payment_systems as $payment_system)
                    <option value="{{ $payment_system->id }}" @if($payment_system->id === (int) old('payment_system'))selected @endif>{{ $payment_system->name }}</option>
                @endforeach
            </select>
        </div>
        <button class="btn btn-primary" type="submit">@lang('cabinet/refill.submit')</button>
    </form>
@endsection
