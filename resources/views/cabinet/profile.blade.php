@extends('layouts.cabinet')

@section('body')
    @include('flash::message')
    @php /** @var $user \Modules\Core\Models\User */ @endphp
    <div class="clearfix"></div>
    <div class="row cabinet-profile">
        <div class="col-sm-12">
            <h1>@lang('cabinet/profile.title')</h1>
            {!! Form::model($user, ['route' => ['cabinet.profile.store'], 'method' => 'post']) !!}
            <div class="form-group">
                {!! Form::label('login', trans('cabinet/profile.form.login')) !!}
                {!! Form::text('login', null, ['class' => 'form-control ', 'readonly']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('name', trans('cabinet/profile.form.name')) !!}
                {!! Form::text('name', null, ['class' => 'form-control '.( $errors->has('name') ? ' is-invalid' : '' )]) !!}
            </div>
            <div class="form-group">
                {!! Form::label('email', trans('cabinet/profile.form.email')) !!}
                {!! Form::text('email', null, ['class' => 'form-control', 'readonly']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('currency_id', trans('cabinet/profile.form.currency')) !!}
                {!! Form::select('currency_id', app('zengine')->model('Currency')->pluck('code_iso', 'id'), null, ['class' => 'form-control' .( $errors->has('currency_id') ? ' is-invalid' : '' )]) !!}
            </div>
            <div class="form-group">
                {!! Form::label('invite_link', trans('cabinet/profile.form.invite_link')) !!}
                {!! Form::text('invite_link', null, ['class' => 'form-control', 'readonly']) !!}
            </div>
            @php
                $inviter = $user->inviter;
            @endphp
            @if($inviter)
                <div class="form-group">
                    {!! Form::label('inviter', 'Вас пригласил') !!}
                    {!! Form::text('inviter', "{$inviter->login}({$inviter->name})", ['class' => 'form-control', 'readonly']) !!}
                </div>
            @endif
            <button class="btn btn-primary" type="submit">@lang('cabinet/ui.save')</button>
            {!! Form::close() !!}
        </div>
    </div>
    <h2>@lang('cabinet/wallets.title')</h2>
    <form action="{{ route('cabinet.wallets.store') }}" method="POST">
        @csrf
        @foreach($payment_systems as $payment_system)
            @php /** @var \Modules\Core\Models\PaymentSystem $payment_system */ @endphp
            <div class="form-group">
                <label for="wallet_{{ $payment_system->id }}">{{ $payment_system->name }}</label>
                <input type="text" class="form-control" name="wallet_{{ $payment_system->id }}" id="wallet_{{ $payment_system->id }}"
                   @if($payment_system->wallets && $payment_system->wallets->count())
                       @cannot('can-modify', $payment_system->wallets[0])
                       readonly
                       @endcannot
                       value="{{ $payment_system->wallets[0]->wallet }}"
                    @endif
                >
            </div>
        @endforeach
        <button class="btn btn-primary" type="submit">@lang('cabinet/ui.save')</button>
        @can('locked')
            <button class="btn btn-secondary" type="button" @click="unlock_modal = true">@lang('cabinet/ui.unlock')</button>
        @endcan
    </form>
@endsection
@section('css')
    <style>
        .cabinet-profile .col-sm-12{
            margin-bottom: 1rem;
        }
    </style>
@endsection
