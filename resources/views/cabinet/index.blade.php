@extends('layouts.cabinet')

@section('body')
    <h1>{{ $user->login }} ({{ $user->balance }} {{ $user->currency->symbol }})</h1>
    <div class="row">
        @if(setting('balances_type') === 'divided')
        <div class="col-sm">
            @if(setting('balances_type') === 'divided')
                <h3>@lang('cabinet/home.wallets')</h3>
                <table class="table table-dark wallets-table">
                    <tr>
                        <th id="wallet">@lang('cabinet/home.wallet')</th>
                        <th id="balance">@lang('cabinet/home.balance')</th>
                    </tr>
                    @foreach($user->wallets as $wallet)
                        <tr>
                            <td>{{ $wallet->payment_system->name }}</td>
                            <td>{{ $wallet->balance }} {{ $wallet->payment_system->currency->symbol }}</td>
                        </tr>
                    @endforeach
                </table>
            @endif
        </div>
        @endif
        <div class="col-sm">
                <h3>@lang('cabinet/home.stats')</h3>
                <table class="table table-dark wallets-table">
                    <tr>
                        <td>Total Refills</td>
                        <td>{{ $stats['total_refills'] }} $</td>
                    </tr>
                    <tr>
                        <td>Total Withdraws</td>
                        <td>{{ $stats['total_withdraws'] }} $</td>
                    </tr>
                    <tr>
                        <td>Deposits opened count</td>
                        <td>{{ $stats['deposits_opened_count'] }}</td>
                    </tr>
                    <tr>
                        <td>Deposits closed sum</td>
                        <td>{{ $stats['deposits_closed_count'] }}</td>
                    </tr>
                    <tr>
                        <td>Deposits opened sum</td>
                        <td>{{ $stats['deposits_opened_sum'] }} $</td>
                    </tr>
                    <tr>
                        <td>Referrals earned</td>
                        <td>{{ $stats['referrals_earned'] }} $</td>
                    </tr>
                    <tr>
                        <td>Referrals count</td>
                        <td>{{ $stats['referrals_count'] }}</td>
                    </tr>
                    <tr>
                        <td>Accruals tomorrow</td>
                        <td>{{ $stats['accruals_tomorrow'] }} $</td>
                    </tr>
                </table>
        </div>
    </div>
@endsection
