@extends('layouts.cabinet')

@section('body')
    @include('flash::message')
    <h1>@lang('cabinet/support.tickets.title')</h1>
    <div class="form-group row pull-right">
        <div class="col-sm-10">
            <a class="btn btn-primary" href="{{ route('cabinet.tickets.create') }}">@lang('cabinet/support.tickets.create_new')</a>
        </div>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">@lang('cabinet/support.tickets.subject')</th>
            <th scope="col">@lang('cabinet/support.tickets.status')</th>
            <th scope="col">@lang('cabinet/support.tickets.actions')</th>
        </tr>
        </thead>
        <tbody>
        @foreach($tickets as $ticket)
            @php /* @var $ticket \Modules\Core\Models\Ticket */ @endphp
            <tr>
                <th scope="row">{{ $ticket->id }}</th>
                <td><a href="{{ route('cabinet.tickets.show', $ticket) }}">{{ $ticket->subject }}</a></td>
                <td>
                    @if($ticket->new_admin_messages->count())
                        <span class="text-primary">@lang('cabinet/support.tickets.new_messages', ['count' => $ticket->new_admin_messages->count()])</span>
                    @else
                        @if($ticket->status === app('zengine')->modelClass('Ticket')::STATUS_OPEN)
                            <span class="text-success">@lang('tickets.statuses.'.app('zengine')->modelClass('Ticket')::STATUS_OPEN )</span>
                        @elseif($ticket->status === app('zengine')->modelClass('Ticket')::STATUS_CLOSE)
                            <span class="text-danger">@lang('tickets.statuses.'.app('zengine')->modelClass('Ticket')::STATUS_CLOSE )</span>
                        @endif
                    @endif
                </td>
                <td>
                    @if($ticket->status === app('zengine')->modelClass('Ticket')::STATUS_OPEN)
                        {!! Form::open(['route' => ['cabinet.tickets.status', $ticket], 'method' => 'post']) !!}
                            <input type="hidden" name="status" value="{{ app('zengine')->modelClass('Ticket')::STATUS_CLOSE }}">
                            {!! Form::button('<i class="fas fa-times"></i>', ['type' => 'submit', 'class' => 'btn btn-primary btn-sm']) !!}
                        {!! Form::close() !!}
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $tickets->links() }}
@endsection
