@extends('layouts.cabinet')

@section('body')
    @include('flash::message')
    @php
    /** @var $ticket \Modules\Core\Models\Ticket */
    $messages = $ticket->messages->sortBy('created_at');
    @endphp
    <div class="container-fluid tickets">
        <h1>{{ $ticket->subject }}</h1>
        @foreach($messages as $message)
            <div class="card col-md-10 @if($message->user_id !== \Auth::id()) offset-md-2 @endif">
                <div class="card-header">
                    {{ $message->user->name }}
                </div>
                <div class="card-body">
                    <p>
                        {{ $message->message }}
                    </p>
                </div>
                <div class="card-footer text-muted">
                    {{ $message->created_at }}
                </div>
            </div>
        @endforeach
        @if($ticket->status === app('zengine')->modelClass('Ticket')::STATUS_OPEN)
            <form action="{{ route('cabinet.tickets.message', $ticket) }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="message_field">@lang('cabinet/support.ticket.message')</label>
                    <textarea class="form-control" id="message_field" rows="5" name="message"></textarea>
                </div>
                <button class="btn btn-primary" type="submit">@lang('cabinet/support.ticket.submit')</button>
            </form>
        @elseif($ticket->status === app('zengine')->modelClass('Ticket')::STATUS_CLOSE)
            {!! Form::open(['route' => ['cabinet.tickets.status', $ticket], 'method' => 'post']) !!}
            <input type="hidden" name="status" value="{{ app('zengine')->modelClass('Ticket')::STATUS_OPEN }}">
            {!! Form::button('Открыть заново', ['type' => 'submit', 'class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
        @endif
    </div>
@endsection

@section('css')
    <style>
        .tickets .card{
            margin-bottom: 1rem;
        }
        .card{
            padding: 0;
        }
    </style>
@endsection
@section('js')
    <script>
		(function(){
			axios.get('{{ route('cabinet.tickets.read', $ticket) }}');
        })();
    </script>
@endsection
