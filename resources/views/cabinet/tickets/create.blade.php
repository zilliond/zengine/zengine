@extends('layouts.cabinet')

@section('body')
    @include('flash::message')

    <div class="clearfix"></div>
    <h1>@lang('cabinet/support.create.title')</h1>
    <form action="{{ route('cabinet.tickets.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="subject_field">@lang('cabinet/support.create.subject')</label>
            <input type="text" class="form-control" name="subject" id="subject_field">
        </div>
        <div class="form-group">
            <label for="message_field">@lang('cabinet/support.create.message')</label>
            <textarea class="form-control" id="message_field" rows="5" name="message"></textarea>
        </div>
        <button class="btn btn-primary" type="submit">@lang('cabinet/support.create.submit')</button>
    </form>
@endsection
