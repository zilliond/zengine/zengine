@extends('layouts.cabinet')

@section('body')
    @include('flash::message')
    @php /** @var $user \Modules\Core\Models\User */ @endphp
    <div class="clearfix"></div>
    <div class="row cabinet-profile">
        <div class="col-sm-12">
            <h1>@lang('cabinet/security.change_password.title')</h1>
            {!! Form::model($user, ['route' => ['cabinet.security.password'], 'method' => 'post']) !!}
            <div class="form-group">
                {!! Form::label('old_password', trans('cabinet/security.change_password.old_password')) !!}
                {!! Form::password('old_password', ['class' => 'form-control '.( $errors->has('old_password') ? ' is-invalid' : '' )]) !!}
            </div>
            <div class="form-group">
                {!! Form::label('password', trans('cabinet/security.change_password.new_password')) !!}
                {!! Form::password('password', ['class' => 'form-control '.( $errors->has('password') ? ' is-invalid' : '' )]) !!}
            </div>
            <div class="form-group">
                {!! Form::label('password_confirmation', trans('cabinet/security.change_password.new_password_confirmation')) !!}
                {!! Form::password('password_confirmation', ['class' => 'form-control '.( $errors->has('password_confirmation') ? ' is-invalid' : '' )]) !!}
            </div>
            <button class="btn btn-primary " type="submit">@lang('cabinet/ui.save')</button>
            {!! Form::close() !!}
        </div>
        <div class="col-sm-12 mt-3">
            @include('cabinet/components.security_2fa')
        </div>
        <div class="col-sm-12 mt-3">
            @include('cabinet/components.security_pin')
        </div>
    </div>
@endsection
@section('css')
    <style>
        .cabinet-profile .col-sm-6{
            margin-bottom: 1rem;
        }
    </style>
@endsection
