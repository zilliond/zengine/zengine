<h1>@lang('cabinet/security.pin.title')</h1>
@if($user->pin_code)
    {!! Form::open(['route' => 'cabinet.security.pin.disable', 'method' => 'POST']) !!}
    <div class="form-group">
        {!! Form::label('code', trans('cabinet/security.pin.code')) !!}
        {!! Form::text('code', null, ['class' => 'form-control '.( $errors->has('old_password') ? ' code' : '' )]) !!}
    </div>
    <button class="btn btn-primary" type="submit">@lang('cabinet/ui.disable')</button>
    @can('locked')
        <button class="btn btn-secondary" type="button" @click="unlock_modal = true">@lang('cabinet/ui.unlock')</button>
    @else
        <a class="btn btn-secondary" href="{{ route('cabinet.security.pin.lock') }}">@lang('cabinet/ui.lock')</a>
    @endcan
    {!! Form::close() !!}
@else
    {!! Form::open(['route' => 'cabinet.security.pin.enable', 'method' => 'POST']) !!}
    <div class="form-group">
        {!! Form::label('code', trans('cabinet/security.pin.code')) !!}
        {!! Form::text('code', null, ['class' => 'form-control '.( $errors->has('old_password') ? ' is-invalid' : '' )]) !!}
    </div>
    <button class="btn btn-primary" type="submit">@lang('cabinet/ui.enable')</button>
    {!! Form::close() !!}
@endif
