<?php

use Modules\Core\Models\Deposit;

return [
    'statuses' => [
        app('zengine')->modelClass('Deposit')::STATUS_OPEN    => 'Открыт',
        app('zengine')->modelClass('Deposit')::STATUS_CLOSED  => 'Закрыт',
    ]
];
