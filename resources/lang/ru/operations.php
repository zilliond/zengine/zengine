<?php
use Modules\Core\Models\Operation;

return [
    'types' => [
        app('zengine')->modelClass('Operation')::TYPE_USER_REFILL     => 'Пополнение',
        app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW   => 'Вывод',
        app('zengine')->modelClass('Operation')::TYPE_USER_TRANSFER   => 'Перевод',
        app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_OPEN    => 'Открытие депозита',
        app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_ACCRUAL => 'Начисление',
        app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_CLOSE   => 'Закрытие депозита',
        app('zengine')->modelClass('Operation')::TYPE_REFERRAL_PAY    => 'Выплата по реферальной системе',
    ],
    'statuses' => [
        app('zengine')->modelClass('Operation')::STATUS_CREATED       => 'Создана',
        app('zengine')->modelClass('Operation')::STATUS_CANCELED      => 'Отменен',
        app('zengine')->modelClass('Operation')::STATUS_REJECTED      => 'Отклонен',
        app('zengine')->modelClass('Operation')::STATUS_IN_PROGRESS   => 'В процессе',
        app('zengine')->modelClass('Operation')::STATUS_SUCCESS       => 'Успешно',
    ],
    'status_messages' => [
        'withdraw' => [
            app('zengine')->modelClass('Operation')::STATUS_CREATED       => 'Заявка на вывод успешно создана',
            app('zengine')->modelClass('Operation')::STATUS_CANCELED      => 'Заявка на вывод отменена',
            app('zengine')->modelClass('Operation')::STATUS_REJECTED      => 'Заявка на вывод отклонена администрацией',
            app('zengine')->modelClass('Operation')::STATUS_IN_PROGRESS   => 'Заявка на вывод в процессе обработки',
            app('zengine')->modelClass('Operation')::STATUS_SUCCESS       => 'Заявка на вывод успешно завершена',
        ]
    ]
];
