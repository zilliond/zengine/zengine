<?php

return [
    'user_operations' => [
        'created' => [
            app('zengine')->modelClass('Operation')::TYPE_USER_REFILL     => 'Создана операция на пополнение, на сумму :amount :currency через платежную систему :Payment',
            app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW   => 'Создана операция на вывод, на сумму :amount :currency через платежную систему :Payment',
            app('zengine')->modelClass('Operation')::TYPE_USER_TRANSFER   => 'Создана операция на перевод, на сумму :amount :currency пользователю :user',
            app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_OPEN    => '', // Создано депозит
            app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_ACCRUAL => '', // Создано начисление
            app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_CLOSE   => 'Депозит :deposit закрыт, заработано :profit :currency',
        ],
        'status_updated' => [
            app('zengine')->modelClass('Operation')::STATUS_SUCCESS => [
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_ACCRUAL => 'Получено начисление по депозиту :deposit, на сумму :amount :currency, осталось :left',
                app('zengine')->modelClass('Operation')::TYPE_REFERRAL_PAY    => 'Получена выплата по реферальной системе, на сумму :amount :currency, за пользователя :user',
                app('zengine')->modelClass('Operation')::TYPE_USER_REFILL     => 'Получено пополнение, на сумму :amount :currency через платежную систему :payment', // пополнение успешно завершен
                app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW   => '', // Вывод успешно завершен
                app('zengine')->modelClass('Operation')::TYPE_USER_TRANSFER   => '', // Успешно переведено
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_OPEN    => '', // Создано депозит
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_CLOSE   => '', // Депозит успешно закончен
            ],
            app('zengine')->modelClass('Operation')::STATUS_CANCELED => [ // Отменено пользователем или автоматически
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_ACCRUAL => '', // начисление по депозиту
                app('zengine')->modelClass('Operation')::TYPE_REFERRAL_PAY    => '', // Выплата по реф системе
                app('zengine')->modelClass('Operation')::TYPE_USER_REFILL     => '', // Пополнение
                app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW   => '', // Вывод
                app('zengine')->modelClass('Operation')::TYPE_USER_TRANSFER   => '', // Перевод
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_OPEN    => '', // Открыт депозит
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_CLOSE   => '', // Закрыт депозит
            ],
            app('zengine')->modelClass('Operation')::STATUS_REJECTED => [ // Отменено админом
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_ACCRUAL => '', // начисление по депозиту
                app('zengine')->modelClass('Operation')::TYPE_REFERRAL_PAY    => '', // Выплата по реф системе
                app('zengine')->modelClass('Operation')::TYPE_USER_REFILL     => '', // Пополнение
                app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW   => '', // Вывод
                app('zengine')->modelClass('Operation')::TYPE_USER_TRANSFER   => '', // Перевод
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_OPEN    => '', // Открыт депозит
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_CLOSE   => '', // Закрыт депозит
            ],
            app('zengine')->modelClass('Operation')::STATUS_IN_PROGRESS => [ // Операция в процессе
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_ACCRUAL => '', // начисление по депозиту
                app('zengine')->modelClass('Operation')::TYPE_REFERRAL_PAY    => '', // Выплата по реф системе
                app('zengine')->modelClass('Operation')::TYPE_USER_REFILL     => '', // Пополнение
                app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW   => '', // Вывод
                app('zengine')->modelClass('Operation')::TYPE_USER_TRANSFER   => '', // Перевод
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_OPEN    => '', // Открыт депозит
                app('zengine')->modelClass('Operation')::TYPE_DEPOSIT_CLOSE   => '', // Закрыт депозит
            ],
        ]
    ],
    'user_deposits' => [
        'created' => 'Создан новый депозит на сумму :amount :currency, :accruals начислений'
    ]
];
