<?php

use Modules\Core\Models\Ticket;

return [
    'statuses' => [
        app('zengine')->modelClass('Ticket')::STATUS_OPEN     => 'Открыт',
        app('zengine')->modelClass('Ticket')::STATUS_CLOSE    => 'Закрыт',
    ]
];
