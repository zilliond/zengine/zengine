<?php

return [
    'title'             => 'Профиль',
    'form'              => [
        'login'         => 'Логин',
        'name'          => 'Имя',
        'email'         => 'Почта',
        'currency'      => 'Валюта',
        'invite_link'   => 'Сссылка для приглашения',
    ],
    'referrals' => [
        'title'         => 'Рефералы',
        'login'         => 'Логин',
        'name'          => 'Имя',
        'earned'        => 'Заработано',
        'level'         => 'Уровень',
    ]
];
