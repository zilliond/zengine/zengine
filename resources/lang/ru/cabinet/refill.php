<?php

return [
    'title'             => 'Пополнить',
    'amount'            => 'Сумма',
    'currency'          => 'Валюта',
    'payment_system'    => 'Платежная система',
    'submit'            => 'Пополнить',
    'alerts'            => [
        'success'       => 'Зачисление успешно',
        'error'         => 'Ошибка при зачислении',
    ],
];
