<?php

return [
    'menu' => [
        'index'         => 'Главная',
        'profile'       => 'Профиль',
        'referrals'     => 'Рефералы',
        'security'      => 'Безопасность',
        'wallets'       => 'Кошельки',
        'invest'        => 'Инвестировать',
        'deposits'      => 'Депозиты',
        'operations'    => 'Операции',
        'refill'        => 'Пополнить',
        'withdraw'      => 'Вывести',
        'transfer'      => 'Перевести',
        'support'       => 'Поддержка',
    ],
    'account_locked'    => 'Аккаунт заблокирован, для вывода необходимо разблокировать.',
    'close'             => 'Закрыть',
    'save'              => 'Сохранить',
    'enable'            => 'Включить',
    'disable'           => 'Выключить',
    'unlock'            => 'Разблокировать',
    'lock'              => 'Заблокировать',
    'pin_code'          => 'PIN код',
    'search'            => 'Поиск',
];
