<?php

return [
    'registration' => 'Регистрация',
    'sign_up'      => 'Зарегистрироваться',
    'menu'         => [
        'home'      => 'Главная',
        'cabinet'   => 'Кабинет',
        'news'      => 'Новости',
        'faq'       => 'FAQ',
        'dashboard' => 'Админ-панель',
        'login'     => 'Вход',
        'register'  => 'Регистрация',
        'logout'    => 'Выйти',
    ],
    'yes'          => 'Да',
    'no'           => 'Нет',
    'days'         => 'Дней',
    'hours'        => 'Месяцев',
];
