<?php

return [
    'plans' => [
        'title'                         => 'Тарифные планы',
        'amount'                        => 'Сумма',
        'percent'                       => 'Процент',
        'charge_weekends'               => 'Начисления в выходные',
        'limit'                         => 'Лимит',
        'additional_referral_percent'   => 'Дополнительный процент рефераллам',
        'invest'                        => 'Инвестировать',
    ],
    'statistics' => [
        'title'             => 'Статистика',
        'users'             => 'Пользователей',
        'active_users'      => 'Активных пользователей',
        'online_users'      => 'Пользователей онлайн',
        'today_deposits'    => 'Сумма депозитов сегодня',
        'total_withdraws'   => 'Сумма выводов',
        'total_refills'     => 'Сумма пополнений',
        'start_date'        => 'Прошло дней со старта',
    ]
];
