<?php

return [
    'title'     => 'Invest',
    'form'      => [
        'currency'  => 'Currency',
        'amount'    => 'Amount',
        'wallet'    => 'Wallet',
        'submit'    => 'Invest',
    ],
    'plan' => [
        'amount'                        => 'Amount',
        'percent'                       => 'Percent',
        'charge_weekends'               => 'Weekends charge',
        'limit'                         => 'Limit',
        'additional_referral_percent'   => 'Additional referral percent',
    ]
];
