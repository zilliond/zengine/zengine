<?php

return [
    'change_password' => [
        'title'                     => 'Change password',
        'old_password'              => 'Old password',
        'new_password'              => 'New password',
        'new_password_confirmation' => 'New password confirmation',
    ],
    '2fa' => [
        'title'             => '2FA authentication',
        'code'              => 'Code',
        'not_support_qr'    => "If your 2FA mobile app doesn't support QR, enter the following secret: <code>:SECRET</code>",
    ],
    'pin' => [
        'title'             => 'PIN code',
        'code'              => 'Code',
    ]
];
