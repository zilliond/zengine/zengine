<?php

return [
    'title'             => 'Withdraw',
    'amount'            => 'Amount',
    'currency'          => 'Currency',
    'payment_system'    => 'Payment system',
    'submit'            => 'Withdraw',
    'alerts'            => [
        'success'       => 'Withdraw success',
        'error'         => 'Withdraw error',
        'in_progress'   => 'Withdraw in progress',
    ],
];
