<?php

return [
    'wallets'   => 'Wallets',
    'wallet'    => 'Wallet',
    'balance'   => 'Balance',
    'stats'     => 'Statistics',
];
