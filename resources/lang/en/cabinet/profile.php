<?php

return [
    'title'             => 'Profile',
    'form'              => [
        'login'         => 'Login',
        'name'          => 'Name',
        'email'         => 'Email',
        'currency'      => 'Currency',
        'invite_link'   => 'Referral link',
    ],
    'referrals' => [
        'title'         => 'Referrals',
        'login'         => 'Login',
        'name'          => 'Name',
        'earned'        => 'Earned',
        'level'         => 'Level',
    ]
];
