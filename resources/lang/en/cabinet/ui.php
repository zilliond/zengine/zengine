<?php

return [
    'menu' => [
        'index'         => 'Home',
        'profile'       => 'Profile',
        'referrals'     => 'Referrals',
        'security'      => 'Security',
        'wallets'       => 'Wallets',
        'invest'        => 'Invest',
        'deposits'      => 'Deposits',
        'operations'    => 'Operations',
        'refill'        => 'Refill',
        'withdraw'      => 'Withdraw',
        'transfer'      => 'Transfer',
        'support'       => 'Support',
    ],
    'account_locked'    => 'The account is blocked, you need to unblock it to withdraw.',
    'close'             => 'Close',
    'save'              => 'Save',
    'enable'            => 'Enable',
    'disable'           => 'Disable',
    'unlock'            => 'Unlock',
    'lock'              => 'Lock',
    'pin_code'          => 'PIN code',
    'search'            => 'Search',
];
