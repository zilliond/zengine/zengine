<?php

return [
    'title'             => 'Refill',
    'amount'            => 'Amount',
    'currency'          => 'Currency',
    'payment_system'    => 'Payment system',
    'submit'            => 'Refill',
    'alerts'            => [
        'success'       => 'Refill success',
        'error'         => 'Refill error',
    ],
];
