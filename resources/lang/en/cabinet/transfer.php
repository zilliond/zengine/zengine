<?php

return [
    'title'             => 'Transfer',
    'receiver_login'    => 'Recipient login',
    'amount'            => 'Amount',
    'currency'          => 'Currency',
    'wallet'            => 'Wallet',
    'submit'            => 'Transfer',
];
