<?php

use Modules\Core\Models\Ticket;

return [
    'statuses' => [
        app('zengine')->modelClass('Ticket')::STATUS_OPEN     => 'Open',
        app('zengine')->modelClass('Ticket')::STATUS_CLOSE    => 'Close',
    ]
];
