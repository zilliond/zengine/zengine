<?php

use Modules\Core\Models\Deposit;

return [
    'statuses' => [
        app('zengine')->modelClass('Deposit')::STATUS_OPEN    => 'Opened',
        app('zengine')->modelClass('Deposit')::STATUS_CLOSED  => 'Closed',
    ]
];
