<?php

return [
    'plans' => [
        'title'                         => 'Investments plans',
        'amount'                        => 'Amount',
        'percent'                       => 'Percent',
        'charge_weekends'               => 'Weekends charge',
        'limit'                         => 'Limit',
        'additional_referral_percent'   => 'Additional referrals percent',
        'invest'                        => 'Invest',
    ],
    'statistics' => [
        'title'             => 'Statistics',
        'users'             => 'Users',
        'active_users'      => 'Active users',
        'online_users'      => 'Online users',
        'today_deposits'    => 'Total today deposits',
        'total_withdraws'   => 'Total withdraws',
        'total_refills'     => 'Total refills',
        'start_date'        => 'Days have passed since the start',
    ]
];
