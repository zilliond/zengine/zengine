export default {
    "en": {
        "ui": {
            "registration": "Registration",
            "sign_up": "Sign up",
            "menu": {
                "home": "Home",
                "cabinet": "Cabinet",
                "news": "News",
                "faq": "FAQ",
                "dashboard": "Admin-panel",
                "login": "Sign in",
                "register": "Sign up",
                "logout": "Logout"
            },
            "yes": "Yes",
            "no": "No",
            "days": "Days",
            "hours": "Hours"
        },
        "cabinet": {
            "invest": {
                "title": "Invest",
                "form": {
                    "currency": "Currency",
                    "amount": "Amount",
                    "wallet": "Wallet",
                    "submit": "Invest"
                },
                "plan": {
                    "amount": "Amount",
                    "percent": "Percent",
                    "charge_weekends": "Weekends charge",
                    "limit": "Limit",
                    "additional_referral_percent": "Additional referral percent"
                }
            },
            "ui": {
                "menu": {
                    "index": "Home",
                    "profile": "Profile",
                    "referrals": "Referrals",
                    "security": "Security",
                    "wallets": "Wallets",
                    "invest": "Invest",
                    "deposits": "Deposits",
                    "operations": "Operations",
                    "refill": "Refill",
                    "withdraw": "Withdraw",
                    "transfer": "Transfer",
                    "support": "Support"
                },
                "account_locked": "The account is blocked, you need to unblock it to withdraw.",
                "close": "Close",
                "save": "Save",
                "enable": "Enable",
                "disable": "Disable",
                "unlock": "Unlock",
                "lock": "Lock",
                "pin_code": "PIN code",
                "search": "Search"
            }
        }
    },
    "ru": {
        "ui": {
            "registration": "Регистрация",
            "sign_up": "Зарегистрироваться",
            "menu": {
                "home": "Главная",
                "cabinet": "Кабинет",
                "news": "Новости",
                "faq": "FAQ",
                "dashboard": "Админ-панель",
                "login": "Вход",
                "register": "Регистрация",
                "logout": "Выйти"
            },
            "yes": "Да",
            "no": "Нет",
            "days": "Дней",
            "hours": "Месяцев"
        },
        "cabinet": {
            "invest": {
                "title": "Инвестировать",
                "form": {
                    "currency": "Валюта",
                    "amount": "Сумма",
                    "wallet": "Кошелек",
                    "submit": "Инвестировать"
                },
                "plan": {
                    "amount": "Сумма",
                    "percent": "Процент",
                    "charge_weekends": "Начисления в выходные",
                    "limit": "Лимит",
                    "additional_referral_percent": "Дополнительный процент рефераллам"
                }
            },
            "ui": {
                "menu": {
                    "index": "Главная",
                    "profile": "Профиль",
                    "referrals": "Рефералы",
                    "security": "Безопасность",
                    "wallets": "Кошельки",
                    "invest": "Инвестировать",
                    "deposits": "Депозиты",
                    "operations": "Операции",
                    "refill": "Пополнить",
                    "withdraw": "Вывести",
                    "transfer": "Перевести",
                    "support": "Поддержка"
                },
                "account_locked": "Аккаунт заблокирован, для вывода необходимо разблокировать.",
                "close": "Закрыть",
                "save": "Сохранить",
                "enable": "Включить",
                "disable": "Выключить",
                "unlock": "Разблокировать",
                "lock": "Заблокировать",
                "pin_code": "PIN код",
                "search": "Поиск"
            }
        }
    }
}
