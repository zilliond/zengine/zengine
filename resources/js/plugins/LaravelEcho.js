import Echo from 'laravel-echo';
if(!window.io){
    window.io = require('socket.io-client');
}
export default class LaravelEcho {
    constructor(options){
        this.echo = new Echo(options);
    }
    install (Vue) {
        Vue.prototype.$laravelEcho = this.echo;
    }
}
