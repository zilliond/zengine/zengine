<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/
Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('user.operations.{userId}', function ($user, $userId) {
    return $user->id === (int) $userId;
});

Broadcast::channel('operation.{id}', function ($user, $id) {
    $operation = app('zengine')->model('Operation')->find($id);
    return $user->id === $operation->user_id;
});
