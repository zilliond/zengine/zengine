<?php

namespace App\Http\Controllers;

use Modules\Core\Models\Post;

class NewsController extends \Modules\Core\Http\Controllers\Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts = app('zengine')->model('Post')->visible()->latest()->paginate();
        return view('posts.index', compact('posts'));
    }

    /**
     * @param  Post  $post
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Post $post)
    {
        $post->views++;
        $post->save();
        return view('posts.show', compact('post'));
    }

    public function watched(Post $post)
    {
        $post->views++;
        $post->save();
    }
}
