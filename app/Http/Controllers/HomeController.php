<?php

namespace App\Http\Controllers;

use Cache;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Log;
use Modules\Core\Http\Controllers\Controller;
use Modules\Core\Models\Deposit;
use Modules\Core\Models\Operation;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return Factory|View
     */
    public function index()
    {
        $plans = app('zengine')->model('Plan')->with('currency')->visible()->get();

        $stats = $this->getStats();
        return view('home', [
            'plans'             => $plans,
            'last_deposits'     => $this->lastDeposits(),
            'last_withdraws'    => $this->lastWithdraws(),
            'top_refs'          => $this->topRefs(),
            'stats'             => $stats
        ]);
    }

    protected function lastDeposits()
    {
        return app('zengine')->model('Deposit')->with(['currency', 'user'])
            ->latest()
            ->limit(5)
            ->get();
    }

    protected function lastWithdraws()
    {
        return app('zengine')->model('Operation')->with(['currency', 'user'])
            ->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW)
            ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)
            ->latest()
            ->limit(5)
            ->get();
    }

    protected function topRefs()
    {
        return app('zengine')->model('User')->with('currency')
            ->orderByDesc('referrals_earned')
            ->limit(5)
            ->get();
    }

    protected function getStats() : array
    {
        $currencyService = app(config('zengine.services.currencies'));
        $users = app('zengine')->model('User')->count() + setting('fake.users');
        $active_users = app('zengine')->model('User')->whereHas('operations')->count() + setting('fake.active_users');
        $online_users = (int) Cache::get('online-users', 0) + setting('fake.online_users');
        $today_deposits = app('zengine')->model('Deposit')->with(['currency'])
                ->where('created_at', '>=', now()->startOfDay())
                ->where('created_at', '<=', now()->endOfDay())
                ->get()
                ->reduce(static function ($carry, $deposit) use ($currencyService) {
                    /** @var Deposit $deposit */
                    return $carry + $currencyService->convertToDefault($deposit->amount, $deposit->currency);
                }, 0) + setting('fake.today_deposits_sums');
        $total_refills = app('zengine')->model('Operation')->with('currency')
            ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)
            ->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_REFILL)
            ->get()
            ->reduce(static function ($carry, $operation) use ($currencyService) {
                /** @var Operation $operation */
                return $carry + $currencyService->convertToDefault($operation->amount, $operation->currency);
            }, 0);
        $total_refills += setting('fake.refills_sum');
        $total_withdraws = app('zengine')->model('Operation')->with('currency')
            ->where('status', app('zengine')->modelClass('Operation')::STATUS_SUCCESS)
            ->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW)
            ->get()
            ->reduce(static function ($carry, $operation) use ($currencyService) {
                /** @var Operation $operation */
                return $carry + $currencyService->convertToDefault($operation->amount, $operation->currency);
            }, 0);
        $start_date = now()->diffInDays(setting('general.start_date'));
        $total_withdraws += setting('fake.withdraws_sum');

        return compact('users', 'active_users', 'online_users', 'today_deposits', 'total_refills', 'total_withdraws', 'start_date');
    }

    public function faq()
    {
        $faq_items = app('zengine')->model('FaqItem')->visible()->get();
        return \view('faq', ['faq_items' => $faq_items]);
    }

    /**
     * TODO: need refactor and move action
     * @param  Request  $request
     * @return mixed
     */
    public function ipn(Request $request)
    {
        Log::channel('payment_systems')->info('OperationService|handler|start '.json_encode($request->all()));
        $operationService = app('operations');
        return $operationService->handle($request);
    }
}
