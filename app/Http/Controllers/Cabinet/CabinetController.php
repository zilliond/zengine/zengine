<?php

namespace App\Http\Controllers\Cabinet;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\Models\Deposit;
use Modules\Core\Models\Operation;
use Modules\Core\Models\User;

class CabinetController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        /** @var User $user */
        $user = \Auth::user();
        $user->load('wallets.payment_system.currency');
        return view('cabinet.index', [
            'user'  => $user,
            'stats' => $this->getStats()
        ]);
    }

    protected function getStats() : array
    {
        $currencyService = app('currencies');
        /** @var \Modules\Core\Models\User $user */
        $user = \Auth::user();
        $stats = [];
        $stats['total_refills'] = $user->operations()->with('currency')->type(app('zengine')->modelClass('Operation')::TYPE_USER_REFILL)->status(app('zengine')->modelClass('Operation')::STATUS_SUCCESS)->get()
            ->reduce(static function ($total, Operation $operation) use ($currencyService) {
                return $total + $currencyService->convertToDefault($operation->amount, $operation->currency);
            }, 0);
        $stats['total_withdraws'] = $user->operations()->with('currency')->type(app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW)->status(app('zengine')->modelClass('Operation')::STATUS_SUCCESS)->get()
            ->reduce(static function ($total, Operation $operation) use ($currencyService) {
                return $total + $currencyService->convertToDefault($operation->amount, $operation->currency);
            }, 0);
        $stats['deposits_opened_count'] = $user->deposits()->where('status', app('zengine')->modelClass('Deposit')::STATUS_OPEN)->count();
        $stats['deposits_closed_count'] = $user->deposits()->where('status', app('zengine')->modelClass('Deposit')::STATUS_CLOSED)->count();
        $stats['deposits_opened_sum'] = $user->deposits()->with('currency')->where('status', app('zengine')->modelClass('Deposit')::STATUS_OPEN)->get()
            ->reduce(static function ($total, Deposit $deposit) use ($currencyService) {
                return $total + $currencyService->convertToDefault($deposit->amount, $deposit->currency);
            }, 0);
        $stats['referrals_earned'] = $currencyService->convertToDefault($user->referrals_earned, $user->currency);
        $stats['referrals_count'] = $user->referrals_count;
        $stats['accruals_tomorrow'] = $this->accruals_tomorrow($user);
        return $stats;
    }

    protected function accruals_tomorrow(User $user)
    {
        $currencies = app('currencies');
        $deposits = $user->deposits()->with('plan')->where('left_accruals', '>', 0)->get();
        $diff_days = now()->diffInDays(now()->addDay(), 1);
        $diff_hours = now()->diffInHours(now()->addDay(), 1);
        $estimated = 0;
        foreach ($deposits as $deposit) {
            /** @var Deposit $deposit */
            if ($deposit->plan->type === app('zengine')->modelClass('Plan')::TYPE_DAILY) {
                $accrual = ($deposit->amount / 100) * $deposit->plan->percent;
                $estimated += $currencies->convertToDefault($accrual * min($deposit->left_accruals, $diff_days), $deposit->currency);
            } elseif ($deposit->plan->type === app('zengine')->modelClass('Plan')::TYPE_HOURLY) {
                $accrual = ($deposit->amount / 100) * $deposit->plan->percent;
                $estimated += $currencies->convertToDefault($accrual * min($deposit->left_accruals, $diff_hours), $deposit->currency);
            }
        }
        return $estimated;
    }

    public function referrals(Request $request)
    {
        $user = \Auth::user();
        $user->loadMissing(['inviter', 'referrals']);
        return view('cabinet.referrals', [
            'user'    => $user,
            'service' => app('users')
        ]);
    }

    public function profile(Request $request)
    {
        /** @var User $user */
        $user = \Auth::user();
        $user->loadMissing(['inviter', 'referrals']);
        $payment_systems = app('zengine')->model('PaymentSystem')->with([
            'wallets' => static function ($query) use ($user) {
                return $query->where('user_id', $user->id);
            }
        ])->enabled()->where('need_wallet', 1)->get();
        return view('cabinet.profile', [
            'user'              => $user,
            'payment_systems'   => $payment_systems,
            'service'           => app('users')
        ]);
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function storeProfile(Request $request)
    {
        $user = \Auth::user();
        $user->fill($request->only(['currency_id', 'name']));
        if ($user->isDirty('currency_id', 'name')) {
            $user->save();
            \Flash::success('Профиль обновлен');
        }
        return redirect(route('cabinet.profile'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function wallets()
    {
        $payment_systems = app('zengine')->model('PaymentSystem')->with([
            'wallets' => function ($query) {
                return $query->where('user_id', \Auth::id());
            }
        ])->enabled()->where('need_wallet', 1)->get();
        return view('cabinet.wallets', [
            'user'              => \Auth::user(),
            'payment_systems'   => $payment_systems,
        ]);
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeWallets(Request $request)
    {
        $user = \Auth::user();
        $wallets = $user->wallets()->pluck('wallet', 'payment_system_id');
        $data = $request->all();
        foreach ($data as $key => $value) {
            if (strpos($key, 'wallet_') !== 0 || !$value) {
                continue;
            }
            $payment_system = app('zengine')->model('PaymentSystem')->findOrFail((int) substr($key, 7));
            if (isset($wallets[$payment_system->id]) && $wallets[$payment_system->id] === $value) {
                continue;
            }
            $user->wallets()->updateOrCreate([
                'payment_system_id' => $payment_system->id
            ], [
                'wallet' => $value
            ]);
            \Flash::success("{$payment_system->name} saved");
        }
        return redirect()->back();
    }
}
