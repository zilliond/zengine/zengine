<?php


namespace App\Http\Controllers\Cabinet;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\Exceptions\BalanceException;
use Modules\Core\Exceptions\BalanceTypeNotFindException;
use Modules\Core\Exceptions\CanPayWalletException;
use Modules\Core\Models\Operation;
use Modules\Core\Models\User;
use Modules\Core\Services\OperationService;

class OperationController extends Controller
{
    /**
     * @var OperationService
     */
    protected $operationService;
    /**
     * @var \Modules\Core\Services\UserService|\Illuminate\Contracts\Foundation\Application
     */
    protected $userService;
    /**
     * @var \Modules\Core\CurrencyRateUpdaters\CurrencyServiceInterface|\Illuminate\Contracts\Foundation\Application
     */
    protected $currencyService;

    public function __construct()
    {
        $this->operationService = app('operations');
        $this->currencyService = app('currencies');
        $this->userService = app('users');
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        /** @var \Modules\Core\Models\User $user */
        $user = \Auth::user();
        $operations = $user->operations()->with(['currency', 'payment_system', 'wallet.payment_system.currency'])->latest()->requestFilter()->paginate();
        $wallets = $user->wallets()->with('payment_system')->get();
        return view('cabinet.operations.index', [
            'operations'    => $operations,
            'wallets'       => $wallets,
            'service'       => $this->operationService,
        ]);
    }

    public function cancel(Request $request, Operation $operation)
    {
        if ($this->operationService->typeByOperation($operation)->canCancel($operation)) {
            $operation = $this->operationService->typeByOperation($operation)->cancel($operation);
            if ($request->ajax()) {
                return [
                    'status'    => 'success',
                    'operation' => $operation
                ];
            } else {
                \Flash::success('Операция успешно отменена');
                return redirect()->back();
            }
        }
        if ($request->ajax()) {
            return [
                'status'    => 'error',
                'operation' => $operation
            ];
        } else {
            \Flash::success('Операция не может быть отменена');
            return redirect()->back();
        }
    }

    public function refill($status = null)
    {
        $payment_systems = app('zengine')->model('PaymentSystem')->refill()->get();
        return view('cabinet.operations.refill', [
            'currencies'        => app('zengine')->model('Currency')->all(),
            'payment_systems'   => $payment_systems,
            'status'            => $status
        ]);
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \Exception
     */
    public function storeRefill(Request $request)
    {
        $user = \Auth::user();
        /** @var $user \Modules\Core\Models\User  */
        $wallet = null;
        if (setting('balances_type') === 'divided') {
            $wallet = $user->wallets()->firstOrCreate([
                'payment_system_id' => $request->get('payment_system'),
            ], [
                'balance' => 0
            ]);
        }
        \Debugbar::info($wallet);
        $operation = app('zengine')->model('Operation')->create([
            'user_id'           => \Auth::id(),
            'currency_id'       => $request->get('currency_id'),
            'amount'            => $request->get('amount'),
            'type'              => app('zengine')->modelClass('Operation')::TYPE_USER_REFILL,
            'payment_system_id' => $request->get('payment_system'),
            'wallet_id'         => $wallet ? $wallet->id : null
        ]);
        $operation->target()->associate($user);
        $operation = $this->operationService->typeByOperation($operation)->create($operation);

        if ($operation->payment_system_data['type'] === 'redirect') {
            return redirect($operation->payment_system_data['redirect_url']);
        } elseif ($operation->payment_system_data['type'] === 'form') {
            return view('payment_form', [
                'operation' => $operation,
            ]);
        } elseif ($operation->payment_system_data['type'] === 'none') {
            \Flash::success('Refill created success.');
            return redirect(route('cabinet.operations'));
        } else {
            \Flash::error('Refill created error (undefined type).');
        }
        return redirect()->back();
    }

    public function withdraw($status = null)
    {
        /** @var User $user */
        $user = \Auth::user();
        $wallets = $user->wallets()->with('payment_system.currency')->get()->keyBy('payment_system_id');
        $payment_systems = app('zengine')->model('PaymentSystem')->withdraw()->get();
        return view('cabinet.operations.withdraw', [
            'currencies'        => app('zengine')->model('Currency')->all(),
            'payment_systems'   => $payment_systems,
            'wallets'           => $wallets,
            'status'            => $status
        ]);
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws BalanceException
     * @throws BalanceTypeNotFindException
     * @throws CanPayWalletException
     * @throws \Modules\Core\Exceptions\OperationException
     */
    public function storeWithdraw(Request $request)
    {
        $currency = app('zengine')->model('Currency')->findOrFail($request->get('currency_id'));
        $paymentSystem = app('zengine')->model('PaymentSystem')->findOrFail($request->get('payment_system'));
        /** @var \Modules\Core\Models\User $user */
        $user = \Auth::user();
        $amount = (float) $request->get('amount');
        $operation = new Operation();
        $operation->fill([
            'user_id'           => \Auth::id(),
            'currency_id'       => $currency->id,
            'amount'            => $amount,
            'type'              => app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW,
            'payment_system_id' => $paymentSystem->id
        ]);
        $wallet = null;
        if (setting('balances_type') === 'divided') {
            $wallet = $user->wallets()->where('payment_system_id', $paymentSystem->id)->first();
            if (!$wallet) {
                throw new CanPayWalletException();
            }
            $operation->wallet()->associate($wallet);
        }
        $operation->target()->associate($user);
        try {
            $this->userService->canPayFrom($user, $amount, $currency, $wallet);
        } catch (BalanceException $e) {
            \Flash::error('Недостаточно средств на счету');
            return redirect()->back();
        }
        if ($paymentSystem->need_wallet) {
            $wallet = $user->wallets()->where('payment_system_id', $paymentSystem->id)->first();
            if (!$wallet) {
                \Flash::error('Не указан кошлелек, укажите его в <a href="'.route('cabinet.wallets').'">настройках</a> ');
                return redirect()->back();
            }
            $operation->payment_system_data = [
                'target' => $wallet->wallet
            ];
        }
        if (setting('general.min_withdraw')) {
            if ($this->currencyService->convertToDefault($amount, $currency) < (float) setting('general.min_withdraw')) {
                \Flash::error('Сумма меньше минимальной для вывода');
                return redirect()->back();
            }
        }
        if (setting('general.max_withdraw')) {
            if ($this->currencyService->convertToDefault($amount, $currency) > (float) setting('general.max_withdraw')) {
                \Flash::error('Сумма больше максимальной для вывода');
                return redirect()->back();
            }
        }
        if (setting('general.withdraws_daily_limit')) {
            $today_withdraws = $user->operations()->where('type', app('zengine')->modelClass('Operation')::TYPE_USER_WITHDRAW)->count();
            if ($today_withdraws >= (int) setting('general.withdraws_daily_limit')) {
                \Flash::error('Превышен лимит заявок на вывод за день');
                return redirect()->back();
            }
        }
        if (setting('general.withdraw_api_limit') && $operation->payment_system->withdraw_type === 'auto') {
            if ($this->currencyService->convertToDefault($amount, $currency) > (float) setting('general.withdraw_api_limit')) {
                \Flash::error('Сумма больше максимальной для вывода c помощью API');
                return redirect()->back();
            }
        }
        $operation->save();
        $operation = $this->operationService->typeByOperation($operation)->create($operation);
        \Debugbar::info($operation);
        if (in_array($operation->status, [app('zengine')->modelClass('Operation')::STATUS_CREATED, app('zengine')->modelClass('Operation')::STATUS_SUCCESS, app('zengine')->modelClass('Operation')::STATUS_IN_PROGRESS], true)) {
            \Flash::success(trans('operations.status_messages.withdraw.'.$operation->status));
            return redirect(route('cabinet.operations'));
        } else {
            \Flash::error(trans('operations.status_messages.withdraw.'.$operation->status));
            return redirect()->back();
        }
    }

    public function transfer()
    {
        /** @var User $user */
        $user = \Auth::user();
        $wallets = $user->wallets()->with(['payment_system.currency'])->get();
        return view('cabinet.operations.transfer', [
            'currencies'    => app('zengine')->model('Currency')->all(),
            'wallets'       => $wallets
        ]);
    }

    public function storeTransfer(Request $request)
    {
        $wallet = null;
        if (setting('balances_type') === 'divided') {
            $wallet = app('zengine')->model('Wallet')->findOrFail($request->get('wallet_id'));
        }
        $operation = app('zengine')->model('Operation')->create([
            'user_id'           => \Auth::id(),
            'currency_id'       => $request->get('currency_id'),
            'wallet_id'         => $wallet ? $wallet->id : null,
            'amount'            => $request->get('amount'),
            'type'              => app('zengine')->modelClass('Operation')::TYPE_USER_TRANSFER,
            'status'            => app('zengine')->modelClass('Operation')::STATUS_CREATED
        ]);
        $target = app('zengine')->model('User')->where('login', $request->get('login'))->first();
        if (!$target) {
            \Flash::error('Пользователь не найден');
            return redirect()->back();
        }
        try {
            $this->userService->canPayFrom(\Auth::user(), $operation->amount, $operation->currency, $wallet);
        } catch (\Exception $e) {
            if ($e instanceof BalanceException) {
                \Flash::error('У вас недостаточно средств');
            } elseif ($e instanceof BalanceTypeNotFindException) {
                \Flash::error('Ошибка типа баланса');
            } elseif ($e instanceof CanPayWalletException) {
                \Flash::error('Ошибка кошелька');
            } else {
                \Flash::error('Неизвестная ошибка');
            }
            return redirect()->back();
        }
        $operation->target()->associate($target);
        $this->operationService->typeByOperation($operation)->create($operation);
        \Flash::success('Успешно переведено');
        return redirect(route('cabinet.operations'));
    }
}
