<?php

namespace App\Http\Controllers\Cabinet;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Models\Ticket;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('cabinet.tickets.index', [
            'tickets' => \Auth::user()->tickets()->latest()->paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('cabinet.tickets.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $ticket = \Auth::user()->tickets()->create([
            'subject' => $request->get('subject')
        ]);
        /** @var $ticket Ticket */
        $ticket->messages()->create([
            'user_id' => \Auth::id(),
            'message' => $request->get('message')
        ]);
        \Flash::success('Тикет отправлен');
        return redirect(route('cabinet.tickets.index'));
    }

    /**
     * Show the specified resource.
     * @param  Ticket  $ticket
     * @return Response
     */
    public function show(Ticket $ticket)
    {
        return view('cabinet.tickets.show', [
            'ticket' => $ticket
        ]);
    }

    /**
     * @param  Ticket  $ticket
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function message(Ticket $ticket, Request $request)
    {
        $ticket->messages()->create([
            'user_id' => \Auth::id(),
            'message' => $request->get('message')
        ]);

        \Flash::success('Сообщение отправлено');
        return redirect(route('cabinet.tickets.show', $ticket));
    }

    /**
     * @param  Ticket  $ticket
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function status(Ticket $ticket, Request $request)
    {
        $ticket->status = $request->get('status');
        $ticket->save();

        if ($request->get('status') === app('zengine')->modelClass('Ticket')::STATUS_OPEN) {
            \Flash::success('Тикет открыт');
        } elseif ($request->get('status') === app('zengine')->modelClass('Ticket')::STATUS_CLOSE) {
            \Flash::success('Тикет закрыт');
        }

        return redirect()->back();
    }

    /**
     * @param  Ticket  $ticket
     * @return array
     */
    public function read(Ticket $ticket)
    {
        $updated = $ticket->messages()->where('user_id', '!=', $ticket->user_id)->update([
            'read' => 1
        ]);
        return [
            'status' => $updated ? 'success' : 'not updated'
        ];
    }
}
