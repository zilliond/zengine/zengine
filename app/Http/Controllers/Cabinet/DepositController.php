<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Requests\Cabinet\StoreDepositRequest;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Core\Models\Deposit;
use Modules\Core\Services\DepositService;

class DepositController extends Controller
{
    use AuthorizesRequests;

    /**
     * @var DepositService
     */
    private $depositService;

    public function __construct(DepositService $depositService)
    {
        $this->depositService = $depositService;
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $deposits = app('zengine')->model('Deposit')->with(['plan', 'currency', 'wallet.payment_system'])->where('user_id', \Auth::id())
            ->latest()->paginate();
        return view('cabinet.deposits.index', compact('deposits'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $plans = app('zengine')->model('Plan')->with('currency')->visible()->get();
        $currencies = app('zengine')->model('Currency')->all();
        $wallets = optional(\Auth::user())->wallets()->with('payment_system')->get();
        return view('cabinet.deposits.create', compact('plans', 'currencies', 'wallets'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  StoreDepositRequest  $request
     * @return string[]
     * @throws \Exception
     */
    public function store(StoreDepositRequest $request)
    {
        \DB::beginTransaction();
        \Debugbar::info($request->all());
        try {
            $deposit = app('zengine')->model('Deposit')->create([
                'user_id'       => \Auth::id(),
                'plan_id'       => $request->get('plan_id'),
                'currency_id'   => $request->get('currency_id'),
                'wallet_id'     => $request->get('wallet_id'),
                'amount'        => $request->get('amount')
            ]);
            $this->depositService->create($deposit);
            \DB::commit();
            return [
                'status' => 'success'
            ];
        } catch (\Exception $exception) {
            \DB::rollBack();
            return [
                'status'  => 'error',
                'message' => $exception->getMessage(),
            ];
        }
    }

    /**
     * @param  Deposit  $deposit
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function close(Deposit $deposit)
    {
        $this->authorize('close', $deposit);
        $this->depositService->close($deposit);
        return redirect()->back();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('cabinet.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('cabinet.edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
