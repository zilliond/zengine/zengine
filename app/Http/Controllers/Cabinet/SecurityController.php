<?php


namespace App\Http\Controllers\Cabinet;

use App\Http\Requests\Cabinet\ChangePasswordRequest;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Core\Services\UserService;

class SecurityController extends Controller
{
    /**
     * @var \Modules\Core\Services\UserService
     */
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index(Request $request)
    {
        $user = \Auth::user();
        $view = view('cabinet.security', [
            'user' => $user
        ]);
        if (!$user->google2fa_secret) {
            $secret = \Google2FA::generateSecretKey();
            $image = \Google2FA::getQRCodeInline(
                $request->getHttpHost(),
                $user->email,
                $secret,
                200
            );
            $view->with([
                'secret_2fa'    => $secret,
                'image_2fa'     => $image
            ]);
        }
        return $view;
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \PragmaRX\Google2FA\Exceptions\IncompatibleWithGoogleAuthenticatorException
     * @throws \PragmaRX\Google2FA\Exceptions\InvalidCharactersException
     * @throws \PragmaRX\Google2FA\Exceptions\SecretKeyTooShortException
     */
    public function enable2fa(Request $request)
    {
        if (\Google2FA::verifyKey($request->get('secret'), $request->get('code'))) {
            $user = \Auth::user();
            $user->google2fa_secret = $request->get('secret');
            $user->save();
            \Flash::success('2FA включено');
        } else {
            \Flash::error('Неверный код');
        }
        return redirect()->back();
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \PragmaRX\Google2FA\Exceptions\IncompatibleWithGoogleAuthenticatorException
     * @throws \PragmaRX\Google2FA\Exceptions\InvalidCharactersException
     * @throws \PragmaRX\Google2FA\Exceptions\SecretKeyTooShortException
     */
    public function disable2fa(Request $request)
    {
        $user = \Auth::user();
        if (\Google2FA::verifyKey($user->google2fa_secret, $request->get('code'))) {
            $user->google2fa_secret = null;
            $user->save();
            \Flash::success('2FA отключено');
        } else {
            \Flash::error('Неверный код');
        }
        return redirect()->back();
    }

    public function enablePin(Request $request)
    {
        $user = \Auth::user();
        $user->pin_code = \Hash::make($request->get('code'));
        $user->save();
        \Flash::success('Pin код установлен');
        return redirect()->back();
    }

    public function disablePin(Request $request)
    {
        $user = \Auth::user();
        if (\Hash::check($request->get('code'), $user->pin_code)) {
            $user->pin_code = null;
            $user->save();
            \Flash::success('Pin код отключено');
        } else {
            \Flash::error('Неверный pin код');
        }
        return redirect()->back();
    }

    public function unlock(Request $request)
    {
        if ($this->userService->locked(\Auth::user())) {
            if ($this->userService->unlock(\Auth::user(), $request->get('code'))) {
                \Flash::success('Аккаунт успешно разблокирован');
            } else {
                \Flash::error('PIN код неверный');
            }
        } else {
            \Flash::info('Аккаунт уже разблокирован');
        }
        return redirect()->back();
    }

    public function lock()
    {
        $user = \Auth::user();
        if ($user->unlocked()) {
            session([
                'unlocked_'.$user->id => false
            ]);
            \Flash::success('Аккаунт заблокирован');
        } else {
            \Flash::info('Аккаунт уже заблокирован');
        }
        return redirect()->back();
    }

    /**
     * @param  ChangePasswordRequest  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        $user = \Auth::user();
        if (\Hash::check($request->get('old_password'), $user->password)) {
            $user->password = \Hash::make($request->get('password'));
            $user->save();
            \Flash::success('Пароль изменен');
        } else {
            \Flash::success('Старый пароль не совпадает');
        }
        return redirect(route('cabinet.profile'));
    }
}
